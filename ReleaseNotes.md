---
# Release Notes For The  [Originware](http://www.originware.com)   Reactive Fabric SDK Sample Kit v1.0 For iOS.
---

## Version 1.0 Release Notes
---
This is a fully functional release. 

### 1. Sample Kit Dependencies.

The Sample Kit requires Xcode 10+ and is written in Swift 5 (also 4.2 compatible).

### 2. Supplied In This Release Package

* Xcode Project:	
	* A sample iPad-iOS demo Point-Of-Interest app designed with Reactive Fabric.
	* A sample library of Reactive Fabric Sources and Operators. 
	* A Unit Test Test Rig for Reactive Fabric elements.

* Documentation:
	* The [Reactive Fabric White Paper](Reactive Fabric/Doc/Reactive Fabric Whitepaper.pdf).
	* The [Reactive Fabric For Desktop and Mobile Product Application](Reactive Fabric/Doc/Reactive Fabric For Desktop and Mobile Product Application.pdf).

* Frameworks (Located in the Framework folder):
	* The **Reactive Fabric SDK** framework is not included in this version. Please email a request for a separate copy of the SDK framework. 
	
### 3. Contact Details:

Please direct requests, questions, comments and feedback to [Terry Stillone](mailto:terry@originware.com) at [Originware.com](https://www.originware.com)


