//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 18/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POI Location Events
///

struct MultiPOILocateEvent_Request : CustomStringConvertible
{
    var poiKeywords : Set<String>
    let location : CLLocation

    var description : String
    {
        return poiKeywords.description
    }
    
    func createPOILocateRequest_WithoutKeywords(_ keywords : Set<String>) -> MultiPOILocateEvent_Request?
    {
        let newKeywordSet = poiKeywords.subtracting(keywords)

        return (newKeywordSet.count != 0) ? MultiPOILocateEvent_Request(poiKeywords: newKeywordSet, location: location) : nil
    }
}

struct POILocateEvent_Request
{
    var poiKeyword : String
    var requestedPOIKeywords: Set<String>
    let location : CLLocation

    func createPOILocateRequest_WithoutKeywords(_ keywords : Set<String>) -> POILocateEvent_Request?
    {
        return !keywords.contains(poiKeyword) ? self : nil
    }
}


struct POILocateEvent_Reply: CustomStringConvertible
{
    let locateSourceName : String
    let requestedKeywords : Set<String>
    let location : CLLocation
    let error : NSError?

    var poiByPOIKeyword = [String : [POI]]()

    var poiKeywords : [String]  {

        return error == nil ? [String](poiByPOIKeyword.keys) : [String](requestedKeywords)
    }

    var description : String {

        if error != nil
        {
            return "Error in locating POIs: \(error!)"
        }
        else
        {
            switch poiByPOIKeyword.count
            {
                case 1:                     return "Located POI: \(poiKeywords[0])"
                case _ where count <= 3:    return "Located POIs: \(poiKeywords)"
                default:                    return "Located POIs: \(poiKeywords)"
            }
        }
    }

    var count : Int {

        var total = 0

        for pois in poiByPOIKeyword.values
        {
            total += pois.count
        }

        return total
    }

    init(locateSourceName : String, requestPOIKeywords: Set<String>, location: CLLocation, error : NSError)
    {
        self.locateSourceName = locateSourceName
        self.requestedKeywords = requestPOIKeywords
        self.location = location
        self.error = error
    }

    init(locateSourceName : String, requestPOIKeywords: Set<String>, location: CLLocation, pois: [POI])
    {
        self.locateSourceName = locateSourceName
        self.requestedKeywords = requestPOIKeywords
        self.location = location
        self.error = nil

        addPOIs(pois)
    }

    mutating func addPOIs(_ pois : [POI])
    {
        var dict = [String : [POI]]()

        for poi in pois
        {
            let keyword = poi.poiKeyword

            if var entry = dict[keyword]
            {
                entry.append(poi)

                dict[keyword] = entry
            }
            else
            {
                dict[keyword] = [poi]
            }
        }

        poiByPOIKeyword = dict
    }

    func countForPOIType(_ keyword : String) -> Int
    {
        let entry = poiByPOIKeyword[keyword]

        return entry != nil ? entry!.count : 0
    }

}
