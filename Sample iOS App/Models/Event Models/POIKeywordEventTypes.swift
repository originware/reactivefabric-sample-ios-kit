//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 29/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POI Keyword Match Events.
///

class POIKeywordEvent_Match: CustomStringConvertible
{
    var poiKeywordMatchesByPOIKeyword = [String : POIKeywordMatch]()

    var haveFullPOIKeywordMatch: Bool               { return m_haveFullPOIMatch }
    var partialPOIKeywordMatchLength: Int           { return m_partialMatchLength }
    var count : Int                                 { return poiKeywordMatchesByPOIKeyword.count }
    var poiKeywords : [String]                      { return [String](poiKeywordMatchesByPOIKeyword.keys) }

    var description : String                        { return poiKeywords.description }

    fileprivate var m_haveFullPOIMatch = false
    fileprivate var m_partialMatchLength = 0

    func addMatchesForPOIKeyword(_ poiKeywordMatches : POIKeywordMatch)
    {
        poiKeywordMatchesByPOIKeyword[poiKeywordMatches.poiKeyword] = poiKeywordMatches

        if poiKeywordMatches.haveFullMatch                              { m_haveFullPOIMatch = true }
        if poiKeywordMatches.maxMatchLength > m_partialMatchLength      { m_partialMatchLength = poiKeywordMatches.maxMatchLength}
    }
}

