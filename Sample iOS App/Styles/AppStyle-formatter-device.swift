//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

extension AppStyle
{
    class func formatUILabel(forLocationChange: eDeviceEvent_Location, isSimulated : Bool) -> NSAttributedString
    {
        switch forLocationChange
        {
            case .eLocation_Invalid:
                return NSAttributedString(string:"Unknown", attributes:[NSAttributedString.Key.foregroundColor : AppStyle.color(.eNotAvailable)])

            case .eLocation_LastKnown:
                return NSAttributedString(string: isSimulated ? "Simulated Location" : "Have Location", attributes:[NSAttributedString.Key.foregroundColor : AppStyle.color(.eAvailable)])
        }
    }

    class func formatUILabel(forReachabilityChange: eDeviceEvent_Reachability) -> NSAttributedString
    {
        switch forReachabilityChange
        {
            case .eReachability_Unknown:
                return NSAttributedString(string:"Unknown", attributes:[NSAttributedString.Key.foregroundColor : AppStyle.color(.eNotAvailable)])

            case .eReachability_NotReachable:
                return NSAttributedString(string:"Not Reachable", attributes:[NSAttributedString.Key.foregroundColor : AppStyle.color(.eNotAvailable)])

            case .eReachability_IsReachable:
                return NSAttributedString(string:"Reachable", attributes:[NSAttributedString.Key.foregroundColor : AppStyle.color(.eAvailable)])
        }
    }
}
