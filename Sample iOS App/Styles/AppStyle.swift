//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import QuartzCore

public enum eColorElement : Int
{
    case eTitleBorderColor
    case eButtonBorderColor
    case eBorderColor
    case eButtonColor
    case eRecordColor
    case ePlayColor

    case eSliderTrackColor
    case eSliderNotchColor

    case eError
    case eErrorTextColor
    case eStatusTextColor

    case eLogTimeStamp
    case eLogEventText
    case eLogHighLightedEventText

    case ePOISearchResultText
    case ePOIHighlightBackground
    case ePOIText

    case eDisabled
    case eEnabled

    case eAvailable
    case eNotAvailable
}

public enum eFontElement : Int
{
    case ePOIResultsText
}

open class AppStyle
{
    public static func color(_ item : eColorElement) -> UIColor
    {
        switch item
        {
            case .eTitleBorderColor:         return UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 0.8)
            case .eButtonBorderColor:        return UIColor(hue: 0.0, saturation: 0.0, brightness: 0.8, alpha: 0.9)
            case .eBorderColor:              return UIColor(hue: 0.0, saturation: 0.0, brightness: 0.5, alpha: 0.5)
            case .eButtonColor:              return UIColor(hue: 1.0, saturation: 0.0, brightness: 1.0, alpha: 1.0)
            case .eRecordColor:              return UIColor(hue: 1.0, saturation: 1.0, brightness: 0.9, alpha: 1.0)
            case .ePlayColor:                return UIColor(hue: 0.66, saturation: 1.0, brightness: 0.5, alpha: 1.0)

            case .eSliderTrackColor:         return UIColor(white: 0.7, alpha: 0.6)
            case .eSliderNotchColor:         return UIColor(white: 0.6, alpha: 0.7)

            case .eError:                    return UIColor.red
            case .eErrorTextColor:           return UIColor.red
            case .eStatusTextColor:          return UIColor.blue

            case .eLogTimeStamp:             return UIColor.gray
            case .eLogEventText:             return UIColor.orange
            case .eLogHighLightedEventText:  return UIColor.purple

            case .ePOISearchResultText:     return UIColor(red:0.5, green: 0.5, blue: 0.45, alpha: 1.0)
            case .ePOIHighlightBackground:  return UIColor(red:0.8, green: 0.8, blue: 1.0, alpha: 0.1)
            case .ePOIText:                 return UIColor(red:0.2, green: 0.9, blue: 0.2, alpha: 1.0)

            case .eDisabled:                return UIColor.lightGray
            case .eEnabled:                 return UIColor(red:0.4, green: 0.8, blue: 0.5, alpha: 1.0)

            case .eAvailable:               return UIColor(red:0.2, green: 0.2, blue: 0.8, alpha: 1.0)
            case .eNotAvailable:            return UIColor(red:0.8, green: 0.2, blue: 0.2, alpha: 1.0)
        }
    }

    public static func font(_ item : eFontElement) -> UIFont
    {
        switch item
        {
            case .ePOIResultsText:

                return UIFont(name:"Helvetica", size:18.0)!

        }
    }

    class func styleSearchResults(_ inUITextView: UITextView)
    {
        // Round all corners.
        inUITextView.layer.cornerRadius = 10.0
        inUITextView.layer.borderWidth = 1.0
        inUITextView.layer.borderColor = AppStyle.color(.eBorderColor).cgColor

        inUITextView.textContainerInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        inUITextView.scrollRangeToVisible(NSRange(location: 0, length: 0))
    }

    class func styleBorder(inView view: UIView)
    {
        var borderColor = AppStyle.color(.eBorderColor)

        // Round all corners.
        view.layer.cornerRadius = 10.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = borderColor.cgColor

        if let button = view as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleBorder(inTextView textView: UIView)
    {
        var borderColor = AppStyle.color(.eTitleBorderColor)

        // Round all corners.
        textView.layer.cornerRadius = 10.0
        textView.layer.borderWidth = 0.7
        textView.layer.borderColor = borderColor.cgColor

        if let button = textView as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleBorder(inButtonView button: UIView)
    {
        var borderColor = AppStyle.color(.eButtonBorderColor)

        // Round all corners.
        button.layer.cornerRadius = 10.0
        button.layer.borderWidth = 2.0
        button.layer.borderColor = borderColor.cgColor

        if let button = button as? UIButton
        {
            // Border colour taken from the text colour.
            let textColor : UIColor = button.titleLabel!.textColor

            borderColor = textColor
        }
    }

    class func styleBorder(inTitleViewContainer: UIView)
    {
        let borderColor = AppStyle.color(.eBorderColor)

        inTitleViewContainer.layer.borderWidth = 1.0
        inTitleViewContainer.layer.borderColor = borderColor.cgColor
    }

    class func styleTextShadow(inTextView textView: UITextView)
    {
        guard let attributedText = textView.attributedText else { return }

        let mutableDescriptionText = NSMutableAttributedString(attributedString: attributedText)
        let shadow = NSShadow()

        shadow.shadowColor = UIColor.init(white: 0.4, alpha: 0.9)
        shadow.shadowBlurRadius = 1
        shadow.shadowOffset = CGSize(width: 0.25, height: 0.5)

        mutableDescriptionText.addAttribute(NSAttributedString.Key.shadow, value: shadow, range: NSRange(location: 0, length: attributedText.length))

        textView.attributedText = mutableDescriptionText
    }

    class func createSliderThumbImage(_ sliderSize : CGSize, trackHeight : CGFloat) -> UIImage
    {
        let sectorSize : CGFloat = 1.0 / 3.0
        let sliderWidth : CGFloat = sliderSize.width
        let sectorNotchWidth : CGFloat = sliderSize.width * 0.1
        let thumbWidth : CGFloat = sliderSize.height
        let trackCentreYPos : CGFloat = (sliderSize.height - trackHeight) / 2.0
        let notchColour : UIColor = AppStyle.color(eColorElement.eSliderNotchColor)
        let trackColour : UIColor = AppStyle.color(eColorElement.eSliderTrackColor)

        UIGraphicsBeginImageContext(sliderSize)

        let context : CGContext = UIGraphicsGetCurrentContext()!

        // Draw the track background.
        context.setFillColor(trackColour.cgColor)
        context.fill(CGRect(x: thumbWidth / 2, y: trackCentreYPos, width: sliderSize.width - thumbWidth, height: trackHeight))

        // Draw the notches in the track.
        context.setFillColor(notchColour.cgColor)

        context.fill(CGRect(x: thumbWidth / 2 + sliderWidth * (0.5 - sectorSize) - sectorNotchWidth / 2.0, y: trackCentreYPos, width: sectorNotchWidth, height: trackHeight))
        context.fill(CGRect(x: (sliderWidth - sectorNotchWidth) / 2.0, y: trackCentreYPos, width: sectorNotchWidth, height: trackHeight))
        context.fill(CGRect(x: -thumbWidth / 2 + sliderWidth * (0.5 + sectorSize) - sectorNotchWidth / 2.0, y: trackCentreYPos, width: sectorNotchWidth, height: trackHeight))

        let image = UIGraphicsGetImageFromCurrentImageContext()

        UIGraphicsEndImageContext()

        return image!
    }

    class func getPOIKeywordsPlaceHolderParagraphStyle(_ columnCount : Int) -> NSMutableParagraphStyle
    {
        let entryLength = 150
        let paraStyle = NSMutableParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle

        let terms = NSTextTab.columnTerminators(for: NSLocale.current)
        var tabs = [NSTextTab]()

        for i in 1..<columnCount
        {
            let options = [NSTextTab.OptionKey.columnTerminators: terms]
            let tab = NSTextTab(textAlignment:NSTextAlignment.left, location:CGFloat(entryLength * i), options:options)

            tabs.append(tab)
        }

        paraStyle.tabStops = tabs
        paraStyle.headIndent = 0
        paraStyle.alignment = NSTextAlignment.left

        return paraStyle
    }

    class func getFormattedPOIKeywordsPlaceHolder(_ columnCount : Int) -> NSAttributedString
    {
        let text                = NSMutableAttributedString()
        var currentColumnNumber = 0

        // Add POI Types with four columns.
        for poiKeyword in App.Settings.allPOIkeywords
        {
            let lineText = poiKeyword
            let nextColumnNumber = currentColumnNumber + 1
            let terminator = (nextColumnNumber % columnCount) == 0 ? "\n" : "\t"

            currentColumnNumber = nextColumnNumber
            text.append(NSAttributedString(string: lineText + terminator))
        }

        let paraStyle = getPOIKeywordsPlaceHolderParagraphStyle(columnCount)

        text.addAttribute(NSAttributedString.Key.paragraphStyle, value:paraStyle, range:NSMakeRange(0, text.length))

        return text
    }
}
