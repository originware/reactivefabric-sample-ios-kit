//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

import ReactiveFabric


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// App namespace, containing App Fabric definitions.
///

public struct App
{
    // See extensions.
}

extension App
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Constants related to the App operation.
    ///

    public struct Constant
    {
        static let ReachabilityHostname                             = "google-public-dns-a.google.com"
        static let SimulatedPosition                                = CLLocation(latitude: 52.205, longitude: 0.119)
        static let LocationChangeUpdateDistance: CLLocationDistance = 100
        static let ConfigScenario                                   = "production"
        static let POINearByDistance                                = CLLocationDistance(100)
        static let MinKeywordMatchLength                            = 3
        static let KeyboardDebounceInSeconds                        = 0.4
        static let LocationAndReachabilityFixTimeout: TimeInterval  = 9
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// App Error representation.
///

extension App
{
    public struct Error : IRfError
    {
        /// The type of error.
        public let errorDescription : String

        /// CustomDebugStringConvertible Protocol conformance.
        public var description: String              { return errorDescription }

        /// Initialise the error with the error description and optional context.
        /// - Parameter errorDescription: Describes the error.
        /// - Parameter context: The optional error context.
        public init(_ errorDescription: String)
        {
            self.errorDescription = errorDescription
        }

        public init(nsError: NSError)
        {
            self.errorDescription = nsError.description
        }
        
        public func isEqual(other : IRfEquatable) -> Bool
        {
            guard let otherError = other as? App.Error else { return false }

            return description == otherError.description
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// App Settings: The App Plist Settings.
///

extension App
{
    class Settings
    {
        /// The setting in the "Data.plist"
        class var allPOIkeywords: [String]
        {
            struct Settings: Codable
            {
                enum CodingKeys: String, CodingKey
                {
                    case POIKeywords = "POIKeywords"
                }

                var POIKeywords : [String]
            }

            guard let path = Bundle.main.path(forResource: "Data", ofType:"plist") else
            {
                fatalError("Data plist does not exist.")
            }

            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let decoder = PropertyListDecoder()

                return try decoder.decode(Settings.self, from: data).POIKeywords
            }
            catch
            {
                fatalError("Data plist not valid.")
            }
        }
    }
}
