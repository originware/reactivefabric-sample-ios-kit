//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone on 24/5/18.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Custom App Fabric Elements.
///
///   - The Reactive Fabric Elements employed in this POI demo sample app.
///   - Also includes the main app logic code.
///

extension App
{
    /// The devices used in the App (implemented as Sources).
    class Devices: Rf.Patterns.SubSystem<Void, AppEvent>
    {
        var location : LocationAppDevice
        var reachability : ReachabilityAppDevice

        init(_ traceID : Rf.TraceID, monitorNotifier: Rf.ANotifier<eAppOpEventType>)
        {
            self.location = LocationAppDevice(traceID.appending("location"))
            self.reachability = ReachabilityAppDevice(traceID.appending("reachability"), hostname: App.Constant.ReachabilityHostname)
            
            super.init(traceID)

            interconnect(consumer: location, producer: location.producer)
            interconnect(consumer: reachability, producer: reachability.producer)
        }
    }

    /// The view models used in the App (implemented as Collectors).
    class ViewModelCollectors: Rf.Patterns.Collector<AppEvent>
    {
        var locationStatusLabel:         LabelViewAdapter
        var reachabilityStatusLabel:     LabelViewAdapter
        var map:                         MapKitViewAdapter
        var keyboard:                    KeyboardDevice
        var confirm:                     ConfirmationViewAdapter
        var matchedKeywordsUICollection: UICollectionViewAdapter
        var keywordMatchResult:          KeywordResultsViewAdapter
        var logTextView:                 LogViewAdapter
        var appOperationView:            AppOperationView

        init(_ traceID : Rf.TraceID, viewController: AppViewController, logFormatter : AppStyle_LogFormatter, monitorNotifier: Rf.ANotifier<eAppOpEventType>)
        {
            self.locationStatusLabel = LabelViewAdapter(traceID.appending("locationStatus"), label: viewController.locationStatus)
            self.reachabilityStatusLabel = LabelViewAdapter(traceID.appending("reachabilityStatus"), label: viewController.reachabilityStatus)
            self.map = MapKitViewAdapter(traceID.appending("map"), mapView: viewController.mapView, monitor: monitorNotifier)
            self.matchedKeywordsUICollection = UICollectionViewAdapter(traceID.appending("matchedKeywords"), collectionView: viewController.foundPOITypesCollectionView)
            self.confirm = ConfirmationViewAdapter(traceID.appending("confirm"), viewController: viewController)
            self.keywordMatchResult = KeywordResultsViewAdapter(traceID.appending("keywordsMatch"), textView: viewController.searchResultsTextView, logFormatter: logFormatter, monitor: monitorNotifier)
            self.logTextView = LogViewAdapter(traceID.appending("log"), textView: viewController.appEventLogTextView, logFormatter: logFormatter, monitor: monitorNotifier)
            self.keyboard = KeyboardDevice(traceID.appending("keyboard"))
            self.appOperationView = viewController.appOperationView

            super.init(Rf.TraceID("/collectors/view"))

            self.consumer.onItem = { [weak self] (item: AppEvent, vTime: Rf.VTime?) in

                RfSDK.EvalQueues.UI.dispatch(async: { [weak self] in

                    guard let strongSelf = self else { return }

                    strongSelf.onNotifyInUIThread(item: item, vTime: vTime)
                })
            }
        }

        private func onNotifyInUIThread(item: AppEvent, vTime: Rf.VTime?)
        {
            switch item.appEventType
            {
                case .eOrientation:

                    logTextView.consumer.notify(item: item, vTime: vTime)

                case .eKeyboardChange:

                    logTextView.consumer.notify(item: item, vTime: vTime)

                case .eLocationChange:

                    locationStatusLabel.consumer.notify(item: item, vTime: vTime)
                    logTextView.consumer.notify(item: item, vTime: vTime)

                case .eNetworkReachabilityChange:

                    reachabilityStatusLabel.consumer.notify(item: item, vTime: vTime)
                    logTextView.consumer.notify(item: item, vTime: vTime)

                case .eUITextFieldChange:

                    logTextView.consumer.notify(item: item, vTime: vTime)

                case .eUIControlTouch:

                    logTextView.consumer.notify(item: item, vTime: vTime)
                    matchedKeywordsUICollection.consumer.notify(item: item, vTime: vTime)
                    map.consumer.notify(item: item, vTime: vTime)

                case .ePOIKeywordMatchSet:

                    keywordMatchResult.consumer.notify(item: item, vTime: vTime)
                    matchedKeywordsUICollection.consumer.notify(item: item, vTime: vTime)
                    logTextView.consumer.notify(item: item, vTime: vTime)

                case .ePOILocateRequest:

                    logTextView.consumer.notify(item: item, vTime: vTime)

                case .ePOILocateResult:

                    map.consumer.notify(item: item, vTime: vTime)

                case .ePresentTimeoutConfirmation:

                    break

                case .eSystemFailure:

                    break
            }
        }
    }

    /// The view inputs used in the App (implemented as Sources).
    class DeviceInputs
    {
        var searchTextField : UITextFieldAppSource
        var buttonTap :       UITouchAdapter

        init(_ traceID : Rf.TraceID, viewController: AppViewController, monitorNotifier: Rf.ANotifier<eAppOpEventType>)
        {
            self.searchTextField = UITextFieldAppSource(traceID.appending("searchTextField"), textField: viewController.searchTextField)
            self.buttonTap       = UITouchAdapter(traceID.appending("button"))
        }
    }

    /// The POI service used in the App (implemented as a Service).
    class POIService : Rf.Patterns.SubSystem<POIPortal.eRequest, Void>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        var poiPortal:    POIPortalAppSocket
        var googlePlaces: GooglePlacesPortalAppSocket
        var httpService:  HTTPAppService

        init(_ traceID : Rf.TraceID, monitorNotifier: Rf.ANotifier<eAppOpEventType>)
        {
            let poiPortal = POIPortalAppSocket(traceID.appending("POIPortal"), monitor: monitorNotifier)
            let googlePlaces = GooglePlacesPortalAppSocket(traceID.appending("google"), monitor: monitorNotifier)
            let httpService = HTTPAppService(traceID.appending("http"), monitor: monitorNotifier)
            let expression = poiPortal.compose(googlePlaces).compose(httpService)

            self.poiPortal = poiPortal
            self.googlePlaces = googlePlaces
            self.httpService = httpService

            super.init(traceID)
            
            interconnect(consumer: poiPortal, producer: expression.producer)
        }
    }

    /// The main logic expression of the App.
    class AppLogic
    {
        let fabric : Rf.AFabric                                     // The main fabric.
        let monitor : Rf.Notifiers.MultiTarget<eAppOpEventType>     // The event monitor that drives the app operational view.
        let logFormatter : AppStyle_LogFormatter                    // The textual log formatter.

        let devices :    Devices                                    // The app devices.
        let inputs :     DeviceInputs                               // The app inputs.
        let viewModels : ViewModelCollectors                        // The app view models.

        let keywordMatcher : POIKeywordMatcher                      // The POI keyword matcher.
        let poiService : POIService                                 // The POI service.

        init(viewController: AppViewController, errorNotifier: Rf.Notifiers.DelegateTarget<App.Error>)
        {
            self.fabric = RfSDK.factory.Fabric(Rf.TraceID("/fabrics/main"))
            self.monitor = Rf.Notifiers.MultiTarget<eAppOpEventType>(Rf.TraceID("/monitor"))
            self.logFormatter = AppStyle_LogFormatter()

            self.devices = Devices(Rf.TraceID("/devices"), monitorNotifier: monitor)
            self.inputs = DeviceInputs(Rf.TraceID("/inputs"), viewController: viewController, monitorNotifier: monitor)
            self.viewModels = ViewModelCollectors(Rf.TraceID("/viewModels"), viewController: viewController, logFormatter: logFormatter, monitorNotifier: monitor)

            self.keywordMatcher = POIKeywordMatcher(inputs.searchTextField.traceID.appending("keywordMatcher"), minSubstringLength: App.Constant.MinKeywordMatchLength, monitor: monitor)
            self.poiService = POIService(Rf.TraceID("/poiService"), monitorNotifier: monitor)
            
            do // Clear the text input and result fields.
            {
                inputs.searchTextField.clear()
                viewModels.keywordMatchResult.reset()
            }

            do // Register button taps.
            {
                inputs.buttonTap.registerForUITouch(control: viewController.clearSearchTextButton, touchTarget: eDeviceEvent_Touch.eTouch_clearSearchButton)
                inputs.buttonTap.registerForUITouch(control: viewController.appEventClearButton, touchTarget: eDeviceEvent_Touch.eTouch_clearLogButton)
                inputs.buttonTap.registerForUITouch(control: viewController.clearPOIMatchesButton, touchTarget: eDeviceEvent_Touch.eTouch_clearPOIKeywordMatchesButton)
                inputs.buttonTap.registerForUITouch(control: viewController.centreMapButton, touchTarget: eDeviceEvent_Touch.eTouch_centerMapButton)
            }

            do  // Compose inputs.
            {
                fabric.compose(inputs.searchTextField)
                fabric.compose(inputs.buttonTap)
                fabric.compose(devices)
            }

            do // Direct inputs to view models.
            {
                inputs.searchTextField.throttle(App.Constant.KeyboardDebounceInSeconds).compose(viewModels)
                inputs.buttonTap.observe(inputs.buttonTap.traceID.appending("observeTap"), item: { [weak self] (appEvent: AppEvent, vTime: Rf.VTime?) -> Void in

                    guard let strongSelf = self else { return }
                    
                    let (_, touchTarget) = appEvent.appEventType.touchControl

                    switch touchTarget
                    {
                        case .eTouch_clearSearchButton:
                            strongSelf.inputs.searchTextField.clear()
                            strongSelf.viewModels.keywordMatchResult.reset()

                        case .eTouch_clearPOIKeywordMatchesButton:
                            strongSelf.inputs.searchTextField.clear()
                            strongSelf.poiService.poiPortal.clearLocatedPOIKeywords()
                            strongSelf.viewModels.matchedKeywordsUICollection.clearPOIMatches()
                            strongSelf.viewModels.keywordMatchResult.reset()
                            strongSelf.viewModels.map.consumer.notify(item: appEvent, vTime: vTime)

                        case .eTouch_centerMapButton:
                            strongSelf.viewModels.map.consumer.notify(item: appEvent, vTime: vTime)

                        case .eTouch_clearLogButton:
                            strongSelf.viewModels.logTextView.consumer.notify(item: appEvent, vTime: vTime)
                    }
                })
            }

            do // Update map with location.
            {
                devices.location.observe(devices.location.traceID.appending("observe"), item: { [weak self] (appEvent: AppEvent, vTime: Rf.VTime?) in

                    guard let strongSelf = self else { return }

                    if case .eLocationChange(let locationChange) = appEvent.appEventType,
                       case .eLocation_LastKnown(let location) = locationChange
                    {
                        if appEvent.isSimulatedEvent
                        {
                            strongSelf.viewModels.confirm.consumer.notify(item: AppEvent(appEventType: .ePresentTimeoutConfirmation("As LocationServices are not available,\nthe location will be simulated as\nCambridge, UK")), vTime: vTime)   
                        }

                        strongSelf.viewModels.map.setLocation(location: location)
                    }
                })
            }

            do // Send monitor notifications to the appOperationView
            {
               monitor.onItem = { [weak self] (appOpEvent : eAppOpEventType, vTime: Rf.VTime?) in

                    guard let strongSelf = self else { return }

                    strongSelf.viewModels.appOperationView.notify(appOpEvent: appOpEvent, vTime: vTime)
                }
            }

            let searchTextFieldTraceID =  inputs.searchTextField.traceID

            // Main logic path.
            inputs.searchTextField.observe(searchTextFieldTraceID.appending("observeSearchText"), item: { [weak self] (appEvent: AppEvent, vTime: Rf.VTime?) in

                RfSDK.EvalQueues.UI.dispatch(async: {

                    guard let strongSelf = self else { return }
                    
                    let textFieldChange = appEvent.appEventType.textFieldChange

                    if appEvent.isSimulatedEvent
                    {
                        // Set the UITextField with the text to be replayed.
                        strongSelf.inputs.searchTextField.textField.attributedText = textFieldChange.attributedText
                    }

                    if textFieldChange.attributedText.string.count <= 2
                    {
                        // Present the standard POI Keywords placeholder in the search results UITextView.
                        strongSelf.viewModels.keywordMatchResult.reset()
                    }
                })
              })
              .compose(keywordMatcher)
              .throttle(App.Constant.KeyboardDebounceInSeconds)
              .observe(searchTextFieldTraceID.appending("observePOIKeyword"), item: { [weak self] (appEvent : AppEvent, vTime: Rf.VTime?) -> Void in

                  guard let strongSelf = self else { return }

                  // Present the matches on the keyword match UITextView
                  strongSelf.viewModels.consumer.notify(item: appEvent, vTime: vTime)
              })
              .poiRequestor(devices: devices,
                             viewModels: viewModels,
                             monitorNotifier: monitor,
                             timeout: App.Constant.LocationAndReachabilityFixTimeout
              )
              .compose(poiService)
        }
    }
}
