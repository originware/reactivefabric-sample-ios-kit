//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 13/09/2015.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The App Operational Scope request type
///

protocol IAppScopeModelRequest      {}
protocol IAppScopeModelReply        {}

protocol IAppScope
{
    func notify(item: IAppScopeModelRequest, vTime: Rf.VTime?)
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// AppScopeModel: Manages the App operating states: Platform, Presentation and Application.
///
/// - Platform scope handles Platform OS service startup and shutdown.
/// - Presentation scope handles view and view controller logic.
/// - Application scope handles application logic.
///

class AppScopeModel: Rf.Patterns.Service<IAppScopeModelRequest>
{
    typealias ePlatformRequest = AppScopeModel.Platform.eRequest
    typealias ePresentationRequest = AppScopeModel.Presentation.eRequest
    typealias eApplicationRequest = AppScopeModel.Application.eRequest

    /// The state of the AppScopeModel, which is passed to each scope.
    fileprivate struct State
    {
        var m_platformIsValid             = true
        var m_memoryIsLow                 = false
    }
    
    /// The scope stack represents the active Operational scopes.
    fileprivate class ScopeStack: Rf.ScopeStack<IAppScope>
    {
        var state = State()
    }

    /// The Platform-Scope manages the app OS Platform lifecycle and whether the Platform supports the apps requirements.
    struct Platform
    {
        enum eRequest : IAppScopeModelRequest
        {
            case eBeginScope((eReply, Rf.VTime?) -> Void)
            case eBeginPresentationScope
            case eLowMemoryCondition
            case eEndScope(App.Error?)
        }

        enum eReply : IAppScopeModelReply
        {
            case eError(App.Error)
            case eDidEndScope(App.Error?)
        }
    }

    class PlatformScope: IAppScope
    {
        typealias ReplyNotify = (Platform.eReply, Rf.VTime?) -> Void

        /// The scope stack reference for the platform scope.
        private var m_scopeStack : ScopeStack

        /// The reply closure to the initiator of the AppScopeModel.
        private var m_replyNotify : ReplyNotify!

        fileprivate init(scopeStack: ScopeStack)
        {
            self.m_scopeStack = scopeStack
        }

        func notify(item: IAppScopeModelRequest, vTime: Rf.VTime?)
        {
            guard m_scopeStack.state.m_platformIsValid else
            {
                m_replyNotify?(.eError(App.Error("Invalid platform")), vTime)
                return
            }
            
            switch item
            {
                case ePlatformRequest.eBeginScope(let replyNotify):

                    m_replyNotify = replyNotify
                    
                case ePlatformRequest.eBeginPresentationScope:

                    /// Push the Presentation Scope onto the scope stack.
                    func pushPresentationScope(_ replyNotify : @escaping (AppScopeModel.Presentation.eReply, Rf.VTime?) -> Void)
                    {
                        let presentationScope = PresentationScope(scopeStack: m_scopeStack)

                        m_scopeStack.push(presentationScope)

                        presentationScope.notify(item: Presentation.eRequest.eBeginScope(replyNotify), vTime: vTime)
                    }

                    pushPresentationScope({ [weak self] (reply: AppScopeModel.Presentation.eReply, vTime: Rf.VTime?) in

                        guard let strongSelf = self else { return }
                        
                        switch reply
                        {
                            case .eError(let error):

                                strongSelf.m_replyNotify(.eError(error), vTime)

                            case .eDidEndScope(let error):

                                strongSelf.m_scopeStack.pop()
                                strongSelf.m_replyNotify?(Platform.eReply.eDidEndScope(error), vTime)
                        }
                    })

                case ePlatformRequest.eLowMemoryCondition:

                    m_scopeStack.state.m_memoryIsLow = true

                case ePlatformRequest.eEndScope(let error):

                    if m_scopeStack.stack.count > 1
                    {
                        m_scopeStack.stack[1].notify(item: AppScopeModel.Presentation.eRequest.eEndScope(error), vTime: vTime)
                    }
                    else
                    {
                        m_replyNotify?(Platform.eReply.eDidEndScope(error), vTime)
                    }

                default:
                    
                    assertionFailure("PlatformScope: Invalid request: \(item)")

            }
        }
    }

    /// The Presentation-Scope manages the presentation lifecycle.
    struct Presentation
    {
        enum eRequest : IAppScopeModelRequest
        {
            case eBeginScope((eReply, Rf.VTime?) -> Void)
            case eBeginOperationalScope(AppViewController)
            case eEndScope(App.Error?)
        }

        enum eReply : IAppScopeModelReply
        {
            case eError(App.Error)
            case eDidEndScope(App.Error?)
        }
    }

    class PresentationScope : IAppScope
    {
        typealias ReplyNotify = (Presentation.eReply, Rf.VTime?) -> Void

        /// The scope stack reference for the presentation scope.
        private var m_scopeStack : ScopeStack

        /// The reply closure to the Platform scope.
        private var m_replyNotify : ReplyNotify!

        fileprivate init(scopeStack: ScopeStack)
        {
            self.m_scopeStack = scopeStack
        }

        func notify(item: IAppScopeModelRequest, vTime: Rf.VTime?)
        {
            switch item
            {
                case ePresentationRequest.eBeginScope(let replyNotify):

                    m_replyNotify = replyNotify

                case ePresentationRequest.eBeginOperationalScope(let viewController):

                    func pushApplicationScope(_ notifyReply : @escaping ApplicationScope.ReplyNotify)
                    {
                        let operationalScope = ApplicationScope(scopeStack: m_scopeStack)

                        m_scopeStack.push(operationalScope)

                        operationalScope.notify(item:  Application.eRequest.eBeginScope(viewController, notifyReply), vTime: vTime)
                    }

                    pushApplicationScope({ [weak self] (reply: Application.eReply, vTime: Rf.VTime?) in

                        guard let strongSelf = self else { return }
                        
                        switch reply
                        {
                            case .eError(let error):

                                strongSelf.m_replyNotify(.eError(error), vTime)

                            case .eDidEndScope(let error):

                                strongSelf.m_scopeStack.pop()
                                strongSelf.m_replyNotify?(Presentation.eReply.eDidEndScope(error), vTime)
                        }
                    })

                case ePlatformRequest.eLowMemoryCondition:

                    m_scopeStack.state.m_memoryIsLow = true
                    
                case ePresentationRequest.eEndScope(let error):

                    if m_scopeStack.stack.count > 2
                    {
                        m_scopeStack.stack[2].notify(item: AppScopeModel.Presentation.eRequest.eEndScope(error), vTime: vTime)
                    }
                    else
                    {
                        m_replyNotify?(Presentation.eReply.eDidEndScope(error), vTime)
                    }

                default:

                    assertionFailure("PresentationScope: Invalid request: \(item)")
            }
        }
    }

    /// The Application-Scope manages the application behaviour lifecycle.
    struct Application
    {
        enum eRequest : IAppScopeModelRequest
        {
            case eBeginScope(AppViewController, (eReply, Rf.VTime?) -> Void)
            case eEndScope(App.Error?)
        }

        enum eReply : IAppScopeModelReply
        {
            case eError(App.Error)
            case eDidEndScope(App.Error?)
        }
    }
    
    class ApplicationScope : IAppScope
    {
        typealias ReplyNotify = (Application.eReply, Rf.VTime?) -> Void

        /// The scope stack reference for the application scope.
        private var m_scopeStack :    ScopeStack

        /// The reply closure to the Presentation scope.
        private var m_replyNotify:    ReplyNotify!

        /// The error notifier used in the app logic.
        private var m_errorNotifier                          = Rf.Notifiers.DelegateTarget<App.Error>(Rf.TraceID("/notifiers/error"))

        /// The app logic and macro behaviour.
        private var m_appLogic:       App.AppLogic?          = nil

        /// The scheduling tick source. Represents the synchronised clock for the Reactive Fabric.
        private var m_mainTickSource: Rf.Sockets.TickSource? = nil

        fileprivate init(scopeStack: ScopeStack)
        {
            self.m_scopeStack = scopeStack
        }

        func notify(item: IAppScopeModelRequest, vTime: Rf.VTime?)
        {
            switch item
            {
                case eApplicationRequest.eBeginScope(let (viewController, replyNotify)):

                    m_replyNotify = replyNotify

                    // Direct errors down the app scope stack, to the initiator of the application.
                    m_errorNotifier.onItem = { (error, vTime) in

                        replyNotify(.eError(error), vTime)
                    }

                    // Application scope elements.
                    let appLogic = App.AppLogic(viewController: viewController, errorNotifier: m_errorNotifier)
                    let tickSource = Rf.Sockets.TickSource(Rf.TraceID("/tickSources/main"), vTimeEpoch: Date())

                    m_appLogic = appLogic
                    m_mainTickSource = tickSource

                    // Trace Reactive Fabric operations to standard out.
                    appLogic.fabric.trace()

                    // Install the tick source for scheduling capability in specific elements.
                    appLogic.fabric.install(tool: .eScheduler(["POIRequestor", "throttle",
                                                                 "location", "reachability", "orientation", "keyboard",
                                                                 "searchTextField", "button", "keywordMatcher",
                                                                 "http"], "scheduler", tickSource))

                    // Initiated the Reactive Fabric.
                    appLogic.fabric.beginEval(.eAsync(nil), vTime: vTime)

                case ePlatformRequest.eLowMemoryCondition:

                    m_scopeStack.state.m_memoryIsLow = true

                case eApplicationRequest.eEndScope(let error):

                    // Stop any confirmations from being presented.
                    m_appLogic?.viewModels.confirm.enabled = false

                    // Terminate the fabric and cessate application activity.
                    m_appLogic!.fabric.endEval(vTime: vTime)
                    m_appLogic = nil

                    m_replyNotify?(Application.eReply.eDidEndScope(error),  vTime)

                default:

                    assertionFailure("ApplicationScope: Invalid request: \(item)")
            }
        }
    }

    /// The scope stack stores the active Operational scopes.
    private let m_scopeStack = ScopeStack()

    init()
    {
        super.init(Rf.TraceID("OperationalModel"))

        let platformScope = PlatformScope(scopeStack: m_scopeStack)

        m_scopeStack.push(platformScope)

        self.consumer.onItem = { [weak self] (item: IAppScopeModelRequest, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            strongSelf.m_scopeStack.stack.last!.notify(item: item, vTime: vTime)
        }
    }
}

