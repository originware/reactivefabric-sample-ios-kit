//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 1/08/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class ExpandAnimatedTransition: NSObject, UIViewControllerAnimatedTransitioning
{
    var duration = 0.0
    var isPresenting = true
    var originFrame = CGRect.zero

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?)-> TimeInterval
    {
        return isPresenting ? duration : duration * 3
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning)
    {
        let containerView = transitionContext.containerView
        let fromView = transitionContext.view(forKey: UITransitionContextViewKey.from)!
        let toView = transitionContext.view(forKey: UITransitionContextViewKey.to)!
        let animatedView = isPresenting ? toView : fromView

        let startFrame = isPresenting ? originFrame : fromView.frame
        let endFrame = isPresenting ? toView.frame : originFrame

        let xScaleFactor = isPresenting ? startFrame.width / endFrame.width : endFrame.width / startFrame.width
        let yScaleFactor = isPresenting ? startFrame.height / endFrame.height :  endFrame.height / startFrame.height
        let scaleTransform = CGAffineTransform(scaleX: xScaleFactor, y: yScaleFactor)

        if isPresenting
        {
            animatedView.transform = scaleTransform
            animatedView.center = CGPoint(x: startFrame.midX, y: startFrame.midY)
            animatedView.clipsToBounds = true
            animatedView.alpha = 0.0
        }

        containerView.addSubview(toView)
        containerView.bringSubviewToFront(animatedView)

        UIView.animate(

            withDuration: duration,
            delay:0.0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: isPresenting ? 10.0 : -5.0,
            options:isPresenting ? UIView.AnimationOptions.curveEaseOut : UIView.AnimationOptions.curveEaseIn,
            animations: {

                animatedView.transform = self.isPresenting ? CGAffineTransform.identity : scaleTransform
                animatedView.center = CGPoint(x: endFrame.midX, y: endFrame.midY)
                animatedView.alpha = self.isPresenting ? 1.0 : 0.2

           }, completion:{_ in

               transitionContext.completeTransition(true)
        })
    }
}
