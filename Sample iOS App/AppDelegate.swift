//
// Reactive Fabric iOS Demo App.
//
//  Created by Terry Stillone on 16/06/15.
//  Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import UIKit
import ReactiveFabric

@UIApplicationMain class AppDelegate: UIResponder, UIApplicationDelegate
{
    var window: UIWindow?
    
    let operationalModel = AppScopeModel()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Create the OperationalModel, which initiates the StartUp scope and pass it onto the root View Controller.
        if let navigationController = window?.rootViewController as? UINavigationController
        {
            // Create the run context that controls operational tracing (for debugging).
            for viewController in navigationController.viewControllers
            {
                switch viewController
                {
                    case let appIntroViewController as AppIntroScreenViewController:
                        appIntroViewController.appOperationScopeController = operationalModel

                    case let appViewController as AppViewController:
                        appViewController.operationalModel = operationalModel

                    default: break
                }
            }

            operationalModel.notify(item: AppScopeModel.Platform.eRequest.eBeginScope({ (reply, vTime) in

                switch reply
                {
                    case .eError(let error):

                        print(">> Diagnostics: error: \(error)")

                    case .eDidEndScope(let error):

                        if let error = error
                        {
                            print(">> Diagnostics: end of platform scope with error: \(error)")
                        }
                }
                
            }), vTime: nil)
        }
        else
        {
            fatalError("Cannot get Root View Controller")
        }

        return true
    }

    func applicationWillResignActive(_ application: UIApplication)
    {
    }

    func applicationDidEnterBackground(_ application: UIApplication)
    {
    }

    func applicationWillEnterForeground(_ application: UIApplication)
    {
    }

    func applicationDidBecomeActive(_ application: UIApplication)
    {
    }

    func applicationWillTerminate(_ application: UIApplication)
    {
    }

    func applicationDidReceiveMemoryWarning(_ application: UIApplication)
    {
        // Notify operational model of a low memory condition.
        operationalModel.notify(item: AppScopeModel.Platform.eRequest.eLowMemoryCondition, vTime: nil)
    }
}
