//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 13/07/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import Foundation.NSURLSession

import ReactiveFabric

struct HTTP
{
    enum eRequest
    {
        case eBeginScope((eReply, Rf.VTime?) -> Void)
        case eHTTPRequest(URLRequestReply)
        case eEndScope(App.Error?)
    }

    enum eReply : CustomStringConvertible
    {
        case eDidBeginScope
        case eSuccess(URLRequestReply)
        case eFailure(App.Error)
        case eDidEndScope
        
        var description: String
        {
            switch self
            {
                case .eDidBeginScope:            return "DidBeginScope"
                case .eSuccess(let reply):       return "Success(\(reply))"
                case .eFailure(let error):       return "Failure(\(error))"
                case .eDidEndScope:              return "DidEndScope"
            }
        }
    }
}

class HTTPAppService: Rf.Patterns.Service<HTTP.eRequest>
{
    typealias ReplyNotify = (HTTP.eReply, Rf.VTime?) -> Void

    /// The scheduling tool.
    public var scheduleTool : Rf.Tools.Schedule? = nil

    fileprivate var m_replyNotify : ReplyNotify!

    /// The monitor of the device notifications.
    private let m_monitorNotifier: Rf.ANotifier<eAppOpEventType>
    
    /// Initialise with trace ID and operation monitor.
    init(_ traceID: Rf.TraceID, monitor: Rf.ANotifier<eAppOpEventType>)
    {
        self.m_monitorNotifier = monitor
        
        super.init(traceID)

        self.consumer.onItem = { [weak self] (item: HTTP.eRequest, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch item
            {
                case .eBeginScope(let replyNotify):

                    strongSelf.m_replyNotify = replyNotify

                    strongSelf.m_replyNotify(.eDidBeginScope, strongSelf.scheduleTool?.currentVTime)

                case .eHTTPRequest(let request):

                    // HTTP request AppEvent monitoring notification.
                    strongSelf.m_monitorNotifier.notify(item: .eFromGooglePlacesService_To_HTTPService, vTime: strongSelf.scheduleTool?.currentVTime)

                    // Handle the request from Google Services.
                    strongSelf.handleHTTPRequest(request, strongSelf.m_replyNotify, vTime: vTime)

                case .eEndScope:

                    strongSelf.m_replyNotify(.eDidEndScope, strongSelf.scheduleTool?.currentVTime)
            }
        }
    }

    func handleHTTPRequest( _ request: URLRequestReply, _  notifyReply: @escaping ReplyNotify, vTime: Rf.VTime?)
    {
        // Create a URL Session HTTP request.
        let dataTask: URLSessionDataTask = URLSession.shared.dataTask(with: request.url) { [weak self] (data: Data?, response: URLResponse?, error: Error?) in

            guard let strongSelf = self else { return }
            
            func processHTTPRequest() -> HTTP.eReply
            {
                // HTTP request AppEvent monitoring notification.
                strongSelf.m_monitorNotifier.notify(item: .eFromHTTPService_To_GooglePlacesService, vTime: nil)

                switch (error, response)
                {
                    case (nil, let httpResponse as HTTPURLResponse):

                        let byteSize = data != nil ? data!.count : 0
                        
                        strongSelf.tracing.outputTrace(message: "    Reply: received: \(byteSize) bytes", vTime: vTime)

                        // NSURLSession issued a reply.
                        return handleHTTPReply(data: data, httpResponse: httpResponse, request: request)

                    case (let httpError?, _):

                        strongSelf.tracing.outputTrace(message: "    Reply: failure: \(httpError)", vTime: vTime)

                        // NSURLSession issued a direct error.
                        return HTTP.eReply.eFailure(App.Error(nsError: httpError as NSError))

                    default:

                        // NSURLSession didn't get a reply within the expected time.
                        let errorDict = [NSLocalizedDescriptionKey: "URLSessionPeer: bad response"]
                        let nsError     = NSError(domain: "URLSessionPeer", code: 0, userInfo: errorDict)

                        strongSelf.tracing.outputTrace(message: "    Reply: failure: no reply within timeout period", vTime: vTime)
                        
                        return HTTP.eReply.eFailure(App.Error(nsError: nsError))
                }
            }

            func handleHTTPReply(data: Data?, httpResponse: HTTPURLResponse, request: URLRequestReply) -> HTTP.eReply
            {
                switch httpResponse.statusCode
                {
                    case 201, 200, 401:

                        if let data = data
                        {
                            let urlReply = URLRequestReply(request: request, replyData: data)

                            return HTTP.eReply.eSuccess(urlReply)
                        }

                    default: break
                }

                let errorDict = [NSLocalizedDescriptionKey : "URLSession: nil or invalid JSON response"]
                let nsError = NSError(domain: "URLSession", code: 0, userInfo: errorDict)

                return HTTP.eReply.eFailure(App.Error(nsError: nsError))
            }

            let reply = processHTTPRequest()
            let now = strongSelf.scheduleTool?.currentVTime

            notifyReply(reply, now)
        }

        // Perform the HTTP request.
        dataTask.resume()
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// URLRequestReply: The HTTPService request/reply entity.
///

class URLRequestReply : CustomStringConvertible
{
    /// The URL of the request/reply.
    let url : URL

    /// The http request data.
    let requestData : Any?

    /// The http replay data.
    let replyData: Any?

    /// Indicator of error during the request/reply operation.
    let error : NSError?

    var hasError : Bool { return error != nil }

    /// CustomStringConvertible conformance.
    var description: String
    {
        guard let jsonAsData = replyData as? Data else { return "Invalid JSON" }

        let jsonDataSource = CrispJSON.JDataSource(jsonAsData)
        let jsonParser = CrispJSON.JParser(jsonDataSource)

        return jsonParser.json ?? "Invalid JSON"
    }

    /// Initialise with url, request data, reply data and optional error.
    /// - Parameter url: The url of the HTTP request.
    /// - Parameter requestData: The HTTP request data.
    /// - Parameter replyData: The HTTP reply data.
    /// - Parameter error: Optional error.
    init(url: URL, requestData: Any? = nil, replyData: Any? = nil, error: Error? = nil)
    {
        self.url = url
        self.requestData = requestData
        self.replyData = replyData
        self.error = error as NSError?
    }

    /// Initialise with the original request and new reply data.
    /// - Parameter request: The original request.
    /// - Parameter replyData: The HTTP reply data.
    init(request: URLRequestReply, replyData: Any)
    {
        self.url = request.url
        self.requestData = request.requestData
        self.replyData = replyData
        self.error = request.error
    }
}
