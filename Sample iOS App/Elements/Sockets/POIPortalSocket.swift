//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 30/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

import ReactiveFabric


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIPortalSocket: The Socket POI Portal Service that queries the Google Places HTTP portal.
///

/// The Request Scope handles requests.
struct POIPortal
{
    typealias ReplyNotify = (eReply, Rf.VTime?) -> Void

    enum eRequest
    {
        case eBeginScope(ReplyNotify)
        case ePOIPortalRequest(MultiPOILocateEvent_Request)
        case eEndScope
    }

    enum eReply
    {
        case eDidBeginScope
        case eRequestSuccess(AppEvent)
        case eRequestFailure(AppEvent)
        case eError(App.Error)
        case eDidEndScope
    }
}

class POIPortalAppSocket: Rf.Patterns.Socket<POIPortal.eRequest, GooglePlacesPortal.eRequest>
{
    typealias ReplyNotify = (POIPortal.eReply, Rf.VTime?) -> Void

    private let m_requestTracker = POIPortalActiveRequestTracker()
    private var m_replyNotify: ReplyNotify!

    /// The monitor of the device notifications.
    let m_monitorNotifier: Rf.ANotifier<eAppOpEventType>
    
    /// Initialise with the run context..
    init(_ traceID: Rf.TraceID, monitor: Rf.ANotifier<eAppOpEventType>)
    {
        self.m_monitorNotifier = monitor
        
        super.init(traceID)

        self.consumer.onItem = { [weak self] (item: POIPortal.eRequest, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch item
            {
                case .eBeginScope(let (replyNotify)):

                    strongSelf.m_replyNotify = replyNotify

                    // Issue the request to the Google Places Service.
                    strongSelf.producer.notify(item: GooglePlacesPortal.eRequest.eBeginScope({ [weak self] (reply, vTime) in

                        guard let strongSelf = self else { return }

                        // Handle the reply from the Google Places Service.

                        switch reply
                        {
                            case .eDidBeginScope:

                                replyNotify(.eDidBeginScope, vTime)

                            case .eRequestSuccess(let urlReply):

                                strongSelf.m_requestTracker.updateFromReply(urlReply)

                                let locateResultAppEvent = AppEvent(appEventType: eAppEventType.ePOILocateResult(urlReply))

                                strongSelf.tracing.outputTrace(message: "    Reply: \(urlReply)", vTime: vTime)
                                replyNotify(POIPortal.eReply.eRequestSuccess(locateResultAppEvent), vTime)

                                // POIPortal Reply AppEvent monitoring notification.
                                strongSelf.m_monitorNotifier.notify(item: .eFromGooglePlaces_To_POIService, vTime: vTime)

                            case .eRequestFailure(let urlReply):

                                let locateResultAppEvent = AppEvent(appEventType: eAppEventType.ePOILocateResult(urlReply))

                                replyNotify(POIPortal.eReply.eRequestSuccess(locateResultAppEvent), vTime)

                            case .eError(let error):

                                replyNotify(POIPortal.eReply.eError(error), vTime)

                            case .eDidEndScope:

                                replyNotify(.eDidEndScope, vTime)
                        }

                    }), vTime: vTime)

                case .ePOIPortalRequest(let multiRequest):

                    for poiKeyword in multiRequest.poiKeywords where !strongSelf.m_requestTracker.isActive(poiKeyword: poiKeyword, location : multiRequest.location)
                    {
                        let request = POILocateEvent_Request(poiKeyword: poiKeyword, requestedPOIKeywords: multiRequest.poiKeywords, location : multiRequest.location)

                        strongSelf.producer.notify(item: GooglePlacesPortal.eRequest.ePOIPortalRequest(request), vTime: vTime)

                        // POIPortal Request AppEvent monitoring notification.
                        strongSelf.m_monitorNotifier.notify(item: .eFromPOIRequestor_To_POIPortal, vTime: vTime)
                    }

                case .eEndScope:

                    strongSelf.m_replyNotify(.eDidEndScope, vTime)
            }
        }
    }

    func clearLocatedPOIKeywords()
    {
        m_requestTracker.clear()
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIPortalActiveRequestTracker: The tracker of active POI requests.
///

fileprivate class POIPortalActiveRequestTracker
{
    private var m_activePOIKeywords = Set<String>()
    private var m_lastLocation: CLLocation? = nil

    /// Indicate if the given keyword is active.
    /// - Parameter poiKeyword: The keyword is check.
    func isActive(poiKeyword: String, location: CLLocation) -> Bool
    {
        let keywordIsActive = m_activePOIKeywords.contains(poiKeyword)

        return isNearLastLocation(location) && keywordIsActive
    }

    /// Update the active keywords state from the given locate request.
    /// - Parameter poiLocateRequest: The POI Locate request to update state from.
    func updateFromReply(_ poiLocateResult : POILocateEvent_Reply)
    {
        m_activePOIKeywords.formUnion(Set<String>(poiLocateResult.requestedKeywords))
        m_lastLocation = poiLocateResult.location
    }

    /// Clear all active keywords in state.
    func clear()
    {
        m_activePOIKeywords.removeAll()
    }

    /// Indicate if the given location is near the last known location.
    /// - Parameter location: The location to check.
    fileprivate func isNearLastLocation(_ location : CLLocation) -> Bool
    {
        return (m_lastLocation == nil) || (m_lastLocation!.distance(from: location) < App.Constant.POINearByDistance)
    }
}
