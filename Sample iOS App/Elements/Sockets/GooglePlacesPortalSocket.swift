//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 22/09/2015.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// GooglePlacesPortalService: The POI Portal Socket that queries the Google Places HTTP portal for points of interest.
///

struct GooglePlacesPortal
{
    fileprivate struct Constant
    {
        static let GoogleAPIKey     = "<API Key available on request>"
        static let PortalName       = "Google Places"
        static let PortalBaseURL    = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
        static let POILocateRadius  = 1000
    }

    enum eRequest
    {
        case eBeginScope((eReply, Rf.VTime?) -> Void)
        case ePOIPortalRequest(POILocateEvent_Request)
        case eEndScope
    }

    enum eReply
    {
        case eDidBeginScope
        case eRequestSuccess(POILocateEvent_Reply)
        case eRequestFailure(POILocateEvent_Reply)
        case eError(App.Error)
        case eDidEndScope
    }
}

class GooglePlacesPortalAppSocket: Rf.Patterns.Socket<GooglePlacesPortal.eRequest, HTTP.eRequest>
{
    typealias ReplyNotify = (GooglePlacesPortal.eReply, Rf.VTime?) -> Void

    enum JSONResult
    {
        case eSuccess([POI])
        case eError(App.Error)
        case eCouldNotExtractData
    }

    fileprivate var m_replyNotify :    ReplyNotify!

    /// The monitor of the device notifications.
    private let     m_monitorNotifier: Rf.ANotifier<eAppOpEventType>
    
    /// Initialise with trace ID and operation monitor.
    init(_ traceID: Rf.TraceID, monitor: Rf.ANotifier<eAppOpEventType>)
    {
        self.m_monitorNotifier = monitor
        
        super.init(traceID)

        self.onItem = { [weak self] (item: GooglePlacesPortal.eRequest, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch item
            {
                case .eBeginScope(let (replyNotify)):

                    strongSelf.m_replyNotify = replyNotify

                    strongSelf.producer.notify(item: HTTP.eRequest.eBeginScope({ [weak self] (reply, vTime) in

                        guard let strongSelf = self else { return }

                        switch reply
                        {
                            case .eDidBeginScope:

                                replyNotify(.eDidBeginScope, vTime)

                            case .eSuccess(let urlReply):

                                // POIPortal Reply AppEvent monitoring notification.
                                strongSelf.m_monitorNotifier.notify(item: .eFromHTTPService_To_GooglePlacesService, vTime: vTime)

                                strongSelf.tracing.outputTrace(message: "    Reply: \(urlReply)", vTime: vTime)
                                replyNotify(strongSelf.handleHTTPReply(urlReply), vTime)

                            case .eFailure(let error):

                                replyNotify(GooglePlacesPortal.eReply.eError(error), vTime)

                            case .eDidEndScope:

                                replyNotify(.eDidEndScope, vTime)
                        }

                    }), vTime: vTime)

                case .ePOIPortalRequest(let request):

                    // POIPortal Request AppEvent monitoring notification.
                    strongSelf.m_monitorNotifier.notify(item: .eFromPOIPortalService_To_GooglePlacesService, vTime: vTime)

                    // Encode the Google Places JSON and emit to HTTPServices.
                    if let httpRequest = strongSelf.handlePOIPortalRequest(request)
                    {
                        strongSelf.producer.notify(item: HTTP.eRequest.eHTTPRequest(httpRequest), vTime: vTime)
                    }
                    else
                    {
                        strongSelf.m_replyNotify?(GooglePlacesPortal.eReply.eError(App.Error("POI Portal request could not be formed: invalid POI Request data.")), vTime)
                    }

                case .eEndScope:

                    strongSelf.m_replyNotify?(.eDidEndScope, vTime)
            }
        }
    }

    func handlePOIPortalRequest(_ request: POILocateEvent_Request) -> URLRequestReply?
    {
        func generateURLAsString(_ poiKeyword: String) -> String?
        {
            let coordinate          = request.location.coordinate
            let urlAsString         = "\(GooglePlacesPortal.Constant.PortalBaseURL)?location=\(coordinate.latitude),\(coordinate.longitude)&radius=\(GooglePlacesPortal.Constant.POILocateRadius)&type=\(poiKeyword)&key=\(GooglePlacesPortal.Constant.GoogleAPIKey)"

            return urlAsString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        }

        guard let urlAsString = generateURLAsString(request.poiKeyword), let url = URL(string: urlAsString) else { return nil }

        return URLRequestReply(url: url, requestData: request, replyData: nil)
    }

    func handleHTTPReply(_ urlRequestReply: URLRequestReply) -> GooglePlacesPortal.eReply
    {
        guard let poiLocateRequest = urlRequestReply.requestData as? POILocateEvent_Request
        else { return .eError(App.Error("Internal Error: cannot get original POI request")) }

        guard urlRequestReply.error == nil
        else { return .eRequestFailure(POILocateEvent_Reply(locateSourceName: GooglePlacesPortal.Constant.PortalName, requestPOIKeywords: [poiLocateRequest.poiKeyword], location : poiLocateRequest.location, error: urlRequestReply.error!)) }

        guard let jsonAsData = urlRequestReply.replyData as? Data
        else { return .eError(App.Error("Internal Error: cannot get HTTP reply data")) }

        let jsonDataSource = CrispJSON.JDataSource(jsonAsData)
        let jsonParser = CrispJSON.JParser(jsonDataSource)

        guard let result = extractPOIsFromJSON(jsonParser, requestedPOIKeywords: poiLocateRequest.requestedPOIKeywords)
                else { return .eError(App.Error("Unknown internal error.")) }
                
        switch result
        {
            case .eSuccess(let pois):

                let locateReply = POILocateEvent_Reply(locateSourceName: GooglePlacesPortal.Constant.PortalName, requestPOIKeywords: [poiLocateRequest.poiKeyword], location : poiLocateRequest.location, pois: pois)

                return .eRequestSuccess(locateReply)

            case .eError(let error):

                return .eError(error)

            case .eCouldNotExtractData:

                return .eError(App.Error("Cannot extract JSON from HTTP reply"))
        }
    }

    func extractPOIsFromJSON(_ jsonParser: CrispJSON.JParser, requestedPOIKeywords: Set<String>) -> JSONResult?
    {
        func remark(_ message: String)
        {
            // print(message)
        }

        func getStatusMessage(_ json: CrispJSON.JTree) -> String?
        {
            guard let errorMessage = json.getContent(named: "status") as? String else { return nil }

            return errorMessage
        }

        func getErrorMessage(_ json: CrispJSON.JTree, defaultError: String) -> App.Error
        {
            guard let errorMessage = json.getContent(named: "error_message") as? String else 
            {
                return App.Error(defaultError)
            }

            return App.Error(errorMessage)
        }

        return jsonParser.parse({ (json) in

            var pois = [POI]()

            json["results"]?.forArray({ (result) in

                guard let name          = result ->> "name" ->> JValue<String>.value     else { remark("name field missing"); return }
                guard let address       = result ->> "vicinity" ->> JValue<String>.value else { remark("address field missing"); return }
                let keywordTypes        = result ->> "types" ->> JValue<[String]>.value

                if let coordinate = result ->> "geometry" ->> "location"
                {
                    guard let lat = coordinate ->> "lat" ->> JValue<Double>.value else { remark("lat field"); return }
                    guard let lng = coordinate ->> "lng" ->> JValue<Double>.value else { remark("lng field"); return }

                    let keywordTypeSet = keywordTypes != nil ? Set<String>(keywordTypes!) : requestedPOIKeywords
                    let commonKeywords = keywordTypeSet.intersection(requestedPOIKeywords)

                    if !commonKeywords.isEmpty
                    {
                        for poiKeyword in commonKeywords
                        {
                            let location = CLLocation(latitude: lat, longitude:lng)
                            let poi = POI(poiKeyword: poiKeyword, location: location, name: name, address: address)

                            pois.append(poi)
                        }
                    }
                }

            })

            if let statusValue = getStatusMessage(json)
            {
                switch statusValue
                {
                    case "OK", "ZERO_RESULTS":

                        return .eSuccess(pois)

                    case "OVER_QUERY_LIMIT":

                        let error = getErrorMessage(json, defaultError: "You have reached the Google places quota threshold for the day.")

                        return .eError(error)

                    default:

                        let error = getErrorMessage(json, defaultError: "Unknown error, Goodle Places reply was: \(statusValue)")

                        return .eError(error)
                }
            }

            return .eCouldNotExtractData
        })
    }
}
