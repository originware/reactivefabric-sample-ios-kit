//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import SystemConfiguration

import ReactiveFabric


@available(iOS 8, *)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ReachabilityDevice: The Device for SCNetworkReachability events in the form of AppEvent notifications.
///

class ReachabilityAppDevice: Rf.Patterns.Source<AppEvent>, IRfScheduling
{
    typealias eEvalNotify = Rf.EvalScope.eNotify

    /// The host name to check reachability to.
    fileprivate var m_hostname: String

    /// The SCNetworkReachability handler.
    fileprivate var m_reachabilityHandler: SCNReachabilityHandler? = nil

    /// Indicator of current reachability.
    var isReachable : Bool { return m_reachabilityHandler?.lastReachabilityEvent == .eReachability_IsReachable }

    /// The scheduler for timeout determination.
    public var  scheduleTool :     Rf.Tools.Schedule? = nil

    /// Initialise with trace ID and reachability host name.
    /// - Parameter traceID : The device trace ID.
    /// - Parameter hostname: The reachability host name.
    init(_ traceID: Rf.TraceID, hostname: String)
    {
        self.m_hostname = hostname

        super.init(traceID)

        self.onControl = { [weak self] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType):

                    if strongSelf.m_reachabilityHandler == nil
                    {
                        // On the first subscription create the SCNReachability handler which is fail-able.
                        // This code needs to run in the Main Thread.
                        if let handler = SCNReachabilityHandler(hostname: strongSelf.m_hostname, notifier: strongSelf.producer, scheduleTool: strongSelf.scheduleTool)
                        {
                            strongSelf.m_reachabilityHandler = handler
                        }
                        else
                        {
                            strongSelf.endEval(App.Error("Cannot start Reachability Service"), vTime: nil)
                        }
                    }
                    else
                    {
                        // On subsequent subscriptions notify initial reachability.
                        evalType.evalQueue!.dispatch(async: { [weak self] in

                            guard let strongSelf = self, let reachabilityHandler = strongSelf.m_reachabilityHandler else { return }

                            if reachabilityHandler.lastReachabilityEvent != .eReachability_Unknown
                            {
                                strongSelf.producer.notify(item: AppEvent(appEventType: .eNetworkReachabilityChange(reachabilityHandler.lastReachabilityEvent)), vTime: vTime)
                            }
                        })
                    }

                case eEvalNotify.eEvalEnd:

                    strongSelf.m_reachabilityHandler = nil

                default:

                    break
            }

            strongSelf.producer.notify(control: control, vTime: vTime)
        }
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// SCNReachabilityHandler: The SCNetworkReachability handler.
///

private final class SCNReachabilityHandler
{
    /// The host name to check reachability to.
    fileprivate var hostname: String

    /// The last reachability event issued by SCNReachability
    fileprivate var lastReachabilityEvent: eDeviceEvent_Reachability = .eReachability_Unknown

    /// The notifier to notify of reachability changes.
    fileprivate var notifier : Rf.ANotifier<AppEvent>

    /// The reachability context.
    private var m_contextPointer : UnsafeMutablePointer<SCNetworkReachabilityContext>


    private let m_scheduleTool : Rf.Tools.Schedule?

    /// Initialise with hostname and notify to notify reachability to.
    /// - Parameter hostname: The reachability host name.
    /// - Parameter notifier: The notifier to notify reachability changes to.
    fileprivate init?(hostname: String, notifier : Rf.ANotifier<AppEvent>, scheduleTool : Rf.Tools.Schedule?)
    {
        self.hostname = hostname
        self.notifier = notifier
        self.m_scheduleTool = scheduleTool
        self.m_contextPointer = UnsafeMutablePointer<SCNetworkReachabilityContext>.allocate(capacity: 1)

        if !registerForReachabilityChanges()
        {
            // Fail init if SCNReachability fails.
            return nil
        }
    }

    /// De-initialise and de-register with SCNetworkReachability.
    deinit
    {
        deregisterForReachabilityChanges()
    }

    /// Register with SCNetworkReachability for reachability changes.
    fileprivate func registerForReachabilityChanges() -> Bool
    {
        guard let reachability = SCNetworkReachabilityCreateWithName(nil, hostname) else { return false }

        let selfPointer = UnsafeMutableRawPointer(Unmanaged.passUnretained(self).toOpaque())
        let context = SCNetworkReachabilityContext(version: 0, info: selfPointer, retain: nil, release: nil, copyDescription: nil)


        let callback : SCNetworkReachabilityCallBack = { (reachability: SCNetworkReachability,
                                                          flags: SCNetworkReachabilityFlags,
                                                          info: UnsafeMutableRawPointer?) in

            let isReachable : Bool = flags.contains(SCNetworkReachabilityFlags.reachable)
            let needsConnection : Bool = flags.contains(SCNetworkReachabilityFlags.connectionRequired)

            let currentReachability : eDeviceEvent_Reachability = (isReachable && !needsConnection) ? .eReachability_IsReachable : .eReachability_NotReachable;

            let handler = Unmanaged<SCNReachabilityHandler>.fromOpaque(info!).takeUnretainedValue()

            if (handler.lastReachabilityEvent != currentReachability)
            {
                handler.lastReachabilityEvent = currentReachability
                handler.notifier.notify(item: AppEvent(appEventType: .eNetworkReachabilityChange(currentReachability)), vTime: handler.m_scheduleTool?.currentVTime)
            }
        }

        m_contextPointer.initialize(to: context)

        SCNetworkReachabilitySetCallback(reachability, callback, m_contextPointer)
        SCNetworkReachabilityScheduleWithRunLoop(reachability, CFRunLoopGetMain(), CFRunLoopMode.commonModes.rawValue)

        return true
    }

    /// De-register with SCNetworkReachability for reachability changes.
    fileprivate func deregisterForReachabilityChanges()
    {
        guard let reachability = SCNetworkReachabilityCreateWithName(nil, hostname) else { return }

        SCNetworkReachabilitySetCallback(reachability, nil, nil)
        m_contextPointer.deinitialize(count: 1)
    }
}
