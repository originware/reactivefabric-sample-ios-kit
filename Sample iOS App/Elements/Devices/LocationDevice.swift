//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 16/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

import ReactiveFabric

@available(iOS 8, *)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// LocationDevice: The Location Device.
///
///    Employs the LocationService to source locations and if it times out, supplies a simulated position.
///

class LocationAppDevice : Rf.Patterns.Source<AppEvent>, IRfScheduling
{
    typealias LocationRequest = LocationService.eRequest
    typealias eEvalNotify = Rf.EvalScope.eNotify
    typealias eFabricNotify = Rf.FabricScope.eNotify

    /// The last discovered position, possibility the simulated position if a real one was not available in time.
    private(set) var currentLocation :  CLLocation? = nil

    /// The location service that supplies locations.
    private let      locationService :  LocationService

    /// The scheduler for timeout determination.
    public var       scheduleTool :     Rf.Tools.Schedule? = nil
    
    /// Initialise with trace ID.
    /// - Parameter traceID : The device trace ID.
    override init(_ traceID : Rf.TraceID)
    {
        self.locationService = LocationService(traceID)

        super.init(traceID)

        self.onControl = { [weak self] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType):

                    guard let scheduleTool = strongSelf.scheduleTool else
                    {
                        assertionFailure("\(String(describing: self)): No valid scheduler")
                        return
                    }

                    scheduleTool.evalQueue = evalType.evalQueue
                    scheduleTool.subscribe()

                    strongSelf.locationService.consumer.notify(item: LocationRequest.eBeginSession({ [weak self] (reply) in

                        guard let strongSelf = self, let evalQueue = strongSelf.evalQueue else { return }

                        evalQueue.dispatch(async: {
                            
                            guard let strongSelf = self else { return }
                            
                            switch reply
                            {
                            case .eDidBeginSession:
                                
                                guard let scheduleTool = strongSelf.scheduleTool else
                                {
                                    assertionFailure("\(strongSelf): No valid scheduler")
                                    return
                                }
                                
                                scheduleTool.schedule(atTime: Date(timeIntervalSinceNow: AppViewController.Constant.LocationSimulationTimeout), action: { (timeoutVTime) in
                                    
                                    guard let strongSelf2 = self else { return }
                                    
                                    if strongSelf2.currentLocation == nil
                                    {
                                        let appEvent = AppEvent(appEventType: eAppEventType.eLocationChange(eDeviceEvent_Location.eLocation_LastKnown(App.Constant.SimulatedPosition)), isSimulatedEvent: true)
                                        
                                        // Notify the current location.
                                        strongSelf2.producer.notify(item: appEvent, vTime: timeoutVTime)
                                        
                                        strongSelf2.currentLocation = App.Constant.SimulatedPosition
                                    }
                                    
                                    // Unsubscribe from any further scheduler events.
                                    strongSelf2.scheduleTool?.unsubscribe(vTime: vTime)
                                })
                                
                                strongSelf.locationService.consumer.notify(item: LocationRequest.eGetLastLocation, vTime: vTime)
                                
                            case .eHaveLocation(let location):
                                
                                let appEvent = AppEvent(appEventType: eAppEventType.eLocationChange(eDeviceEvent_Location.eLocation_LastKnown(location)), isSimulatedEvent: false)
                                
                                // Unsubscribe from any further scheduler events.
                                if let scheduleTool = strongSelf.scheduleTool
                                {
                                    scheduleTool.unsubscribe(vTime: vTime)
                                }
                                
                                strongSelf.producer.notify(item: appEvent, vTime: scheduleTool.currentVTime)
                                
                                strongSelf.currentLocation = location
                                
                            case .eError(let error):
                                
                                strongSelf.endEval(error, vTime: vTime)
                                
                            case .eDidEndSession:
                                
                                break
                            }
                        })
                       
                    }), vTime: vTime)

                case eEvalNotify.eEvalEnd:

                    strongSelf.locationService.consumer.notify(item: LocationRequest.eEndSession, vTime: vTime)

                case eFabricNotify.eInstallTool(.eScheduler(let (_, name, _))):

                    if name == "scheduler", let tool = strongSelf.getScheduleTool(traceID)
                    {
                        strongSelf.scheduleTool = tool
                    }

                default:

                    break
            }

            strongSelf.producer.notify(control: control, vTime: vTime)
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// LocationService: The Location Service, interfaces with the LocationManager.
///

class LocationService: Rf.Patterns.Service<LocationService.eRequest>
{
    public typealias ReplyNotify = (eReply) -> Void

    public enum eRequest
    {
        case eBeginSession(ReplyNotify)
        case eGetLastLocation
        case eEndSession
    }

    public enum eReply
    {
        case eDidBeginSession
        case eHaveLocation(location: CLLocation)
        case eError(App.Error)
        case eDidEndSession
    }

    private var m_replyFunc: ReplyNotify? = nil
    private var m_locationManager : LocationManager!

    fileprivate override init(_ traceID: Rf.TraceID)
    {
        super.init(traceID)

        self.onItem = { [weak self] (item: LocationService.eRequest, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch item
            {
                case .eBeginSession(let replyNotify):

                    strongSelf.m_replyFunc = replyNotify

                    strongSelf.m_locationManager = LocationManager(replyNotify)

                    RfSDK.EvalQueues.UI.dispatch(async: {

                        if !strongSelf.m_locationManager.isLocationServiceEnabled()
                        {
                            strongSelf.m_replyFunc!(.eError(App.Error("Location Service is not available")))
                        }

                    })

                    strongSelf.m_replyFunc!(.eDidBeginSession)

                case .eGetLastLocation:

                    guard let location = strongSelf.m_locationManager.m_lastLocation else { return }

                    strongSelf.m_replyFunc!(.eHaveLocation(location: location))

                case .eEndSession:

                    strongSelf.m_locationManager.stopLocationService()

                    strongSelf.m_replyFunc!(.eDidEndSession)
            }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// LocationManager: The Location Manager which interfaces with the system CLLocationManager.
///

fileprivate class LocationManager : NSObject, CLLocationManagerDelegate
{
    public typealias ReplyFunc = (LocationService.eReply) -> Void

    fileprivate var m_locationManager: CLLocationManager? = nil
    fileprivate let m_useSignificantChanges = CLLocationManager.significantLocationChangeMonitoringAvailable()
    fileprivate var m_lastLocation: CLLocation? = nil

    private let m_replyFunc : ReplyFunc

    fileprivate init(_ replyFunc : @escaping ReplyFunc)
    {
        self.m_replyFunc = replyFunc
    }

    deinit
    {
        stopLocationService()
    }

    fileprivate func isLocationServiceEnabled() -> Bool
    {
        guard CLLocationManager.locationServicesEnabled() else { return false }

        m_locationManager = CLLocationManager()
        m_locationManager!.delegate = self

        return true
    }
    
    /// The location didUpdateLocations delegate handler.
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let currentLocation: CLLocation = locations.last, m_lastLocation != currentLocation else { return }

        m_lastLocation = currentLocation

        // Reply the location back to the client.
        m_replyFunc(.eHaveLocation(location: currentLocation))
    }

    /// The location didFailWithError delegate handler.
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        let nsError = error as NSError

        guard nsError.code != CLError.locationUnknown.rawValue else { return }

        m_replyFunc(.eError(App.Error(nsError: nsError)))
    }

    /// The location didChangeAuthorizationStatus delegate handler.
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus)
    {
        switch status
        {
            case .notDetermined:

                manager.requestAlwaysAuthorization()

            case .restricted, .denied:

                m_replyFunc(.eError(App.Error("Location Services Not Enabled")))

            default:
                
                startLocationService()
        }
    }

    /// Start Location Services.
    private func startLocationService()
    {
        guard let locationManager = m_locationManager else { return }

        if m_useSignificantChanges
        {
            locationManager.distanceFilter = App.Constant.LocationChangeUpdateDistance
            locationManager.desiredAccuracy = kCLLocationAccuracyBest

            locationManager.startMonitoringSignificantLocationChanges()
        }
        else
        {
            locationManager.distanceFilter = App.Constant.LocationChangeUpdateDistance
            locationManager.desiredAccuracy = kCLLocationAccuracyBest

            locationManager.startUpdatingLocation()
        }
    }

    /// Stop Location Services.
    fileprivate func stopLocationService()
    {
        guard let locationManager = m_locationManager else { return }

        if m_useSignificantChanges
        {
            locationManager.stopMonitoringSignificantLocationChanges()
        }
        else
        {
            locationManager.stopUpdatingLocation()
        }
    }
}

