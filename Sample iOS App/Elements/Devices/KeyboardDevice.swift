//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// KeyboardDevice: The Device for Keyboard show/hide events in the form of AppEvent notifications.
///

class KeyboardDevice:  Rf.Patterns.Source<AppEvent>
{
    typealias eEvalNotify = Rf.EvalScope.eNotify

    /// The show keyboard NSNotification observers.
    fileprivate var m_NSNotificationCenterShowObserver: AnyObject? = nil

    /// The hide keyboard NSNotification observers.
    fileprivate var m_NSNotificationCenterHideObserver: AnyObject? = nil

    /// Initialise with trace ID.
    /// - Parameter traceID : The device trace ID.
    override init(_ traceID: Rf.TraceID)
    {
        super.init(traceID)

        self.onControl = { [weak self] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin:

                    strongSelf.registerForKeyboardNSNotifications()

                case eEvalNotify.eEvalEnd:

                    strongSelf.deregisterForKeyboardNSNotifications()

                default:

                    break
            }

            strongSelf.producer.notify(control: control, vTime: vTime)
        }
    }

    /// Register for NSNotification keyboard events.
    fileprivate func registerForKeyboardNSNotifications()
    {
        m_NSNotificationCenterShowObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UIKeyboardDidShowNotification"), object: nil, queue: nil, using: { [weak self] (notification : Notification!) in

            guard let strongSource = self else { return }

            RfSDK.EvalQueues.UI.dispatch(async: {
                strongSource.producer.notify(item: AppEvent(appEventType: .eKeyboardChange(.eKeyboard_didShow)), vTime: nil)
            })
        })

        m_NSNotificationCenterHideObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "UIKeyboardDidHideNotification"), object: nil, queue: nil, using: { [weak self] (notification : Notification!) in

            guard let strongSource = self else { return }

            RfSDK.EvalQueues.UI.dispatch(async: {
                strongSource.producer.notify(item: AppEvent(appEventType: .eKeyboardChange(.eKeyboard_didHide)), vTime: nil)
            })
        })
    }

    /// De-register for NSNotification keyboard events.
    fileprivate func deregisterForKeyboardNSNotifications()
    {
        if m_NSNotificationCenterShowObserver != nil
        {
            NotificationCenter.default.removeObserver(m_NSNotificationCenterShowObserver!)
        }

        if m_NSNotificationCenterHideObserver != nil
        {
            NotificationCenter.default.removeObserver(m_NSNotificationCenterHideObserver!)
        }
    }
}
