//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 29/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIKeywordMatcherSource: Match given text for POI keyword matches and sub-string matches. Notify matches.
///

class POIKeywordMatcher : Rf.Patterns.Operator<AppEvent, AppEvent>, IRfScheduling
{
    fileprivate let m_minSubstringLength: Int
    fileprivate var m_POIKeywordMatcherByPOIKeyword = [String : SubstringMatcher]()

    /// The scheduler for timeout determination.
    public var      scheduleTool :        Rf.Tools.Schedule? = nil

    /// The monitor of the device notifications.
    private let     m_monitorNotifier:    Rf.ANotifier<eAppOpEventType>

    init(_ traceID: Rf.TraceID, minSubstringLength : Int, monitor: Rf.ANotifier<eAppOpEventType>)
    {
        self.m_minSubstringLength = minSubstringLength
        self.m_monitorNotifier = monitor

        super.init(traceID)

        // Load keywords to be matched.
        for poiKeyword in App.Settings.allPOIkeywords
        {
            m_POIKeywordMatcherByPOIKeyword[poiKeyword] = SubstringMatcher(string: poiKeyword, minSubstringLength : minSubstringLength)
        }

        self.onItem = { [weak self] (item: AppEvent, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }
            guard case .eUITextFieldChange = item.appEventType else { return }

            let textFieldChange = item.appEventType.textFieldChange
            let attributedText = textFieldChange.attributedText

            if attributedText.string.count > 2, let matches = strongSelf.matchPOIKeywords(attributedText.string)
            {
                // Commence keyword matching.
                strongSelf.producer.notify(item: AppEvent(appEventType: .ePOIKeywordMatchSet(matches)), vTime: strongSelf.scheduleTool?.currentVTime)
            }

            // emit monitor event.
            strongSelf.m_monitorNotifier.notify(item: .eFromKeyboard_To_POIKeywordMatcherPath, vTime: vTime)
         }
    }

    /// Match the given keywords
    /// - Parameter stringToMatch: The text to match for POI keywords/
    func matchPOIKeywords(_ stringToMatch: String) -> POIKeywordEvent_Match?
    {
        let wordList = stringToMatch.split(separator: " ").map{ String($0).lowercased() }
        let matches = POIKeywordEvent_Match()

        for keyWord in wordList
        {
            let substringMatcher : SubstringMatcher = SubstringMatcher(string: keyWord, minSubstringLength : m_minSubstringLength)
            let stringToMatchLength = keyWord.distance(from: keyWord.startIndex, to: keyWord.endIndex)
            let matchThresholdLength = stringToMatchLength <= 2 ? 2 : stringToMatchLength - 1

            for matcher in m_POIKeywordMatcherByPOIKeyword.values
            {
                let poiKeywordMatches = matcher.getCommonSubstrings(substringMatcher)

                if (poiKeywordMatches.totalMatchLength >= matchThresholdLength) && (poiKeywordMatches.maxMatchLength >= 3)
                {
                    matches.addMatchesForPOIKeyword(poiKeywordMatches)
                }
            }
        }

        return matches.count != 0 ? matches : nil
    }
}
