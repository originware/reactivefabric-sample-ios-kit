//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 5/07/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import CoreLocation

import ReactiveFabric

extension Rf.Operators
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// POIRequestor: Receives POI keywords requests and emits
    ///

    static func poiRequestor(devices: App.Devices,
                             viewModels: App.ViewModelCollectors,
                             monitorNotifier: Rf.ANotifier<eAppOpEventType>,
                             timeout: Rf.VTime) -> Rf.AOperator<AppEvent, POIPortal.eRequest>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify
        typealias eFabricNotify = Rf.FabricScope.eNotify

        let traceID                                                        = Rf.TraceID("/appLogic/POIRequestor")
        let requestorOperator : Rf.AOperator<AppEvent, POIPortal.eRequest> = RfSDK.factory.Operator(traceID)

        var haveLocation = false
        var haveReachability = false
        var haveGivenTimeoutConfirmation = false
        var currentLocation: CLLocation? = nil
        var scheduleTool: Rf.Tools.Schedule? = nil

        requestorOperator.consumer.onItem = { [weak requestorOperator] (appEvent: AppEvent, vTime: Rf.VTime?) in

            guard let strongOperator = requestorOperator else { return }

            func emitPOIRequest()
            {
                guard let strongOperator2 = requestorOperator else { return }

                // Location and Network connectivity are available, so emit a poi request.
                let poikeywordCollectionMatches = appEvent.appEventType.poikeywordCollectionMatches
                let poiLocateRequest = MultiPOILocateEvent_Request(poiKeywords: Set<String>(poikeywordCollectionMatches.poiKeywords), location : currentLocation!)

                haveGivenTimeoutConfirmation = false

                strongOperator2.producer.notify(item: POIPortal.eRequest.ePOIPortalRequest(poiLocateRequest), vTime: vTime)

                // emit monitor event.
                monitorNotifier.notify(item: eAppOpEventType.eFromPOIRequestor_To_POIPortal, vTime: nil)
            }

            func emitLocationOrReachabilityTimeout(vTime: Rf.VTime?)
            {
                guard !haveGivenTimeoutConfirmation, let confirmMessage : String = {

                    haveGivenTimeoutConfirmation = true

                    switch (haveLocation, haveReachability)
                    {
                        case (false, false):        return "Network and Location not available"
                        case (false, true):         return "Location not available"
                        case (true, false):         return "Network is not reachable"
                        case (true, true):
                            
                            // Late reply from location or reachability.
                            emitPOIRequest()
                            return nil
                    }
                }() else
                {
                    return
                }

                viewModels.consumer.notify(item: AppEvent(appEventType: .ePresentTimeoutConfirmation(confirmMessage)), vTime: vTime)
            }

            // If we have a location fix and network link.
            if haveLocation && haveReachability
            {
                // Cancel the timeout timer.
                scheduleTool?.unsubscribe(vTime: vTime)
                scheduleTool = nil

                // Location and Network connectivity are available, so emit a poi request.
                emitPOIRequest()
            }
            else if !haveReachability
            {
                viewModels.confirm.consumer.notify(item: AppEvent(appEventType: .ePresentTimeoutConfirmation("Please connect this device to the net.")), vTime: vTime)
            }
            else
            {
                // Either the location or network is not currently available.
                // Wait for timeout secs to allow location and reachability to be made available.

                scheduleTool?.schedule(atTime: Date(timeIntervalSinceNow: timeout), action: emitLocationOrReachabilityTimeout)
            }
        }

        requestorOperator.onControl = { [weak requestorOperator] (control, vTime) in

            guard let strongOperator = requestorOperator else { return }

            switch control
            {
                case eFabricNotify.eInstallTool(.eScheduler(let (_, name, _))):

                    if name == "scheduler", let tool = strongOperator.getScheduleTool(traceID.appending("scheduleTool"))
                    {
                        scheduleTool = tool
                    }

                    strongOperator.producer.notify(control: control, vTime: vTime)

                case eEvalNotify.eEvalBegin(let evalType):

                    // Subscribe for location updates.
                    // Note: this is performed in the subscription thread as Core location wants to
                    //   attach to the RunLoop of the Main UI Thread.

                    devices.observe(devices.traceID.appending("observeLocationAndReachability"), item: { (item: AppEvent, vTime: Rf.VTime?) in

                        switch item.appEventType
                        {
                            case .eLocationChange(let location):

                                switch location
                                {
                                    case .eLocation_LastKnown(let lastLocation):

                                        currentLocation = lastLocation
                                        haveLocation = true

                                        // emit to view model
                                        viewModels.consumer.notify(item: item, vTime: vTime)

                                        // emit monitor event.
                                        monitorNotifier.notify(item: eAppOpEventType.eFromLocation_To_POIRequestor, vTime: nil)

                                    default:

                                        haveLocation = false
                                }

                            case .eNetworkReachabilityChange(let reachability):

                                switch reachability
                                {
                                    case .eReachability_IsReachable:

                                        haveReachability = true

                                        // emit to view model
                                        viewModels.consumer.notify(item: item, vTime: vTime)

                                        // emit monitor event.
                                        monitorNotifier.notify(item: eAppOpEventType.eFromReachability_To_POIRequestor, vTime: nil)

                                    case .eReachability_NotReachable:

                                        haveReachability = false

                                        // emit to view model
                                        viewModels.consumer.notify(item: item, vTime: vTime)
                                        
                                        // emit monitor event.
                                        monitorNotifier.notify(item: eAppOpEventType.eFromPOIRequestor_To_POIPortal, vTime: nil)

                                    default:

                                        break
                                }

                            default:

                                break
                        }
                    })

                    strongOperator.producer.notify(control: control, vTime: vTime)
                    
                    strongOperator.producer.notify(item: POIPortal.eRequest.eBeginScope({ (reply, vTime) in

                        switch reply
                        {
                            case .eDidBeginScope:

                                break

                            case .eRequestSuccess(let appEvent):

                                // The request was success full, so update the map, matched keywords and log the event.
                                
                                // Emit the POI location result to the map.
                                viewModels.map.consumer.notify(item: appEvent, vTime: nil)

                                // Log the result.
                                viewModels.logTextView.consumer.notify(item: appEvent, vTime: vTime)

                                // Display the POI keyword result.
                                viewModels.matchedKeywordsUICollection.consumer.notify(item: appEvent, vTime: nil)

                            case .eRequestFailure(let appEvent):

                                // The POI Service replied, but failed to get an adequate response, log it.
                                viewModels.logTextView.consumer.notify(item: appEvent, vTime: vTime)

                            case .eError(let error):

                                // The POI Service experienced a System error, log the event.
                                let appEvent = AppEvent(appEventType: eAppEventType.eSystemFailure(error))
                                
                                viewModels.confirm.consumer.notify(item: AppEvent(appEventType: .ePresentTimeoutConfirmation("Google Places portal error:\n\(error.description)")), vTime: vTime)

                                viewModels.logTextView.consumer.notify(item: appEvent, vTime: vTime)

                            case .eDidEndScope:

                                break
                        }

                    }), vTime: nil)

                    scheduleTool?.evalQueue = evalType.evalQueue
                    scheduleTool?.subscribe()

                case eEvalNotify.eEvalEnd:

                    // Destroy all resources and services being used.
                    scheduleTool?.unsubscribe(vTime: vTime)
                    strongOperator.producer.notify(control: control, vTime: vTime)

                default:

                    strongOperator.producer.notify(control: control, vTime: vTime)
            }
        }

        return requestorOperator
    }
}

extension Rf.AElement where OutItem == AppEvent
{
    func poiRequestor(devices: App.Devices,
                        viewModels: App.ViewModelCollectors,
                        monitorNotifier : Rf.ANotifier<eAppOpEventType>,
                        timeout: Rf.VTime) -> Rf.AOperator<AppEvent, POIPortal.eRequest>
    {
        let requestorOperator: Rf.AOperator<AppEvent, POIPortal.eRequest> = Rf.Operators.poiRequestor(devices: devices, viewModels: viewModels, monitorNotifier: monitorNotifier, timeout: timeout)

        compose(withConsumer: requestorOperator)

        return requestorOperator
    }
}
