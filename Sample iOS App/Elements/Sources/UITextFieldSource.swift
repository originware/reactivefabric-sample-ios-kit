//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 25/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// UITextFieldSource: The Source for UITextField events in the form of AppEvent notifications.
///

class UITextFieldAppSource: Rf.Patterns.Source<AppEvent>, IRfScheduling
{
    typealias eEvalNotify = Rf.EvalScope.eNotify
    
    /// The UITextField being observed for NSNotifications
    let textField: UITextField

    /// The scheduling tool.
    public var scheduleTool : Rf.Tools.Schedule? = nil
    
    /// The NSNotifications observer.
    fileprivate var m_NSNotificationCenterObserver : AnyObject? = nil

    /// The OS notification center.
    fileprivate var notificationCenter : NotificationCenter { return NotificationCenter.default }
    
    /// Initialise with UITextField.
    /// - Parameter traceID : The device trace ID.
    /// - Parameter textField: The UITextField to observe.
    init(_ traceID: Rf.TraceID, textField : UITextField)
    {
        self.textField = textField

        super.init(traceID)

        self.consumer.onControl = { [weak self] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin:

                    strongSelf.registerForNSNotifications()

                case eEvalNotify.eEvalEnd:

                    strongSelf.deregisterForNSNotifications()

                default:

                    break
            }

            strongSelf.producer.notify(control: control, vTime: vTime)
        }
    }

    /// Clear the UITextField
    public func clear()
    {
        textField.text = ""
    }

    /// Register for NSNotifications.
    /// - Parameter notifier: The notifier to notify UITextField changes to.
    fileprivate func registerForNSNotifications()
    {
        let textField = self.textField

        m_NSNotificationCenterObserver = notificationCenter.addObserver(forName: UITextField.textDidChangeNotification, object: textField, queue: nil, using: { [weak self] _ in

            guard let strongSelf = self else { return }

            let item = AppEvent(appEventType: eAppEventType.eUITextFieldChange(eDeviceEvent_TextField.eText_Change(textField, textField.attributedText!)))
            let vTime : Rf.VTime? = strongSelf.scheduleTool?.currentVTime

            strongSelf.evalQueue?.dispatch(async: {

                strongSelf.producer.notify(item: item, vTime: vTime)
            })
        })
    }

    // De-register for NSNotifications.
    fileprivate func deregisterForNSNotifications()
    {
        guard m_NSNotificationCenterObserver != nil else { return }

        notificationCenter.removeObserver(m_NSNotificationCenterObserver!)
        m_NSNotificationCenterObserver = nil
    }
}
