//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// UITouchAdapter: Adapter from UITouch Control to app events. Handles touch registrations and deregistrations.
///

class UITouchAdapter: Rf.Patterns.Source<AppEvent>, IRfScheduling
{
    typealias eEvalNotify = Rf.EvalScope.eNotify

    /// The scheduling tool.
    public var scheduleTool : Rf.Tools.Schedule? = nil

    /// Map of UIControl Target to UIControl.
    private var m_touchTargetByUIControl = [UIControl : eDeviceEvent_Touch]()

    /// Initialise with UITextField.
    /// - Parameter traceID : The device trace ID.
    override init(_ traceID: Rf.TraceID)
    {
        super.init(traceID)

        self.onControl = { [weak self] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            switch control
            {
                case eEvalNotify.eEvalEnd:          strongSelf.deregisterAll()
                default:                            break
            }

            strongSelf.producer.notify(control: control, vTime: vTime)
        }
    }

    /// Register UIControl for touches to target.
    /// - Parameter control: The UIControl to register for touches.
    /// - Parameter touchTarget: The target of the touch.
    func registerForUITouch(control : UIControl, touchTarget: eDeviceEvent_Touch)
    {
        m_touchTargetByUIControl[control] = touchTarget

        control.addTarget(self, action:#selector(onUIControlEvent(_:event:)), for:UIControl.Event.allTouchEvents)
    }

    /// Deregister UIControl for touches to target.
    /// - Parameter control: The UIControl to de-register touches.
    func deregisterForUITouch(_ control : UIControl)
    {
        m_touchTargetByUIControl.removeValue(forKey: control)

        control.removeTarget(self, action:#selector(onUIControlEvent(_:event:)), for: UIControl.Event.allTouchEvents)
    }

    /// Deregister all registered touches.
    func deregisterAll()
    {
        // Make a copy of the keys as the deregister will modify the underlying m_touchTargetByUIControl
        let controls = Array<UIControl>(m_touchTargetByUIControl.keys)

        for control in controls
        {
            deregisterForUITouch(control)
        }
    }

    /// The Touch event handler, that emits producer touch events.
    @objc func onUIControlEvent(_ sender: UIControl, event : UIEvent)
    {
        guard let touches = event.allTouches else { return }

        for touch : UITouch in touches where touch.phase == UITouch.Phase.ended
        {
            let touchTarget = m_touchTargetByUIControl[sender]

            let appEvent = AppEvent(appEventType: .eUIControlTouch((sender, touchTarget!)))

            producer.notify(item: appEvent, vTime: scheduleTool?.currentVTime)
        }
    }
}

