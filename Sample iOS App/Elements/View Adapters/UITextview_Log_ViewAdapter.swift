//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// LogViewAdapter: An adapter to present textual logs from AppEvents.
///

class LogViewAdapter: Rf.Patterns.Collector<AppEvent>
{
    /// The UITextView to present to.
    fileprivate let m_textView: UITextView

    /// The text formatter.
    fileprivate var m_logFormatter: AppStyle_LogFormatter

    /// The monitor of the device notifications.
    private let     m_monitorNotifier: Rf.ANotifier<eAppOpEventType>
    
    /// Initialise with trace ID, target UITextView and text formatter
    /// - Parameter traceID : The device trace ID.
    /// - Parameter textView : The UI text view to present text.
    /// - Parameter logFormatter : The formatter that formats the text.
    init(_ traceID: Rf.TraceID, textView : UITextView, logFormatter : AppStyle_LogFormatter, monitor: Rf.ANotifier<eAppOpEventType>)
    {
        self.m_textView = textView
        self.m_logFormatter = logFormatter
        self.m_monitorNotifier = monitor

        super.init(traceID)

        self.onItem = { [weak self] (item: AppEvent, vTime: Rf.VTime?) in

            RfSDK.EvalQueues.UI.dispatch(async: { [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.onNotifyInMainThread(item: item, vTime: vTime)
            })
        }
    }
    
    private func onNotifyInMainThread(item: AppEvent, vTime: Rf.VTime?)
    {
        /// Set the log text in the UITextView associated with this Adapter.
        /// - Parameter attributedText: The log text.
        func setText(_ attributedText : NSAttributedString)
        {
            m_textView.attributedText = attributedText
            m_textView.scrollRangeToVisible(NSMakeRange(attributedText.length, 0))
        }
        
        switch item.appEventType
        {
        case .eLocationChange(let locationChange):
            let simulatedNote = item.isSimulatedEvent ? " [Simulated]" : ""
            
            setText(m_logFormatter.formatLogLineEntry(item, locationChange.description + simulatedNote, AppStyle.color(.eLogEventText)))
            
        case .eNetworkReachabilityChange(let reachabilityChange):
            setText(m_logFormatter.formatLogLineEntry(item, reachabilityChange.description, AppStyle.color(.eLogEventText)))
            
        case .ePOILocateResult(let poilocateResult):
            m_monitorNotifier.notify(item: eAppOpEventType.eFromPOILocator_To_LogAdaptor, vTime: nil)
            
            setText(m_logFormatter.formatPOILocateResultForLog(item, poilocateResult : poilocateResult))
            
        case .eOrientation:
            setText(m_logFormatter.formatLogLineEntry(item, item.description, AppStyle.color(.eLogEventText)))
            
        case .eUITextFieldChange(let uiTextField):
            setText(m_logFormatter.formatLogLineEntry(item, "Search Input Text: \(uiTextField.attributedText.string)", AppStyle.color(.eLogEventText)))
            
        case .ePOIKeywordMatchSet:
            m_monitorNotifier.notify(item: eAppOpEventType.eFromPOIKeywordMatcher_To_LogAdapter, vTime: nil)
            
            setText(m_logFormatter.formatPOIKeywordMatchLogText(item))
            
        case .eUIControlTouch(let (_, touchTarget)):
            
            setText(m_logFormatter.formatLogLineEntry(item, "Tap: \(touchTarget.rawValue)", AppStyle.color(.eLogEventText)))
            
            if touchTarget == .eTouch_clearLogButton
            {
                setText(NSAttributedString(string: ""))
                m_logFormatter.clearLog()
            }
            
        case .eSystemFailure:
            
            setText(m_logFormatter.formatPOIKeywordMatchLogText(item))
            
        default:
            fatalError("Unexpected AppEvent: \(item.description)")
        }
    }
}
