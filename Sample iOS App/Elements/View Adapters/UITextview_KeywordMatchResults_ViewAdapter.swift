//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 7/07/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric


struct Constant
{
    static let POITypePlaceHolderColumns = 3
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// KeywordResultsViewAdapter: An adapter to present POI keyword matches.
///

class KeywordResultsViewAdapter : Rf.Patterns.Collector<AppEvent>
{
    /// The UITextView to present to.
    fileprivate let m_textView: UITextView

    /// The monitor of the device notifications.
    private let     m_monitorNotifier: Rf.ANotifier<eAppOpEventType>
    
    /// Initialise with trace ID, target UITextView and text formatter
    /// - Parameter traceID : The device trace ID.
    init(_ traceID : Rf.TraceID, textView: UITextView, logFormatter: AppStyle_LogFormatter, monitor: Rf.ANotifier<eAppOpEventType>)
    {
        self.m_textView = textView
        self.m_monitorNotifier = monitor

        // Element runs in the UIThread.
        super.init(traceID)

        self.onItem = { [weak self] (item: AppEvent, vTime: Rf.VTime?) in

            RfSDK.EvalQueues.UI.dispatch(async: { [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.onNotifyInMainThread(item: item, vTime: vTime)
            })
        }
    }

    public func reset()
    {
        m_textView.attributedText = AppStyle.getFormattedPOIKeywordsPlaceHolder(Constant.POITypePlaceHolderColumns)
    }

    private func onNotifyInMainThread(item: AppEvent, vTime: Rf.VTime?)
    {
        m_monitorNotifier.notify(item: .eFromPOIKeywordMatcher_To_POIKeywordsResults, vTime: nil)

        switch item.appEventType
        {
            case .ePOIKeywordMatchSet:

                // Process Keyword Match AppEvent notifications.
                m_textView.attributedText = formatPOIKeywordMatchResultsText(item)

            default:

                fatalError("Unexpected AppEvent: \(item.description)")
        }
    }

    /// Format a collection of keyword matches from an AppEvent.
    /// - Parameter appEvent: The appevent with the Keyword Match.
    /// - Returns: The formatted keywords as an NSAttributedString.
    fileprivate func formatPOIKeywordMatchResultsText(_ appEvent : AppEvent) -> NSAttributedString
    {
        func formatLogText(_ string : String) -> NSMutableAttributedString
        {
            let font = AppStyle.font(.ePOIResultsText)
            let textColor = AppStyle.color(.ePOISearchResultText)
            let paragraphStyle = NSMutableParagraphStyle()
            
            paragraphStyle.lineHeightMultiple = 1
            paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
            paragraphStyle.alignment = NSTextAlignment.center
            
            return NSMutableAttributedString(string: string + "\n", attributes: [
                        NSAttributedString.Key.font : font,
                        NSAttributedString.Key.foregroundColor : textColor,
                        NSAttributedString.Key.paragraphStyle : paragraphStyle])
        }
        
        let poikeywordCollectionMatches = appEvent.appEventType.poikeywordCollectionMatches
        
        if poikeywordCollectionMatches.count == 0
        {
            return formatLogText("")
        }
        
        let matchText : NSMutableAttributedString = formatLogText("")
        
        let highlightColor : UIColor = AppStyle.color(.ePOIHighlightBackground)
        let matchedTextColor : UIColor = AppStyle.color(.ePOIText)
        
        for (poiKeyword, matches) in poikeywordCollectionMatches.poiKeywordMatchesByPOIKeyword
        {
            let text = formatLogText(poiKeyword)
            
            for range in matches.keywordMatchRanges
            {
                let nsRange : NSRange = poiKeyword.rangeToNSRange(range)

                text.addAttribute(NSAttributedString.Key.backgroundColor, value:highlightColor, range:nsRange)
                text.addAttribute(NSAttributedString.Key.foregroundColor, value:matchedTextColor, range:nsRange)
            }

            matchText.append(text)
        }
        
        return matchText
    }
}
