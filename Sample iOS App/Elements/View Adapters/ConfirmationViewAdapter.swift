//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 16/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ConfirmationViewAdapter: An adapter to present confirmations triggered from AppEvent notifications.
///

class ConfirmationViewAdapter : Rf.Patterns.Collector<AppEvent>
{
    // Enabler.
    open var enabled = true

    /// The current alerts begin presented.
    fileprivate lazy var m_pendingAlertQueue = [String]()

    private weak var m_viewController: AppViewController?
    
    /// Initialise with trace ID.
    /// - Parameter traceID : The device trace ID.
    init(_ traceID: Rf.TraceID, viewController: AppViewController)
    {
        self.m_viewController = viewController
        super.init(traceID)

        self.onItem = { [weak self] (item: AppEvent, vTime: Rf.VTime?) in

            RfSDK.EvalQueues.UI.dispatch(async: { [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.onNotifyInMainThread(item: item, vTime: vTime)
            })
        }
    }

    private func onNotifyInMainThread(item: AppEvent, vTime: Rf.VTime?)
    {
        if !enabled { return }

        let message = item.appEventType.confirmation

        // If the alert has not already been presented, then either queue or present.
        if !m_pendingAlertQueue.contains(message)
        {
            m_pendingAlertQueue.append(message)

            if m_pendingAlertQueue.count == 1
            {
                RfSDK.EvalQueues.UI.dispatch(async: {
                    self.presentConfirmation(message)
                })
            }
        }
    }

    /// Present Confirmation dialog.
    fileprivate func presentConfirmation(_ message : String)
    {
        if !enabled { return }

        // Get present the confirmation on the top most view controller.
        if let viewController = m_viewController
        {
            let alert = UIAlertController(title: "Alert", message: message, preferredStyle: UIAlertController.Style.alert)

            alert.modalPresentationStyle = UIModalPresentationStyle.popover
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { [unowned self] action in

                // Dismiss the presented confirmation.
                alert.dismiss(animated: false, completion: nil)

                // Remove the last alert request.
                self.m_pendingAlertQueue.remove(at: 0)

                // If there are more messages queued, present the next.
                if let nextMessage = self.m_pendingAlertQueue.first
                {
                    self.presentConfirmation(nextMessage)
                }
            }))

            if let popoverController = alert.popoverPresentationController
            {
                popoverController.sourceView = viewController.view
                popoverController.sourceRect = viewController.view.bounds
                popoverController.canOverlapSourceViewRect = true
            }

            // Present the confirmation.
            viewController.present(alert, animated: false, completion: nil)
        }
    }
}
