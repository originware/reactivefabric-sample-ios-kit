//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 17/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// ViewAdapter_Label: An adapter to present labels triggered from AppEvent notifications.
///

class LabelViewAdapter : Rf.Patterns.Collector<AppEvent>
{
    /// The label to present to.
    fileprivate let m_label: UILabel
    
    /// Initialise with trace ID.
    /// - Parameter traceID : The device trace ID.
    public init(_ traceID : Rf.TraceID, label: UILabel)
    {
        self.m_label = label

        super.init(traceID)

        self.onItem = { [weak self] (item: AppEvent, vTime: Rf.VTime?) in
            
            RfSDK.EvalQueues.UI.dispatch(async: { [weak self] in

                guard let strongSelf = self else { return }

                strongSelf.onNotifyInMainThread(item: item, vTime: vTime)
            })
        }
    }
    
    private func onNotifyInMainThread(item: AppEvent, vTime: Rf.VTime?)
    {
        func updateLabelTitle(_ text: NSAttributedString)
        {
            guard text != m_label.attributedText else { return }

            m_label.attributedText = text
        }

        switch item.appEventType
        {
            case .eLocationChange(let locationChange):
                updateLabelTitle(AppStyle.formatUILabel(forLocationChange: locationChange, isSimulated: item.isSimulatedEvent))

            case .eNetworkReachabilityChange(let reachability):
                updateLabelTitle(AppStyle.formatUILabel(forReachabilityChange: reachability))

            default:

                fatalError("Unexpected AppEvent: \(item.description)")
        }
    }
}
