//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 18/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

import ReactiveFabric


@available(iOS 7, *)

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIEntryState: The state (index, color and text) of a POI entry in the UICollectionView.
///

private class POIEntryState
{
    let poiKeyword : String
    var hasChanged : Bool
    var index : Int
    var error: NSError? = nil

    fileprivate var m_resultCount: Int

    var enabled : Bool          { return (m_resultCount > 0) && (error == nil) }
    var resultCount: Int        { return m_resultCount }

    init(poiKeyword : String, index : Int)
    {
        self.poiKeyword = poiKeyword
        self.index = index
        self.hasChanged = true
        self.m_resultCount = 0
    }

    func updateState(_ newError: NSError?, newResultCount : Int)
    {
        if (m_resultCount != newResultCount) || (error != newError)
        {
            m_resultCount = newResultCount
            error = newError
            hasChanged = true
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POIEntryStyle: The styler for a POI entry in the UICollectionView.
///

private class POIEntryStyle
{
    class func styleButton(_ button : inout UIButton, buttonState : POIEntryState)
    {
        func getButtonText(_ buttonState: POIEntryState, normalisedText : String) -> String
        {
            switch (buttonState.error, buttonState.enabled)
            {
                case (nil, true), (nil, false):
                    return "\(normalisedText) (\(buttonState.resultCount))"

                default:
                    return "\(normalisedText) (no reply)"
            }
        }
        
        func getButtonColor(_ buttonState: POIEntryState) -> UIColor
        {
            switch (buttonState.error, buttonState.enabled)
            {
            case (nil, true):
                return AppStyle.color(.eEnabled)
                
            case (nil, false):
                return AppStyle.color(.eDisabled)
                
            default:
                return AppStyle.color(.eError)
            }
        }

        func formatButtonText(_ keyword : String, buttonColor : UIColor, buttonState: POIEntryState) -> NSAttributedString
        {
            let normalisedText = keyword.replacingOccurrences(of: "_", with: " ")
            let textWithCount = getButtonText(buttonState, normalisedText: normalisedText)

            return NSAttributedString(string: textWithCount, attributes: [NSAttributedString.Key.foregroundColor : buttonColor, NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 14)!])
        }

        let buttonColor = getButtonColor(buttonState)

        button.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
        button.titleLabel?.textAlignment = NSTextAlignment.center
        button.tag = buttonState.index
        button.layer.cornerRadius = 10.0

        button.setAttributedTitle(formatButtonText(buttonState.poiKeyword, buttonColor: buttonColor, buttonState: buttonState), for:UIControl.State())
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// POICollectionController: The data source and delegate for the UICollectionView presenting POI locate statuses.
///

private class POICollectionController: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
{
    fileprivate var m_collectionView: UICollectionView
    fileprivate var m_poiButtonStateByPOIKeyword = [String : POIEntryState]()

    init(collectionView : UICollectionView)
    {
        self.m_collectionView = collectionView

        super.init()

        self.m_collectionView.delegate = self
        self.m_collectionView.dataSource = self
    }

    deinit
    {
        self.m_collectionView.delegate = nil
        self.m_collectionView.dataSource = nil
    }

    func clear()
    {
        m_poiButtonStateByPOIKeyword.removeAll()
        reload()
    }

    func reload()
    {
        self.m_collectionView.reloadData()
        self.m_collectionView.performBatchUpdates({

              var indexPaths = [IndexPath]()

              for buttonInfo in self.m_poiButtonStateByPOIKeyword.values
              {
                  if buttonInfo.hasChanged
                  {
                      let newIndexPath = IndexPath(index : buttonInfo.index)

                      indexPaths.append(newIndexPath)

                      buttonInfo.hasChanged = false
                  }
              }

          }, completion: { (finished : Bool) in

            self.m_collectionView.reloadData()
        })
    }

    @objc func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return 1
    }

    @objc func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return m_poiButtonStateByPOIKeyword.count
    }

    @objc func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let index = (indexPath as NSIndexPath).row
        let buttonState = getPOIButtonStateForIndex(index)

        assert(buttonState != nil, "Expected a valid POIButtonInfo for the index:\(index)")

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "POITypeCell", for : indexPath)

        for view in cell.contentView.subviews
        {
            if var button = view as? UIButton
            {
                POIEntryStyle.styleButton(&button, buttonState : buttonState!)
                break
            }
        }

        return cell
    }
    
    @objc func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView
    {
        if kind == UICollectionView.elementKindSectionHeader
        {
            let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header", for : indexPath)
            let borderColor : UIColor = AppStyle.color(.eBorderColor)
            
            sectionHeader.layer.cornerRadius = 10.0
            sectionHeader.layer.borderWidth = 0.5
            sectionHeader.layer.borderColor = borderColor.cgColor
            
            return sectionHeader
        }
    

        fatalError("Cannot get UICollectionReusableView(located POI header)")
    }

    func getPOIButtonStateForIndex(_ index : Int) -> POIEntryState?
    {
        for buttonInfo in m_poiButtonStateByPOIKeyword.values
        {
            if buttonInfo.index == index
            {
                return buttonInfo
            }
        }

        return nil
    }

    func itemsHaveChanged() -> Bool
    {
        for buttonInfo in m_poiButtonStateByPOIKeyword.values
        {
            if buttonInfo.hasChanged
            {
                return true
            }
        }

        return false
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// UICollectionViewAdapter: An adapter to present POI lookup results in a collection view.
///

class UICollectionViewAdapter : Rf.Patterns.Collector<AppEvent>
{
    /// The UICollectionView to present to.
    fileprivate let m_collectionController : POICollectionController

    /// The current index to the UICollectionView.
    fileprivate var m_currentIndex : Int = 0
    
    /// Initialise with the name tag and the target UI Collection.
    /// - Parameter traceID : The device trace ID.
    init(_ traceID : Rf.TraceID, collectionView : UICollectionView)
    {
        m_collectionController = POICollectionController(collectionView : collectionView)

        super.init(traceID)

        self.onItem = { [weak self] (item: AppEvent, vTime: Rf.VTime?) in

                RfSDK.EvalQueues.UI.dispatch(async: { [weak self] in

                    guard let strongSelf = self else { return }

                    strongSelf.onNotifyInMainThread(item: item, vTime: vTime)
                })
        }
    }

    /// Clear all enteries in the UICollectionView.
    public func clearPOIMatches()
    {
        m_collectionController.clear()
        m_currentIndex = 0
    }
    
    private func onNotifyInMainThread(item: AppEvent, vTime: Rf.VTime?)
    {
        switch item.appEventType
        {
            case .ePOIKeywordMatchSet(let poiKeywordMatches):

                for (poiKeyword, _) in poiKeywordMatches.poiKeywordMatchesByPOIKeyword
                {
                    // Create the button state and store.

                    if m_collectionController.m_poiButtonStateByPOIKeyword[poiKeyword] == nil
                    {
                        let poiButtonState = POIEntryState(poiKeyword: poiKeyword, index: m_currentIndex)

                        m_currentIndex += 1
                        m_collectionController.m_poiButtonStateByPOIKeyword[poiKeyword] = poiButtonState
                    }
                }

                if m_collectionController.itemsHaveChanged()
                {
                    m_collectionController.reload()
                }

            case .ePOILocateResult(let poiLocateResult):

                for poiKeyword in poiLocateResult.poiKeywords
                {
                    // Update the button state.

                    // Assume that the buttons were created before hand.
                    let poiButtonState = m_collectionController.m_poiButtonStateByPOIKeyword[poiKeyword]

                    poiButtonState?.updateState(poiLocateResult.error, newResultCount: poiLocateResult.countForPOIType(poiKeyword))
                }

                if m_collectionController.itemsHaveChanged()
                {
                    m_collectionController.reload()
                }

            case .eUIControlTouch(let (_, touchTarget)):

                if touchTarget == .eTouch_clearPOIKeywordMatchesButton
                {
                    clearPOIMatches()
                }

            default:

                fatalError("Unexpected AppEvent: \(item.description)")
        }
    }
}
