//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 20/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit

class TappableUIScrollView: UIScrollView, UIScrollViewDelegate
{
    struct Constant
    {
        static let ZoomInScale: CGFloat  = 0.95
        static let ZoomOutScale: CGFloat = 2.0
    }

    var containerView : UIView? = nil

    required init?(coder aDecoder : NSCoder)
    {
        super.init(coder: aDecoder)

        self.delegate = self

        let tapGesture = UITapGestureRecognizer(target: self, action:#selector(onDoubleTap(_:)))

        tapGesture.numberOfTapsRequired = 2
        addGestureRecognizer(tapGesture)
    }

    @objc func onDoubleTap(_ tapGesture : UITapGestureRecognizer)
    {
        setZoomScale(zoomScale > Constant.ZoomInScale ?  Constant.ZoomInScale : Constant.ZoomOutScale, animated: true)
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView?
    {
        return containerView
    }
}
