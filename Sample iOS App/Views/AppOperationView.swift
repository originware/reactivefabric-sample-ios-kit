//
// Reactive Fabric iOS Demo App.
//
// Created by Terry Stillone (http://www.originware.com) on 21/06/15.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation
import UIKit
import QuartzCore

import ReactiveFabric

public struct Animation
{
    enum eRequest
    {
        case eBeginSession((eReply) -> Void)
        case eStartBeadAnimation(UIBezierPath, CALayer, CALayer, CALayer)
        case eStopBeadAnimations(CALayer, CALayer, CALayer)
        case eEndSession
    }

    enum eReply
    {
        case eAnimationsCompleted
    }

    fileprivate enum eCommand
    {
        case eTranslateBeadAlongPath(CALayer, UIBezierPath)
        case eMorphPath(CALayer, UIBezierPath)
        case eFadeAllOut

        static func isTranslateBeadAlongPathID(_ value : Int) -> Bool     { return (value & 2 << 10) != 0 }
        static func isMorphPathID(_ value : Int) -> Bool                  { return (value & 2 << 11) != 0 }
        static func isFadeAllOutID(_ value : Int) -> Bool                 { return (value & 2 << 12) != 0 }

        func nextAnimationID(_ counter : inout Int) -> Int
        {
            counter += 1

            if counter >= (2 << 10)
            {
                counter = 0
            }

            return counter | BaseID
        }

        fileprivate var BaseID : Int {

            switch self
            {
                case .eTranslateBeadAlongPath:                           return 2 << 10
                case .eMorphPath:                                        return 2 << 11
                case .eFadeAllOut:                                       return 2 << 12
            }
        }
    }
}

private struct Layers
{
    fileprivate class LayerDelegateDirector: NSObject, CALayerDelegate
    {
        var m_view : UIView

        init(view : UIView)
        {
            m_view = view
        }

        func draw(_ layer: CALayer, in ctx: CGContext)
        {
            UIGraphicsPushContext(ctx)
            m_view.draw(layer, in: ctx)
            UIGraphicsPopContext()
        }
    }

    fileprivate static func createLayerWithDelegateDirector(_ bounds : CGRect, director: Layers.LayerDelegateDirector) -> CALayer
    {
        let layer = CALayer()

        layer.opacity = 1.0
        layer.backgroundColor = UIColor.clear.cgColor
        layer.frame = bounds
        layer.delegate = director

        return layer
    }

    fileprivate static func createPathMorphShapeLayer(_ bounds : CGRect, pathColor : CGColor) -> CAShapeLayer
    {
        let layer = CAShapeLayer()

        layer.strokeColor = pathColor
        layer.fillColor = nil
        layer.opacity = 1.0

        layer.backgroundColor = UIColor.clear.cgColor
        layer.frame = bounds

        return layer
    }

    fileprivate static func createBeadShapeLayer() -> CAShapeLayer
    {
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: -12, y: -7, width: 24, height: 14))
        let layer = CAShapeLayer()

        layer.path = ovalPath.cgPath
        layer.backgroundColor = UIColor.clear.cgColor
        layer.strokeColor = UIColor.green.cgColor
        layer.fillColor = UIColor.blue.cgColor

        layer.lineWidth = 4
        layer.opacity = 0.0

        return layer
    }
}

private struct Animations
{
    static let BaseAnimationTime = CFTimeInterval(0.2)
    static let FadeAnimationTime = CFTimeInterval(5.0)
    static let KeyID: String = "AnimationID"

    fileprivate static func createBeadTranslateAlongPath_Animation(_ path: UIBezierPath) -> CAAnimation
    {
        // Create the keyframe animation of the bead following along the path.
        let translateBeadAlongPathAnimation: CAKeyframeAnimation = CAKeyframeAnimation(keyPath: "position")

        translateBeadAlongPathAnimation.path = path.cgPath
        translateBeadAlongPathAnimation.timingFunctions = [CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)]
        translateBeadAlongPathAnimation.isRemovedOnCompletion = false

        return translateBeadAlongPathAnimation
    }

    fileprivate static func createMorphPath_Animation(_ path: UIBezierPath) -> CABasicAnimation
    {
        // Create the animation to morph the thickness of the line-track of the bead.
        let morphPathAnimation: CABasicAnimation = CABasicAnimation(keyPath: "lineWidth")

        morphPathAnimation.fromValue = 2.0
        morphPathAnimation.toValue = 30.0

        morphPathAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        morphPathAnimation.isRemovedOnCompletion = false
        morphPathAnimation.autoreverses = false
        morphPathAnimation.fillMode = CAMediaTimingFillMode.both

        return morphPathAnimation
    }

    fileprivate static func createFadeAllOut_Animation() -> CAAnimation
    {
        let pathSlowFadeOutAnimation = CAKeyframeAnimation(keyPath: "opacity")

        pathSlowFadeOutAnimation.values = [1.0, 0.0]
        pathSlowFadeOutAnimation.keyTimes = [0.0, 1.0]
        pathSlowFadeOutAnimation.isRemovedOnCompletion = false
        pathSlowFadeOutAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        pathSlowFadeOutAnimation.fillMode = CAMediaTimingFillMode.both

        return pathSlowFadeOutAnimation
    }

    fileprivate static func createFadingAnimationFromAnimation(_ animation: CAAnimation) -> CAAnimationGroup
    {
        // Create a keyframe opacity animation to bring the bead into view and then out.
        let alphaAnimation = CAKeyframeAnimation(keyPath: "opacity")

        alphaAnimation.values = [0.0, 1.0, 1.0, 0.0]
        alphaAnimation.keyTimes = [0.1, 0.2, 0.8, 0.9]
        alphaAnimation.isRemovedOnCompletion = false

        // Group the move and opacity animations.
        let groupAnimation = CAAnimationGroup()

        groupAnimation.animations = [animation, alphaAnimation]
        groupAnimation.isRemovedOnCompletion = false

        return groupAnimation
    }
}

private class AnimationService : NSObject, IRfPublicElementWrapper, CAAnimationDelegate
{
    typealias ReplyFunc = (Animation.eReply) -> Void
    typealias InItem = Animation.eRequest
    typealias OutItem = Void
    
    public var element : Rf.AElement<Animation.eRequest, Void>

    private var m_replyFunc:              ReplyFunc!
    private let m_enhancePathsLayer:      CALayer
    private let m_animationRequestQueue : Rf.Notifications.Store<Animation.eRequest>
    private let m_requestEvalQueue :      Rf.Eval.Queue

    private var m_currentAnimationTime: CFTimeInterval = Animations.BaseAnimationTime
    private var m_isFadingOut = false
    private var m_currentIDCounter = 0
    
    fileprivate init(_ traceID: Rf.TraceID,  enhancePathsLayer : CALayer)
    {
        self.element = RfSDK.factory.Service(traceID)
        self.m_requestEvalQueue = Rf.Eval.Queue(traceID.appending("requestQueue"), queueType: .eSerial)
        self.m_animationRequestQueue = Rf.Notifications.Store(traceID.appending("AnimationQueue"))
        self.m_enhancePathsLayer = enhancePathsLayer

        super.init()
        
        element.consumer.onItem = { [weak self] (item: Animation.eRequest, vTime: Rf.VTime?) in

            guard let strongSelf = self else { return }

            strongSelf.m_requestEvalQueue.dispatch(sync: { [weak self] in

                guard let strongSelf2 = self else { return }

                strongSelf2.m_animationRequestQueue.queue(item: item)

                if strongSelf2.m_animationRequestQueue.itemCount == 1
                {
                    strongSelf2.processNextAnimationRequest()
                }
            })
        }
    }

    /// Process the next queued animation request.
    private func processNextAnimationRequest()
    {
        /// Get the next queued animation request.
        func getNextRequest() -> Animation.eRequest?
        {
            var result : Animation.eRequest? = nil

            m_requestEvalQueue.dispatch(sync: { [weak self] in

                guard let strongSelf = self else { return }

                if let request = strongSelf.m_animationRequestQueue.unqueue()
                {
                    result = request
                }
            })

            return result
        }

        if let animationRequest = getNextRequest()
        {
            switch animationRequest
            {
                case .eBeginSession(let reply):

                    m_replyFunc = reply

                case .eStartBeadAnimation(let (path, beadTranslateAnimationLayer, pathMorphAnimationLayer, enhancePathsLayer)):

                    // Cancel any fade outs.
                    enhancePathsLayer.removeAllAnimations()
                    m_isFadingOut = false

                    // Dispatch the animation.
                    run(animationCommand: Animation.eCommand.eTranslateBeadAlongPath(beadTranslateAnimationLayer, path))
                    run(animationCommand: Animation.eCommand.eMorphPath(pathMorphAnimationLayer, path))

                case .eStopBeadAnimations(let (beadTranslateAnimationLayer, pathMorphAnimationLayer, enhancePathsLayer)):

                    beadTranslateAnimationLayer.removeAllAnimations()
                    pathMorphAnimationLayer.removeAllAnimations()
                    enhancePathsLayer.removeAllAnimations()

                    break

                case .eEndSession:

                    m_replyFunc = nil
            }
        }
    }

    /// Execute a given animation command.
    private func run(animationCommand: Animation.eCommand)
    {
        RfSDK.EvalQueues.UI.dispatch(async: { [unowned self] in

            switch animationCommand
            {
                case .eTranslateBeadAlongPath(let (layer, path)):

                    // Speed up animations if the animation queue grows beyond threshold.
                    if self.m_animationRequestQueue.itemCount > 2
                    {
                        self.m_currentAnimationTime *= 0.9
                    }

                    let beadAnimation = Animations.createBeadTranslateAlongPath_Animation(path)
                    let fadingBeadAnimation = Animations.createFadingAnimationFromAnimation(beadAnimation)
                    let animationID = animationCommand.nextAnimationID(&self.m_currentIDCounter)

                    fadingBeadAnimation.delegate = self
                    fadingBeadAnimation.duration = self.m_currentAnimationTime
                    fadingBeadAnimation.setValue(animationID, forKey:Animations.KeyID)

                    layer.add(fadingBeadAnimation, forKey: Animations.KeyID)

                case .eMorphPath(let (layer, path)):

                    let morphPathAnimation = Animations.createMorphPath_Animation(path)
                    let fadingMorphPathAnimation = Animations.createFadingAnimationFromAnimation(morphPathAnimation)
                    let shapeLayer = layer as! CAShapeLayer
                    let animationID = animationCommand.nextAnimationID(&self.m_currentIDCounter)

                    fadingMorphPathAnimation.delegate = self
                    fadingMorphPathAnimation.duration = self.m_currentAnimationTime / 2
                    fadingMorphPathAnimation.setValue(animationID, forKey:Animations.KeyID)

                    shapeLayer.path = path.cgPath
                    layer.add(fadingMorphPathAnimation, forKey: Animations.KeyID)

                default:

                    // do nothing
                    break;
            }
        })
    }

    func animationDidStop(_ anim: CAAnimation, finished flag: Bool)
    {
        if let animationID = anim.value(forKey: Animations.KeyID) as? Int
        {
            switch animationID
            {
                case _ where Animation.eCommand.isTranslateBeadAlongPathID(animationID):

                    processNextAnimationRequest()

                    if !m_isFadingOut
                    {
                        m_isFadingOut = true

                        let fadeAnimationID = Animation.eCommand.eFadeAllOut.nextAnimationID(&self.m_currentIDCounter)
                        let fadeOutAllAnimation = Animations.createFadeAllOut_Animation()

                        fadeOutAllAnimation.duration = Animations.FadeAnimationTime
                        fadeOutAllAnimation.delegate = self
                        fadeOutAllAnimation.setValue(fadeAnimationID, forKey:Animations.KeyID)

                        self.m_enhancePathsLayer.add(fadeOutAllAnimation, forKey: Animations.KeyID)
                    }

                case _ where Animation.eCommand.isFadeAllOutID(animationID):

                    if flag
                    {
                        m_replyFunc(.eAnimationsCompleted)

                        m_currentAnimationTime = Animations.BaseAnimationTime
                        m_enhancePathsLayer.opacity = 0.0
                    }

                default:
                    break
            }
        }
    }
}

class AppOperationView : UIView
{
    fileprivate struct Constants
    {
        static let StrokeColor = UIColor(red: 0.023, green: 0.071, blue: 0.20, alpha: 1)
        static let StrokeEnhanceColor = UIColor.green
        static let StrokeEnhanceWidth = CGFloat(40)
    }

    fileprivate struct Paths
    {
        var pathsAreSet = false

        var fromKeyboard_To_POIKeywordMatcher: UIBezierPath? = nil
        var fromLocation_To_POIRequestor: UIBezierPath? = nil
        var fromReachability_To_POIRequestor: UIBezierPath? = nil

        var fromPOIKeywordMatcher_To_POIKeywordsResults: UIBezierPath? = nil
        var fromPOIKeywordMatcher_To_LogAdapter: UIBezierPath? = nil

        var fromPOIKeywordMatcher_To_POIRequestor: UIBezierPath? = nil
        var fromPOIKeywordMatcher_To_POILocator: UIBezierPath? = nil

        var fromPOILocator_To_LogAdaptor: UIBezierPath? = nil
        var fromPOILocator_To_MapAdaptor: UIBezierPath? = nil

        var fromPOILocator_To_POIPortalRxPeer: UIBezierPath? = nil
        var fromPOIPortalRxPeer_To_POILocator: UIBezierPath? = nil

        var fromPOIPortalRxPeer_To_URLSessionRxPeer: UIBezierPath? = nil
        var fromURLSessionRxPeer_To_POIPortalRxPeer: UIBezierPath? = nil
    }

    fileprivate class State
    {
        var paths = Paths()

        // Animations.
        var beadTranslateAnimationLayer: CAShapeLayer
        var pathMorphAnimationLayer: CAShapeLayer
        var enhancePathsLayer: CALayer
        var diagramLayer: CALayer
        var diagramLayerDirector: Layers.LayerDelegateDirector
        var enhancedPathLayerDirector: Layers.LayerDelegateDirector

        // Pathing
        var pathsToEnhance = [UIBezierPath]()
        var currentActiveAppEvents = Set<eAppOpEventType>()

        // Animation Service.
        var animationService: AnimationService

        let traceID: Rf.TraceID

        init(_ traceID: Rf.TraceID, parentView : UIView)
        {
            self.traceID = traceID
            
            // Directors
            diagramLayerDirector = Layers.LayerDelegateDirector(view: parentView)
            enhancedPathLayerDirector = Layers.LayerDelegateDirector(view: parentView)

            // Layers
            beadTranslateAnimationLayer = Layers.createBeadShapeLayer()
            pathMorphAnimationLayer = Layers.createPathMorphShapeLayer(parentView.bounds, pathColor: Constants.StrokeColor.cgColor)
            enhancePathsLayer = Layers.createLayerWithDelegateDirector(parentView.bounds, director: enhancedPathLayerDirector)
            diagramLayer = Layers.createLayerWithDelegateDirector(parentView.bounds, director: diagramLayerDirector)

            // Services
            animationService = AnimationService(traceID.appending("AnimationService"), enhancePathsLayer: enhancePathsLayer)

            parentView.layer.addSublayer(enhancePathsLayer)
            parentView.layer.addSublayer(pathMorphAnimationLayer)
            parentView.layer.addSublayer(diagramLayer)
            parentView.layer.addSublayer(beadTranslateAnimationLayer)
        }

        deinit
        {
            animationService.consumer.notify(item: Animation.eRequest.eStopBeadAnimations(beadTranslateAnimationLayer, pathMorphAnimationLayer, enhancePathsLayer), vTime: nil)

            beadTranslateAnimationLayer.removeFromSuperlayer()
            pathMorphAnimationLayer.removeFromSuperlayer()
            enhancePathsLayer.removeFromSuperlayer()
            diagramLayer.removeFromSuperlayer()
        }
    }
    
    fileprivate var m_state : State? = nil

    override func didMoveToSuperview()
    {
        if superview != nil
        {
            // Begin animation service.
            m_state!.animationService.consumer.notify(item: Animation.eRequest.eBeginSession({ [weak self] (reply : Animation.eReply) in

                RfSDK.EvalQueues.UI.dispatch(async: {

                    guard let strongSelf = self else { return }

                    switch reply
                    {
                        case .eAnimationsCompleted:
                            
                            strongSelf.m_state!.pathsToEnhance.removeAll()
                            strongSelf.m_state!.currentActiveAppEvents.removeAll()
                    }
                })
                
            }), vTime: nil)
        }
    }

    override func willMove(toSuperview newSuperview: UIView?)
    {
        if newSuperview != nil
        {
            m_state = State(Rf.TraceID("AppOperationView"), parentView: self)

            layer.setNeedsLayout()
            m_state!.diagramLayer.setNeedsDisplay()
        }
        else
        {
            shutdown()
        }
    }

    func notify(appOpEvent: eAppOpEventType, vTime: Rf.VTime?)
    {
        RfSDK.EvalQueues.UI.dispatch(async: { [weak self] in

            guard let strongSelf = self else { return }

            strongSelf.notifyInMainThread(appOpEvent: appOpEvent, vTime: vTime)
        })
    }

    func notifyInMainThread(appOpEvent: eAppOpEventType, vTime: Rf.VTime?)
    {
        if (self.layer.opacity == 0) || (m_state == nil) { return }

        let pathAlreadyDrawn = m_state!.currentActiveAppEvents.contains(appOpEvent)
        let path = appOpEventToPath(appOpEvent)

        if path == nil { return }

        // Clear any running fade out animations.
        m_state!.animationService.consumer.notify(item: Animation.eRequest.eStartBeadAnimation(path!, m_state!.beadTranslateAnimationLayer, m_state!.pathMorphAnimationLayer, m_state!.enhancePathsLayer), vTime: nil)

        if !pathAlreadyDrawn
        {
            m_state!.currentActiveAppEvents.insert(appOpEvent)
            m_state!.pathsToEnhance.append(path!)

            m_state!.enhancePathsLayer.removeAllAnimations()
            m_state!.enhancePathsLayer.opacity = 1.0

            m_state!.enhancePathsLayer.setNeedsDisplay()
        }
    }

    func shutdown()
    {
        m_state = nil
    }

    fileprivate func appOpEventToPath(_ appOpEvent: eAppOpEventType) -> UIBezierPath?
    {
        if m_state == nil    { return nil }

        let paths = m_state!.paths

        switch appOpEvent
        {
            case .eFromKeyboard_To_POIKeywordMatcherPath:
                return paths.fromKeyboard_To_POIKeywordMatcher

            case .eFromLocation_To_POIRequestor:
                return paths.fromLocation_To_POIRequestor

            case .eFromReachability_To_POIRequestor:
                return paths.fromReachability_To_POIRequestor

            case .eFromPOIKeywordMatcher_To_POIKeywordsResults:
                return paths.fromPOIKeywordMatcher_To_POIKeywordsResults

            case .eFromPOIKeywordMatcher_To_LogAdapter:
                return paths.fromPOIKeywordMatcher_To_LogAdapter

            case .eFromPOIKeywordMatcher_To_POIRequestor:
                return paths.fromPOIKeywordMatcher_To_POIRequestor

            case .eFromPOIRequestor_To_POIPortal:
                return paths.fromPOIKeywordMatcher_To_POILocator

            case .eFromPOILocator_To_LogAdaptor:
                return paths.fromPOILocator_To_LogAdaptor

            case .eFromPOILocator_To_MapAdaptor:
                return paths.fromPOILocator_To_MapAdaptor

            case .eFromPOIPortalService_To_GooglePlacesService:
                return paths.fromPOILocator_To_POIPortalRxPeer

            case .eFromGooglePlaces_To_POIService:
                return paths.fromPOIPortalRxPeer_To_POILocator

            case .eFromGooglePlacesService_To_HTTPService:
                return paths.fromPOIPortalRxPeer_To_URLSessionRxPeer

            case .eFromHTTPService_To_GooglePlacesService:
                return paths.fromURLSessionRxPeer_To_POIPortalRxPeer
        }
    }

    override func draw(_ layer: CALayer, in context: CGContext)
    {
        switch layer
        {
            case m_state!.enhancePathsLayer:
                drawEnhancedPathsLayer(layer, in: context)

            case m_state!.diagramLayer:
                drawDiagramLayer(layer, in: context)

            default:
                super.draw(layer, in: context)
        }
    }

    func drawEnhancedPathsLayer(_ layer: CALayer, in context: CGContext)
    {
        func strokeLines()
        {
            let shadowOffset = CGSize(width: 2, height: 2)

            context.setLineWidth(Constants.StrokeEnhanceWidth)
            context.setStrokeColor(Constants.StrokeEnhanceColor.cgColor)
            context.setShadow(offset: shadowOffset, blur: 20.0, color: Constants.StrokeEnhanceColor.cgColor)

            for path in m_state!.pathsToEnhance
            {
                path.stroke()
            }
        }

        UIGraphicsPushContext(context)
        strokeLines()
        UIGraphicsPopContext()
    }

    func drawDiagramLayer(_ layer: CALayer!, in context: CGContext!)
    {
        // Drawing code generated by PaintCode 2.3.2
        
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()
        
        // Clear the view.
        context?.setFillColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        context?.fill(layer.bounds)

        //// Color Declarations
        let color3 = UIColor(red: 0.023, green: 0.071, blue: 0.206, alpha: 1.000)
        let gradientColor2 = UIColor(red: 0.822, green: 0.840, blue: 0.924, alpha: 1.000)
        let shadow2Color = UIColor(red: 1.000, green: 0.989, blue: 0.989, alpha: 1.000)
        let color4 = UIColor(red: 0.310, green: 0.373, blue: 0.550, alpha: 1.000)
        let gradientColor = UIColor(red: 0.660, green: 0.637, blue: 0.851, alpha: 0.911)
        let greenGradientColor = UIColor(red: 0.292, green: 0.643, blue: 0.530, alpha: 1.000)
        let greenGradientColor2 = UIColor(red: 0.858, green: 0.974, blue: 0.861, alpha: 1.000)
        let gradientColor3 = UIColor(red: 0.672, green: 0.788, blue: 0.337, alpha: 1.000)
        let gradientColor4 = UIColor(red: 0.617, green: 0.692, blue: 0.289, alpha: 1.000)
        let softBlackShadowColor = UIColor(red: 0.000, green: 0.000, blue: 0.000, alpha: 0.580)
        let gradient2Color = UIColor(red: 0.381, green: 0.391, blue: 0.966, alpha: 1.000)
        let blueGradientColor = UIColor(red: 0.833, green: 0.876, blue: 0.931, alpha: 1.000)
        let color7 = UIColor(red: 0.902, green: 0.809, blue: 1.000, alpha: 1.000)
        
        //// Gradient Declarations

        let purpleGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [gradientColor.cgColor, gradientColor2.cgColor] as CFArray, locations: [0.08, 1])
        let greenGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [greenGradientColor.cgColor, greenGradientColor2.cgColor] as CFArray, locations: [0.1, 0.65, 1])
        let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [gradientColor3.cgColor, gradientColor4.cgColor] as CFArray, locations: [0, 1])
        let blueGradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: [gradient2Color.cgColor, blueGradientColor.cgColor] as CFArray, locations: [0, 1])
        
        //// Shadow Declarations
        let hardBlackShadow = NSShadow()
        hardBlackShadow.shadowColor = UIColor.black
        hardBlackShadow.shadowOffset = CGSize(width: 3.1, height: 3.1)
        hardBlackShadow.shadowBlurRadius = 5
        let softBlackShadow = NSShadow()
        softBlackShadow.shadowColor = softBlackShadowColor
        softBlackShadow.shadowOffset = CGSize(width: 3.1, height: 3.1)
        softBlackShadow.shadowBlurRadius = 11
        let whiteShadow = NSShadow()
        whiteShadow.shadowColor = shadow2Color.withAlphaComponent(0.43)
        whiteShadow.shadowOffset = CGSize(width: 3.1, height: 2.1)
        whiteShadow.shadowBlurRadius = 2
        
        //// fromPOIPortalRxPeer_To_POILocator Drawing
        let fromPOIPortalRxPeer_To_POILocatorPath = UIBezierPath()
        fromPOIPortalRxPeer_To_POILocatorPath.move(to: CGPoint(x: 372.21, y: 500))
        fromPOIPortalRxPeer_To_POILocatorPath.addCurve(to: CGPoint(x: 372.21, y: 438), controlPoint1: CGPoint(x: 372.21, y: 494.56), controlPoint2: CGPoint(x: 372.21, y: 438))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOIPortalRxPeer_To_POILocatorPath.lineWidth = 8
        fromPOIPortalRxPeer_To_POILocatorPath.stroke()
        context?.restoreGState()
        
        
        //// fromURLSessionRxPeer_To_POIPortalRxPeer Drawing
        let fromURLSessionRxPeer_To_POIPortalRxPeerPath = UIBezierPath()
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.move(to: CGPoint(x: 371.21, y: 598))
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.addCurve(to: CGPoint(x: 371.21, y: 536), controlPoint1: CGPoint(x: 371.21, y: 592.56), controlPoint2: CGPoint(x: 371.21, y: 536))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.lineWidth = 8
        fromURLSessionRxPeer_To_POIPortalRxPeerPath.stroke()
        context?.restoreGState()
        
        
        //// fromPOIKeywordMatcher_To_LogAdapter Drawing
        let fromPOIKeywordMatcher_To_LogAdapterPath = UIBezierPath()
        fromPOIKeywordMatcher_To_LogAdapterPath.move(to: CGPoint(x: 445.21, y: 199.93))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurve(to: CGPoint(x: 467.97, y: 199.93), controlPoint1: CGPoint(x: 445.21, y: 199.93), controlPoint2: CGPoint(x: 453.54, y: 199.41))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurve(to: CGPoint(x: 489.79, y: 199.93), controlPoint1: CGPoint(x: 473.68, y: 200.14), controlPoint2: CGPoint(x: 484.45, y: 198.02))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurve(to: CGPoint(x: 528.69, y: 366), controlPoint1: CGPoint(x: 507.82, y: 206.4), controlPoint2: CGPoint(x: 511.82, y: 338.05))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurve(to: CGPoint(x: 556.2, y: 389), controlPoint1: CGPoint(x: 535.33, y: 377), controlPoint2: CGPoint(x: 540.4, y: 386.11))
        fromPOIKeywordMatcher_To_LogAdapterPath.addCurve(to: CGPoint(x: 596.04, y: 389), controlPoint1: CGPoint(x: 577.99, y: 392.99), controlPoint2: CGPoint(x: 596.04, y: 389))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_LogAdapterPath.lineWidth = 8
        fromPOIKeywordMatcher_To_LogAdapterPath.stroke()
        context?.restoreGState()
        
        
        //// fromPOIKeywordMatcher_To_POILocator Drawing
        let fromPOIKeywordMatcher_To_POILocatorPath = UIBezierPath()
        fromPOIKeywordMatcher_To_POILocatorPath.move(to: CGPoint(x: 354.3, y: 321.16))
        fromPOIKeywordMatcher_To_POILocatorPath.addLine(to: CGPoint(x: 354.05, y: 370.16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_POILocatorPath.lineWidth = 8
        fromPOIKeywordMatcher_To_POILocatorPath.stroke()
        context?.restoreGState()
        
        
        //// fromPOIKeywordMatcher_To_POIKeywordsResults Drawing
        let fromPOIKeywordMatcher_To_POIKeywordsResultsPath = UIBezierPath()
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.move(to: CGPoint(x: 437.02, y: 196.71))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurve(to: CGPoint(x: 452.8, y: 200), controlPoint1: CGPoint(x: 437.02, y: 196.71), controlPoint2: CGPoint(x: 428.57, y: 199.87))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurve(to: CGPoint(x: 468.92, y: 200), controlPoint1: CGPoint(x: 460.87, y: 200.04), controlPoint2: CGPoint(x: 468.92, y: 200))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurve(to: CGPoint(x: 502.73, y: 207.12), controlPoint1: CGPoint(x: 478.97, y: 200.11), controlPoint2: CGPoint(x: 495.68, y: 198.04))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurve(to: CGPoint(x: 547.66, y: 234), controlPoint1: CGPoint(x: 513.51, y: 221), controlPoint2: CGPoint(x: 516.76, y: 233.34))
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.addCurve(to: CGPoint(x: 614.22, y: 234.33), controlPoint1: CGPoint(x: 594.15, y: 235), controlPoint2: CGPoint(x: 614.22, y: 234.33))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.lineWidth = 8
        fromPOIKeywordMatcher_To_POIKeywordsResultsPath.stroke()
        context?.restoreGState()
        
        
        //// fromPOILocator_To_LogAdaptor Drawing
        let fromPOILocator_To_LogAdaptorPath = UIBezierPath()
        fromPOILocator_To_LogAdaptorPath.move(to: CGPoint(x: 426.23, y: 413))
        fromPOILocator_To_LogAdaptorPath.addCurve(to: CGPoint(x: 458.98, y: 408.62), controlPoint1: CGPoint(x: 426.23, y: 413), controlPoint2: CGPoint(x: 450.82, y: 410.21))
        fromPOILocator_To_LogAdaptorPath.addCurve(to: CGPoint(x: 514.46, y: 390), controlPoint1: CGPoint(x: 466.77, y: 407.11), controlPoint2: CGPoint(x: 486.1, y: 388.93))
        fromPOILocator_To_LogAdaptorPath.addCurve(to: CGPoint(x: 596.04, y: 390.03), controlPoint1: CGPoint(x: 541.02, y: 391), controlPoint2: CGPoint(x: 596.04, y: 390.03))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOILocator_To_LogAdaptorPath.lineWidth = 8
        fromPOILocator_To_LogAdaptorPath.stroke()
        context?.restoreGState()
        
        
        //// fromPOILocator_To_MapAdaptor Drawing
        let fromPOILocator_To_MapAdaptorPath = UIBezierPath()
        fromPOILocator_To_MapAdaptorPath.move(to: CGPoint(x: 428.13, y: 410))
        fromPOILocator_To_MapAdaptorPath.addCurve(to: CGPoint(x: 458.97, y: 419), controlPoint1: CGPoint(x: 428.13, y: 410), controlPoint2: CGPoint(x: 449.25, y: 407.81))
        fromPOILocator_To_MapAdaptorPath.addCurve(to: CGPoint(x: 514.46, y: 535), controlPoint1: CGPoint(x: 462.44, y: 423), controlPoint2: CGPoint(x: 501.12, y: 535))
        fromPOILocator_To_MapAdaptorPath.addCurve(to: CGPoint(x: 604.58, y: 534.71), controlPoint1: CGPoint(x: 558.1, y: 535), controlPoint2: CGPoint(x: 604.58, y: 534.71))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOILocator_To_MapAdaptorPath.lineWidth = 8
        fromPOILocator_To_MapAdaptorPath.stroke()
        context?.restoreGState()
        
        
        //// fromLocation_To_POIRequestor Drawing
        let fromLocation_To_POIRequestorPath = UIBezierPath()
        fromLocation_To_POIRequestorPath.move(to: CGPoint(x: 272.55, y: 295.36))
        fromLocation_To_POIRequestorPath.addCurve(to: CGPoint(x: 265.06, y: 295.36), controlPoint1: CGPoint(x: 272.55, y: 295.36), controlPoint2: CGPoint(x: 270.61, y: 295.36))
        fromLocation_To_POIRequestorPath.addCurve(to: CGPoint(x: 221.03, y: 295.36), controlPoint1: CGPoint(x: 253.88, y: 295.36), controlPoint2: CGPoint(x: 234.14, y: 295.36))
        fromLocation_To_POIRequestorPath.addCurve(to: CGPoint(x: 185.44, y: 327.58), controlPoint1: CGPoint(x: 202.3, y: 295.36), controlPoint2: CGPoint(x: 195.74, y: 289.22))
        fromLocation_To_POIRequestorPath.addCurve(to: CGPoint(x: 167.64, y: 378.2), controlPoint1: CGPoint(x: 182.87, y: 337.16), controlPoint2: CGPoint(x: 176.63, y: 375.14))
        fromLocation_To_POIRequestorPath.addCurve(to: CGPoint(x: 124.56, y: 378.2), controlPoint1: CGPoint(x: 162.34, y: 380), controlPoint2: CGPoint(x: 124.56, y: 378.2))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromLocation_To_POIRequestorPath.lineWidth = 8
        fromLocation_To_POIRequestorPath.stroke()
        context?.restoreGState()
        
        
        //// fromPOIKeywordMatcher_To_POIRequestor Drawing
        let fromPOIKeywordMatcher_To_POIRequestorPath = UIBezierPath()
        fromPOIKeywordMatcher_To_POIRequestorPath.move(to: CGPoint(x: 351.5, y: 214.93))
        fromPOIKeywordMatcher_To_POIRequestorPath.addLine(to: CGPoint(x: 351.29, y: 254.93))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOIKeywordMatcher_To_POIRequestorPath.lineWidth = 8
        fromPOIKeywordMatcher_To_POIRequestorPath.stroke()
        context?.restoreGState()
        
        
        //// Rectangle 33 Drawing
        let rectangle33Path = UIBezierPath(roundedRect: CGRect(x: 262, y: 175, width: 187, height: 40), cornerRadius: 10)
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        rectangle33Path.addClip()
        context?.drawLinearGradient(purpleGradient!, start: CGPoint(x: 301.67, y: 141.17), end: CGPoint(x: 409.33, y: 248.83), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        
        
        //// fromPOIPortalRxPeer_To_URLSessionRxPeer Drawing
        let fromPOIPortalRxPeer_To_URLSessionRxPeerPath = UIBezierPath()
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.move(to: CGPoint(x: 329.7, y: 526.16))
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.addLine(to: CGPoint(x: 329.38, y: 587.16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.lineWidth = 8
        fromPOIPortalRxPeer_To_URLSessionRxPeerPath.stroke()
        context?.restoreGState()
        
        
        //// fromPOILocator_To_POIPortalRxPeer Drawing
        let fromPOILocator_To_POIPortalRxPeerPath = UIBezierPath()
        fromPOILocator_To_POIPortalRxPeerPath.move(to: CGPoint(x: 329.76, y: 410.16))
        fromPOILocator_To_POIPortalRxPeerPath.addLine(to: CGPoint(x: 329.38, y: 482.16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromPOILocator_To_POIPortalRxPeerPath.lineWidth = 8
        fromPOILocator_To_POIPortalRxPeerPath.stroke()
        context?.restoreGState()
        
        
        //// fromKeyboard_To_POIKeywordMatcher Drawing
        let fromKeyboard_To_POIKeywordMatcherPath = UIBezierPath()
        fromKeyboard_To_POIKeywordMatcherPath.move(to: CGPoint(x: 137, y: 227.73))
        fromKeyboard_To_POIKeywordMatcherPath.addCurve(to: CGPoint(x: 163, y: 226.03), controlPoint1: CGPoint(x: 137, y: 227.73), controlPoint2: CGPoint(x: 155.29, y: 229))
        fromKeyboard_To_POIKeywordMatcherPath.addCurve(to: CGPoint(x: 192.02, y: 201.34), controlPoint1: CGPoint(x: 170.37, y: 223.2), controlPoint2: CGPoint(x: 177.58, y: 201.27))
        fromKeyboard_To_POIKeywordMatcherPath.addCurve(to: CGPoint(x: 258.44, y: 201.83), controlPoint1: CGPoint(x: 249.78, y: 201.65), controlPoint2: CGPoint(x: 258.44, y: 201.83))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromKeyboard_To_POIKeywordMatcherPath.lineWidth = 8
        fromKeyboard_To_POIKeywordMatcherPath.stroke()
        context?.restoreGState()
        
        
        //// Bezier 7 Drawing
        context?.saveGState()
        context?.translateBy(x: 669.52, y: 236)
        context?.rotate(by: -179.95 * CGFloat(Float.pi) / 180)
        
        let bezier7Path = UIBezierPath()
        bezier7Path.move(to: CGPoint(x: -37.52, y: -66.97))
        bezier7Path.addCurve(to: CGPoint(x: 51.69, y: -0.97), controlPoint1: CGPoint(x: -36.37, y: -63.75), controlPoint2: CGPoint(x: 50.55, y: -29.94))
        bezier7Path.addCurve(to: CGPoint(x: -37.52, y: 65.03), controlPoint1: CGPoint(x: 52.83, y: 28.01), controlPoint2: CGPoint(x: -37.52, y: 65.03))
        bezier7Path.addLine(to: CGPoint(x: -37.52, y: -66.97))
        bezier7Path.addLine(to: CGPoint(x: -37.52, y: -66.97))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        bezier7Path.addClip()
        context?.drawLinearGradient(greenGradient!, start: CGPoint(x: 51.7, y: -0.97), end: CGPoint(x: -37.52, y: -0.97), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        bezier7Path.lineWidth = 1.5
        bezier7Path.stroke()
        
        context?.restoreGState()
        
        
        //// Text 13 Drawing
        context?.saveGState()
        context?.translateBy(x: 667.77, y: 236.92)
        context?.rotate(by: 90 * CGFloat(Float.pi) / 180)
        
        let text13Rect = CGRect(x: -55.92, y: -38.42, width: 111.84, height: 76.84)
        let text13TextContent = NSString(string: "POI Keyword\nMatch Results\nView\nAdapter")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text13Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text13Style.alignment = NSTextAlignment.center
        
        let text13FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 13)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text13Style]
        
        let text13TextHeight: CGFloat = text13TextContent.boundingRect(with: CGSize(width: text13Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text13FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text13Rect);
        text13TextContent.draw(in: CGRect(x: text13Rect.minX, y: text13Rect.minY + (text13Rect.height - text13TextHeight) / 2, width: text13Rect.width, height: text13TextHeight), withAttributes: text13FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 5 Drawing
        let rectangle5Path = UIBezierPath(roundedRect: CGRect(x: 285, y: 273, width: 145, height: 64), cornerRadius: 10)
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        rectangle5Path.addClip()
        context?.drawLinearGradient(gradient!, start: CGPoint(x: 357.5, y: 273), end: CGPoint(x: 357.5, y: 337), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        
        
        //// Text 14 Drawing
        context?.saveGState()
        context?.translateBy(x: 357.4, y: 302.99)
        context?.rotate(by: -0.39 * CGFloat(Float.pi) / 180)
        
        let text14Rect = CGRect(x: -57.55, y: -21.01, width: 115.11, height: 42.03)
        let text14TextContent = NSString(string: "POI Requestor")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text14Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text14Style.alignment = NSTextAlignment.center
        
        let text14FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text14Style]
        
        let text14TextHeight: CGFloat = text14TextContent.boundingRect(with: CGSize(width: text14Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text14FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text14Rect);
        text14TextContent.draw(in: CGRect(x: text14Rect.minX, y: text14Rect.minY + (text14Rect.height - text14TextHeight) / 2, width: text14Rect.width, height: text14TextHeight), withAttributes: text14FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 10 Drawing
        let rectangle10Path = UIBezierPath(roundedRect: CGRect(x: 256, y: 495, width: 189, height: 38), cornerRadius: 10)
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        rectangle10Path.addClip()
        context?.drawLinearGradient(purpleGradient!, start: CGPoint(x: 296.67, y: 460.17), end: CGPoint(x: 404.33, y: 567.83), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        
        
        //// Text 5 Drawing
        context?.saveGState()
        context?.translateBy(x: 350.15, y: 514.23)
        
        let text5Rect = CGRect(x: -92.16, y: -8.77, width: 184.32, height: 17.54)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text5Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text5Style.alignment = NSTextAlignment.center
        
        let text5FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text5Style]
        
        "Google Places Service".draw(in: text5Rect, withAttributes: text5FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 11 Drawing
        let rectangle11Path = UIBezierPath(roundedRect: CGRect(x: 256, y: 589, width: 190, height: 39), cornerRadius: 10)
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        rectangle11Path.addClip()
        context?.drawLinearGradient(purpleGradient!, start: CGPoint(x: 296.67, y: 554.17), end: CGPoint(x: 405.33, y: 662.83), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        
        
        //// Text 15 Drawing
        context?.saveGState()
        context?.translateBy(x: 353, y: 608.73)
        
        let text15Rect = CGRect(x: -83, y: -12.27, width: 166, height: 24.54)
        let text15TextContent = NSString(string: "HTTP Service")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text15Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text15Style.alignment = NSTextAlignment.center
        
        let text15FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text15Style]
        
        let text15TextHeight: CGFloat = text15TextContent.boundingRect(with: CGSize(width: text15Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text15FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text15Rect);
        text15TextContent.draw(in: CGRect(x: text15Rect.minX, y: text15Rect.minY + (text15Rect.height - text15TextHeight) / 2, width: text15Rect.width, height: text15TextHeight), withAttributes: text15FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 13 Drawing
        let rectangle13Path = UIBezierPath()
        rectangle13Path.move(to: CGPoint(x: 122.08, y: 242.5))
        rectangle13Path.addLine(to: CGPoint(x: 142.95, y: 228.5))
        rectangle13Path.addLine(to: CGPoint(x: 122.08, y: 214.5))
        rectangle13Path.addLine(to: CGPoint(x: 122.08, y: 242.5))
        rectangle13Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle13Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle13Path.lineWidth = 1
        rectangle13Path.stroke()
        
        
        //// Bezier 5 Drawing
        let bezier5Path = UIBezierPath()
        bezier5Path.move(to: CGPoint(x: 37.5, y: 159))
        bezier5Path.addCurve(to: CGPoint(x: 127.14, y: 227), controlPoint1: CGPoint(x: 38.65, y: 162.32), controlPoint2: CGPoint(x: 125.99, y: 197.15))
        bezier5Path.addCurve(to: CGPoint(x: 37.5, y: 295), controlPoint1: CGPoint(x: 128.29, y: 256.85), controlPoint2: CGPoint(x: 37.5, y: 295))
        bezier5Path.addLine(to: CGPoint(x: 37.5, y: 159))
        bezier5Path.addLine(to: CGPoint(x: 37.5, y: 159))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        bezier5Path.addClip()
        context?.drawLinearGradient(blueGradient!, start: CGPoint(x: 127.15, y: 227), end: CGPoint(x: 37.5, y: 227), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        bezier5Path.lineWidth = 1.5
        bezier5Path.stroke()
        
        
        //// Text 10 Drawing
        context?.saveGState()
        context?.translateBy(x: 86.04, y: 227.42)
        context?.rotate(by: -90 * CGFloat(Float.pi) / 180)
        
        let text10Rect = CGRect(x: -43.42, y: -40.96, width: 86.84, height: 81.93)
        let text10TextContent = NSString(string: "Keyboard\nDevice \nAdaptor\n")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text10Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text10Style.alignment = NSTextAlignment.center
        
        let text10FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 13)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text10Style]
        
        let text10TextHeight: CGFloat = text10TextContent.boundingRect(with: CGSize(width: text10Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text10FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text10Rect);
        text10TextContent.draw(in: CGRect(x: text10Rect.minX, y: text10Rect.minY + (text10Rect.height - text10TextHeight) / 2, width: text10Rect.width, height: text10TextHeight), withAttributes: text10FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Text 4 Drawing
        context?.saveGState()
        context?.translateBy(x: 375.79, y: 104.43)
        
        let text4Rect = CGRect(x: -205.08, y: -21.04, width: 410.15, height: 42.09)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        let text4Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text4Style.alignment = NSTextAlignment.center
        
        let text4FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 20)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text4Style]
        
        "Live App Operation Visualisation".draw(in: text4Rect, withAttributes: text4FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Text 6 Drawing
        context?.saveGState()
        context?.translateBy(x: 373.71, y: 133.46)
        
        let text6Rect = CGRect(x: -357.29, y: -15.54, width: 714.58, height: 31.08)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text6Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text6Style.alignment = NSTextAlignment.center
        
        let text6FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 15)!, NSAttributedString.Key.foregroundColor: UIColor.darkGray, NSAttributedString.Key.paragraphStyle: text6Style]
        
        "Visualisation of Event Notifications Flowing Through The App - enter text to observe".draw(in: text6Rect, withAttributes: text6FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle Drawing
        let rectanglePath = UIBezierPath()
        rectanglePath.move(to: CGPoint(x: 116.49, y: 393.5))
        rectanglePath.addLine(to: CGPoint(x: 137.37, y: 379.5))
        rectanglePath.addLine(to: CGPoint(x: 116.49, y: 365.5))
        rectanglePath.addLine(to: CGPoint(x: 116.49, y: 393.5))
        rectanglePath.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectanglePath.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectanglePath.lineWidth = 1
        rectanglePath.stroke()
        
        
        //// Bezier Drawing
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 40, y: 321))
        bezierPath.addCurve(to: CGPoint(x: 124.08, y: 378), controlPoint1: CGPoint(x: 41.08, y: 323.78), controlPoint2: CGPoint(x: 123, y: 352.98))
        bezierPath.addCurve(to: CGPoint(x: 40, y: 435), controlPoint1: CGPoint(x: 125.16, y: 403.02), controlPoint2: CGPoint(x: 40, y: 435))
        bezierPath.addLine(to: CGPoint(x: 40, y: 321))
        bezierPath.addLine(to: CGPoint(x: 40, y: 321))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        bezierPath.addClip()
        context?.drawLinearGradient(blueGradient!, start: CGPoint(x: 124.09, y: 378), end: CGPoint(x: 40, y: 378), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        bezierPath.lineWidth = 1.5
        bezierPath.stroke()
        
        
        //// Text Drawing
        context?.saveGState()
        context?.translateBy(x: 81.41, y: 378.42)
        context?.rotate(by: -90 * CGFloat(Float.pi) / 180)
        
        let textRect = CGRect(x: -43.42, y: -33.59, width: 86.84, height: 67.18)
        let textTextContent = NSString(string: "Location\nDevice \nAdaptor\n")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let textStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.center
        
        let textFontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 13)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: textStyle]
        
        let textTextHeight: CGFloat = textTextContent.boundingRect(with: CGSize(width: textRect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: textFontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: textRect);
        textTextContent.draw(in: CGRect(x: textRect.minX, y: textRect.minY + (textRect.height - textTextHeight) / 2, width: textRect.width, height: textTextHeight), withAttributes: textFontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 6 Drawing
        context?.saveGState()
        context?.translateBy(x: 602.59, y: 244.45)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle6Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 9, height: 16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle6Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle6Path.lineWidth = 1
        rectangle6Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 8 Drawing
        let rectangle8Path = UIBezierPath()
        rectangle8Path.move(to: CGPoint(x: 602.5, y: 249.5))
        rectangle8Path.addLine(to: CGPoint(x: 624.5, y: 235.5))
        rectangle8Path.addLine(to: CGPoint(x: 602.5, y: 221.5))
        rectangle8Path.addLine(to: CGPoint(x: 602.5, y: 249.5))
        rectangle8Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle8Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle8Path.lineWidth = 1
        rectangle8Path.stroke()
        
        
        //// Rectangle 12 Drawing
        let rectangle12Path = UIBezierPath(roundedRect: CGRect(x: 258, y: 390, width: 182, height: 39), cornerRadius: 10)
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        rectangle12Path.addClip()
        context?.drawLinearGradient(purpleGradient!, start: CGPoint(x: 349, y: 390), end: CGPoint(x: 349, y: 429), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        
        
        //// Text 16 Drawing
        context?.saveGState()
        context?.translateBy(x: 349.62, y: 409.73)
        
        let text16Rect = CGRect(x: -78.62, y: -9.27, width: 157.23, height: 18.54)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text16Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text16Style.alignment = NSTextAlignment.center
        
        let text16FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text16Style]
        
        "POI Portal Service".draw(in: text16Rect, withAttributes: text16FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 15 Drawing
        context?.saveGState()
        context?.translateBy(x: 248.95, y: 209.46)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle15Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.39, height: 16.01))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle15Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle15Path.lineWidth = 1
        rectangle15Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 16 Drawing
        let rectangle16Path = UIBezierPath()
        rectangle16Path.move(to: CGPoint(x: 248.6, y: 214.5))
        rectangle16Path.addLine(to: CGPoint(x: 263.5, y: 200.5))
        rectangle16Path.addLine(to: CGPoint(x: 248.6, y: 186.5))
        rectangle16Path.addLine(to: CGPoint(x: 248.6, y: 214.5))
        rectangle16Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle16Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle16Path.lineWidth = 1
        rectangle16Path.stroke()
        
        
        //// Group 12
        context?.saveGState()
        context?.translateBy(x: 331, y: 488.5)
        context?.rotate(by: 90.37 * CGFloat(Float.pi) / 180)
        
        
        
        //// Rectangle 25 Drawing
        context?.saveGState()
        context?.translateBy(x: -9.34, y: 8.98)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle25Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.1, height: 15.98))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle25Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle25Path.lineWidth = 1
        rectangle25Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 26 Drawing
        let rectangle26Path = UIBezierPath()
        rectangle26Path.move(to: CGPoint(x: -9.4, y: 14))
        rectangle26Path.addLine(to: CGPoint(x: 5.5, y: 0))
        rectangle26Path.addLine(to: CGPoint(x: -9.4, y: -14))
        rectangle26Path.addLine(to: CGPoint(x: -9.4, y: 14))
        rectangle26Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle26Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle26Path.lineWidth = 1
        rectangle26Path.stroke()
        
        
        
        context?.restoreGState()
        
        
        //// Group 14
        context?.saveGState()
        context?.translateBy(x: 331, y: 592.5)
        context?.rotate(by: 90.37 * CGFloat(Float.pi) / 180)
        
        
        
        //// Rectangle 27 Drawing
        context?.saveGState()
        context?.translateBy(x: -9.34, y: 8.98)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle27Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.1, height: 15.98))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle27Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle27Path.lineWidth = 1
        rectangle27Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 28 Drawing
        let rectangle28Path = UIBezierPath()
        rectangle28Path.move(to: CGPoint(x: -9.4, y: 14))
        rectangle28Path.addLine(to: CGPoint(x: 5.5, y: 0))
        rectangle28Path.addLine(to: CGPoint(x: -9.4, y: -14))
        rectangle28Path.addLine(to: CGPoint(x: -9.4, y: 14))
        rectangle28Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle28Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle28Path.lineWidth = 1
        rectangle28Path.stroke()
        
        
        
        context?.restoreGState()
        
        
        //// Group
        //// Rectangle 29 Drawing
        context?.saveGState()
        context?.translateBy(x: 380.73, y: 436.87)
        context?.rotate(by: 90.06 * CGFloat(Float.pi) / 180)
        
        let rectangle29Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.1, height: 15.16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle29Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle29Path.lineWidth = 1
        rectangle29Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 30 Drawing
        context?.saveGState()
        context?.translateBy(x: 360, y: 436.91)
        context?.rotate(by: -89.63 * CGFloat(Float.pi) / 180)
        
        let rectangle30Path = UIBezierPath()
        rectangle30Path.move(to: CGPoint(x: 0, y: 27))
        rectangle30Path.addLine(to: CGPoint(x: 14.9, y: 13.5))
        rectangle30Path.addLine(to: CGPoint(x: 0, y: 0))
        rectangle30Path.addLine(to: CGPoint(x: 0, y: 27))
        rectangle30Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle30Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle30Path.lineWidth = 1
        rectangle30Path.stroke()
        
        context?.restoreGState()
        
        
        
        
        //// Text 11 Drawing
        let text11Rect = CGRect(x: 220, y: 552, width: 110, height: 16)
        let text11Path = UIBezierPath(rect: text11Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text11Path.fill()
        context?.restoreGState()
        
        let text11Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text11Style.alignment = NSTextAlignment.center
        
        let text11FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text11Style]
        
        "URL HTTP Request".draw(in: text11Rect, withAttributes: text11FontAttributes)
        
        
        //// Text 18 Drawing
        let text18Rect = CGRect(x: 372, y: 552, width: 104, height: 16)
        let text18Path = UIBezierPath(rect: text18Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text18Path.fill()
        context?.restoreGState()
        
        let text18Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text18Style.alignment = NSTextAlignment.center
        
        let text18FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text18Style]
        
        "URL HTTP Reply".draw(in: text18Rect, withAttributes: text18FontAttributes)
        
        
        //// Text 3 Drawing
        context?.saveGState()
        context?.translateBy(x: 355.57, y: 195.89)
        context?.rotate(by: -0.39 * CGFloat(Float.pi) / 180)
        
        let text3Rect = CGRect(x: -86.31, y: -17.69, width: 172.63, height: 35.38)
        let text3TextContent = NSString(string: "POI Keyword Matcher ")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text3Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text3Style.alignment = NSTextAlignment.center
        
        let text3FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text3Style]
        
        let text3TextHeight: CGFloat = text3TextContent.boundingRect(with: CGSize(width: text3Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text3FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text3Rect);
        text3TextContent.draw(in: CGRect(x: text3Rect.minX, y: text3Rect.minY + (text3Rect.height - text3TextHeight) / 2, width: text3Rect.width, height: text3TextHeight), withAttributes: text3FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Group 10
        context?.saveGState()
        context?.translateBy(x: 356, y: 389.5)
        context?.rotate(by: 90.37 * CGFloat(Float.pi) / 180)
        
        
        
        //// Rectangle 23 Drawing
        context?.saveGState()
        context?.translateBy(x: -9.34, y: 8.98)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle23Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.1, height: 15.98))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle23Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle23Path.lineWidth = 1
        rectangle23Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 24 Drawing
        let rectangle24Path = UIBezierPath()
        rectangle24Path.move(to: CGPoint(x: -9.4, y: 14))
        rectangle24Path.addLine(to: CGPoint(x: 5.5, y: 0))
        rectangle24Path.addLine(to: CGPoint(x: -9.4, y: -14))
        rectangle24Path.addLine(to: CGPoint(x: -9.4, y: 14))
        rectangle24Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle24Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle24Path.lineWidth = 1
        rectangle24Path.stroke()
        
        
        
        context?.restoreGState()
        
        
        //// Text 19 Drawing
        let text19Rect = CGRect(x: 204, y: 450, width: 129, height: 16)
        let text19Path = UIBezierPath(rect: text19Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text19Path.fill()
        context?.restoreGState()
        
        let text19Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text19Style.alignment = NSTextAlignment.center
        
        let text19FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text19Style]
        
        "POI Location Request Request".draw(in: text19Rect, withAttributes: text19FontAttributes)
        
        
        //// Text 21 Drawing
        let text21Rect = CGRect(x: 456, y: 399, width: 133, height: 16)
        let text21Path = UIBezierPath(rect: text21Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text21Path.fill()
        context?.restoreGState()
        
        let text21Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text21Style.alignment = NSTextAlignment.center
        
        let text21FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text21Style]
        
        "POI Location Result".draw(in: text21Rect, withAttributes: text21FontAttributes)
        
        
        //// Text 7 Drawing
        let text7Rect = CGRect(x: 300, y: 228, width: 125, height: 16)
        let text7Path = UIBezierPath(rect: text7Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text7Path.fill()
        context?.restoreGState()
        
        let text7Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text7Style.alignment = NSTextAlignment.center
        
        let text7FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text7Style]
        
        "POI Keyword Matches".draw(in: text7Rect, withAttributes: text7FontAttributes)
        
        
        //// Group 9
        context?.saveGState()
        context?.translateBy(x: 353.01, y: 267.55)
        context?.rotate(by: 90.37 * CGFloat(Float.pi) / 180)
        
        
        
        //// Rectangle 21 Drawing
        context?.saveGState()
        context?.translateBy(x: -7.39, y: 8.98)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle21Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.1, height: 15.98))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle21Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle21Path.lineWidth = 1
        rectangle21Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 22 Drawing
        let rectangle22Path = UIBezierPath()
        rectangle22Path.move(to: CGPoint(x: -7.45, y: 14))
        rectangle22Path.addLine(to: CGPoint(x: 7.45, y: -0))
        rectangle22Path.addLine(to: CGPoint(x: -7.45, y: -14))
        rectangle22Path.addLine(to: CGPoint(x: -7.45, y: 14))
        rectangle22Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle22Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle22Path.lineWidth = 1
        rectangle22Path.stroke()
        
        
        
        context?.restoreGState()
        
        
        //// Text 22 Drawing
        let text22Rect = CGRect(x: 275, y: 354, width: 151, height: 16)
        let text22Path = UIBezierPath(rect: text22Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text22Path.fill()
        context?.restoreGState()
        
        let text22Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text22Style.alignment = NSTextAlignment.center
        
        let text22FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text22Style]
        
        "POI Location Request".draw(in: text22Rect, withAttributes: text22FontAttributes)
        
        
        //// Bezier 12 Drawing
        context?.saveGState()
        context?.translateBy(x: 662.21, y: 392)
        context?.rotate(by: -179.95 * CGFloat(Float.pi) / 180)
        
        let bezier12Path = UIBezierPath()
        bezier12Path.move(to: CGPoint(x: -44.3, y: -61.97))
        bezier12Path.addCurve(to: CGPoint(x: 44.29, y: -0), controlPoint1: CGPoint(x: -43.16, y: -58.94), controlPoint2: CGPoint(x: 43.15, y: -27.2))
        bezier12Path.addCurve(to: CGPoint(x: -44.3, y: 61.97), controlPoint1: CGPoint(x: 45.42, y: 27.2), controlPoint2: CGPoint(x: -44.3, y: 61.97))
        bezier12Path.addLine(to: CGPoint(x: -44.3, y: -61.97))
        bezier12Path.addLine(to: CGPoint(x: -44.3, y: -61.97))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        bezier12Path.addClip()
        context?.drawLinearGradient(greenGradient!, start: CGPoint(x: 44.3, y: -0), end: CGPoint(x: -44.3, y: -0), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        bezier12Path.lineWidth = 1.5
        bezier12Path.stroke()
        
        context?.restoreGState()
        
        
        //// Text 12 Drawing
        context?.saveGState()
        context?.translateBy(x: 690.74, y: 372.37)
        context?.rotate(by: 90 * CGFloat(Float.pi) / 180)
        
        let text12Rect = CGRect(x: -30.11, y: -15.5, width: 94.41, height: 62.37)
        let text12TextContent = NSString(string: "App Log\nView\nAdapter")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text12Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text12Style.alignment = NSTextAlignment.center
        
        let text12FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 13)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text12Style]
        
        let text12TextHeight: CGFloat = text12TextContent.boundingRect(with: CGSize(width: text12Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text12FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text12Rect);
        text12TextContent.draw(in: CGRect(x: text12Rect.minX, y: text12Rect.minY + (text12Rect.height - text12TextHeight) / 2, width: text12Rect.width, height: text12TextHeight), withAttributes: text12FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Bezier 20 Drawing
        context?.saveGState()
        context?.translateBy(x: 664.11, y: 534.5)
        context?.rotate(by: -179.95 * CGFloat(Float.pi) / 180)
        
        let bezier20Path = UIBezierPath()
        bezier20Path.move(to: CGPoint(x: -44.29, y: -69.49))
        bezier20Path.addCurve(to: CGPoint(x: 44.28, y: -2.52), controlPoint1: CGPoint(x: -43.16, y: -66.23), controlPoint2: CGPoint(x: 43.15, y: -31.92))
        bezier20Path.addCurve(to: CGPoint(x: -44.29, y: 64.46), controlPoint1: CGPoint(x: 45.42, y: 26.89), controlPoint2: CGPoint(x: -44.29, y: 64.46))
        bezier20Path.addLine(to: CGPoint(x: -44.29, y: -69.49))
        bezier20Path.addLine(to: CGPoint(x: -44.29, y: -69.49))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        bezier20Path.addClip()
        context?.drawLinearGradient(greenGradient!, start: CGPoint(x: 44.29, y: -2.52), end: CGPoint(x: -44.29, y: -2.52), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        bezier20Path.lineWidth = 1.5
        bezier20Path.stroke()
        
        context?.restoreGState()
        
        
        //// Text 17 Drawing
        context?.saveGState()
        context?.translateBy(x: 694.74, y: 521.2)
        context?.rotate(by: 90 * CGFloat(Float.pi) / 180)
        
        let text17Rect = CGRect(x: -31.33, y: -15.5, width: 98.21, height: 62.37)
        let text17TextContent = NSString(string: "Map MKAnnotation\nAdapter")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text17Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text17Style.alignment = NSTextAlignment.center
        
        let text17FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 13)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text17Style]
        
        let text17TextHeight: CGFloat = text17TextContent.boundingRect(with: CGSize(width: text17Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text17FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text17Rect);
        text17TextContent.draw(in: CGRect(x: text17Rect.minX, y: text17Rect.minY + (text17Rect.height - text17TextHeight) / 2, width: text17Rect.width, height: text17TextHeight), withAttributes: text17FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 2 Drawing
        context?.saveGState()
        context?.translateBy(x: 604.59, y: 543.45)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle2Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 9, height: 16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle2Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle2Path.lineWidth = 1
        rectangle2Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 3 Drawing
        let rectangle3Path = UIBezierPath()
        rectangle3Path.move(to: CGPoint(x: 604.5, y: 548.5))
        rectangle3Path.addLine(to: CGPoint(x: 626.5, y: 534.5))
        rectangle3Path.addLine(to: CGPoint(x: 604.5, y: 520.5))
        rectangle3Path.addLine(to: CGPoint(x: 604.5, y: 548.5))
        rectangle3Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle3Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle3Path.lineWidth = 1
        rectangle3Path.stroke()
        
        
        //// Rectangle 9 Drawing
        context?.saveGState()
        context?.translateBy(x: 601.59, y: 398.45)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle9Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 9, height: 16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle9Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle9Path.lineWidth = 1
        rectangle9Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 14 Drawing
        let rectangle14Path = UIBezierPath()
        rectangle14Path.move(to: CGPoint(x: 601.5, y: 403.5))
        rectangle14Path.addLine(to: CGPoint(x: 623.5, y: 389.5))
        rectangle14Path.addLine(to: CGPoint(x: 601.5, y: 375.5))
        rectangle14Path.addLine(to: CGPoint(x: 601.5, y: 403.5))
        rectangle14Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle14Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle14Path.lineWidth = 1
        rectangle14Path.stroke()
        
        
        //// Rectangle 4 Drawing
        let rectangle4Path = UIBezierPath(roundedRect: CGRect(x: 18, y: 152, width: 25, height: 150), cornerRadius: 6)
        color7.setFill()
        rectangle4Path.fill()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        rectangle4Path.lineWidth = 2
        rectangle4Path.stroke()
        context?.restoreGState()
        
        
        //// Text 23 Drawing
        context?.saveGState()
        context?.translateBy(x: 33.01, y: 225.78)
        context?.rotate(by: -90 * CGFloat(Float.pi) / 180)
        
        let text23Rect = CGRect(x: -42.11, y: -10.78, width: 84.22, height: 21.57)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text23Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text23Style.alignment = NSTextAlignment.center
        
        let text23FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text23Style]
        
        "Keyboard".draw(in: text23Rect, withAttributes: text23FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 19 Drawing
        let rectangle19Path = UIBezierPath(roundedRect: CGRect(x: 18, y: 313, width: 25, height: 133), cornerRadius: 6)
        color7.setFill()
        rectangle19Path.fill()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        rectangle19Path.lineWidth = 2
        rectangle19Path.stroke()
        context?.restoreGState()
        
        
        //// Text 24 Drawing
        context?.saveGState()
        context?.translateBy(x: 32.97, y: 377.87)
        context?.rotate(by: -90 * CGFloat(Float.pi) / 180)
        
        let text24Rect = CGRect(x: -38.87, y: -8.85, width: 77.74, height: 17.69)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text24Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text24Style.alignment = NSTextAlignment.center
        
        let text24FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text24Style]
        
        "GPS".draw(in: text24Rect, withAttributes: text24FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 34 Drawing
        let rectangle34Path = UIBezierPath(roundedRect: CGRect(x: 706.5, y: 324, width: 25, height: 136), cornerRadius: 6)
        color7.setFill()
        rectangle34Path.fill()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        rectangle34Path.lineWidth = 2
        rectangle34Path.stroke()
        context?.restoreGState()
        
        
        //// Text 26 Drawing
        context?.saveGState()
        context?.translateBy(x: 719.84, y: 388.67)
        context?.rotate(by: 90 * CGFloat(Float.pi) / 180)
        
        let text26Rect = CGRect(x: -41.67, y: -10.37, width: 83.33, height: 20.74)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text26Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text26Style.alignment = NSTextAlignment.center
        
        let text26FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text26Style]
        
        "App Log\n".draw(in: text26Rect, withAttributes: text26FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 35 Drawing
        let rectangle35Path = UIBezierPath(roundedRect: CGRect(x: 707, y: 466, width: 25, height: 142), cornerRadius: 6)
        color7.setFill()
        rectangle35Path.fill()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        rectangle35Path.lineWidth = 2
        rectangle35Path.stroke()
        context?.restoreGState()
        
        
        //// Text 27 Drawing
        context?.saveGState()
        context?.translateBy(x: 719.79, y: 531.29)
        context?.rotate(by: 90 * CGFloat(Float.pi) / 180)
        
        let text27Rect = CGRect(x: -41.71, y: -10.37, width: 83.43, height: 20.74)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text27Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text27Style.alignment = NSTextAlignment.center
        
        let text27FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text27Style]
        
        "Map\n".draw(in: text27Rect, withAttributes: text27FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 20 Drawing
        let rectangle20Path = UIBezierPath(roundedRect: CGRect(x: 706, y: 162, width: 25, height: 148), cornerRadius: 6)
        color7.setFill()
        rectangle20Path.fill()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        rectangle20Path.lineWidth = 2
        rectangle20Path.stroke()
        context?.restoreGState()
        
        
        //// Text 25 Drawing
        context?.saveGState()
        context?.translateBy(x: 717.36, y: 236.5)
        context?.rotate(by: 90 * CGFloat(Float.pi) / 180)
        
        let text25Rect = CGRect(x: -74.5, y: -10.78, width: 149, height: 21.57)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text25Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text25Style.alignment = NSTextAlignment.center
        
        let text25FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text25Style]
        
        "POI  Results".draw(in: text25Rect, withAttributes: text25FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Text 2 Drawing
        let text2Rect = CGRect(x: 486, y: 184, width: 125, height: 16)
        let text2Path = UIBezierPath(rect: text2Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text2Path.fill()
        context?.restoreGState()
        
        let text2Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text2Style.alignment = NSTextAlignment.center
        
        let text2FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text2Style]
        
        "POI Keyword Matches".draw(in: text2Rect, withAttributes: text2FontAttributes)
        
        
        //// Text 28 Drawing
        let text28Rect = CGRect(x: 454, y: 270, width: 125, height: 16)
        let text28Path = UIBezierPath(rect: text28Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text28Path.fill()
        context?.restoreGState()
        
        let text28Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text28Style.alignment = NSTextAlignment.center
        
        let text28FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text28Style]
        
        "POI Keyword Matches".draw(in: text28Rect, withAttributes: text28FontAttributes)
        
        
        //// Text 20 Drawing
        let text20Rect = CGRect(x: 467, y: 482, width: 133, height: 16)
        let text20Path = UIBezierPath(rect: text20Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text20Path.fill()
        context?.restoreGState()
        
        let text20Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text20Style.alignment = NSTextAlignment.center
        
        let text20FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text20Style]
        
        "POI Location Result".draw(in: text20Rect, withAttributes: text20FontAttributes)
        
        
        //// Text 29 Drawing
        let text29Rect = CGRect(x: 352, y: 450, width: 121, height: 16)
        let text29Path = UIBezierPath(rect: text29Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text29Path.fill()
        context?.restoreGState()
        
        let text29Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text29Style.alignment = NSTextAlignment.center
        
        let text29FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text29Style]
        
        "POI Location Result".draw(in: text29Rect, withAttributes: text29FontAttributes)
        
        
        //// fromReachability_To_POIRequestor Drawing
        let fromReachability_To_POIRequestorPath = UIBezierPath()
        fromReachability_To_POIRequestorPath.move(to: CGPoint(x: 271, y: 321.87))
        fromReachability_To_POIRequestorPath.addCurve(to: CGPoint(x: 263.64, y: 321.87), controlPoint1: CGPoint(x: 271, y: 321.87), controlPoint2: CGPoint(x: 269.09, y: 321.87))
        fromReachability_To_POIRequestorPath.addCurve(to: CGPoint(x: 221, y: 322), controlPoint1: CGPoint(x: 252.65, y: 321.87), controlPoint2: CGPoint(x: 221, y: 322))
        fromReachability_To_POIRequestorPath.addCurve(to: CGPoint(x: 193, y: 402.98), controlPoint1: CGPoint(x: 221, y: 322), controlPoint2: CGPoint(x: 203.13, y: 306.7))
        fromReachability_To_POIRequestorPath.addCurve(to: CGPoint(x: 169, y: 513.34), controlPoint1: CGPoint(x: 190.47, y: 427.05), controlPoint2: CGPoint(x: 186, y: 498.54))
        fromReachability_To_POIRequestorPath.addCurve(to: CGPoint(x: 125.56, y: 529.82), controlPoint1: CGPoint(x: 158.97, y: 522.07), controlPoint2: CGPoint(x: 125.56, y: 529.82))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        fromReachability_To_POIRequestorPath.lineWidth = 8
        fromReachability_To_POIRequestorPath.stroke()
        context?.restoreGState()
        
        
        //// Rectangle 36 Drawing
        let rectangle36Path = UIBezierPath()
        rectangle36Path.move(to: CGPoint(x: 115.49, y: 543.5))
        rectangle36Path.addLine(to: CGPoint(x: 136.37, y: 529.5))
        rectangle36Path.addLine(to: CGPoint(x: 115.49, y: 515.5))
        rectangle36Path.addLine(to: CGPoint(x: 115.49, y: 543.5))
        rectangle36Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle36Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle36Path.lineWidth = 1
        rectangle36Path.stroke()
        
        
        //// Bezier 2 Drawing
        let bezier2Path = UIBezierPath()
        bezier2Path.move(to: CGPoint(x: 39, y: 471))
        bezier2Path.addCurve(to: CGPoint(x: 123.08, y: 528), controlPoint1: CGPoint(x: 40.08, y: 473.78), controlPoint2: CGPoint(x: 122, y: 502.98))
        bezier2Path.addCurve(to: CGPoint(x: 39, y: 585), controlPoint1: CGPoint(x: 124.16, y: 553.02), controlPoint2: CGPoint(x: 39, y: 585))
        bezier2Path.addLine(to: CGPoint(x: 39, y: 471))
        bezier2Path.addLine(to: CGPoint(x: 39, y: 471))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        context?.beginTransparencyLayer(auxiliaryInfo: nil)
        bezier2Path.addClip()
        context?.drawLinearGradient(blueGradient!, start: CGPoint(x: 123.09, y: 528), end: CGPoint(x: 39, y: 528), options: [])
        context?.endTransparencyLayer()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        bezier2Path.lineWidth = 1.5
        bezier2Path.stroke()
        
        
        //// Text 30 Drawing
        context?.saveGState()
        context?.translateBy(x: 74.41, y: 529.42)
        context?.rotate(by: -90 * CGFloat(Float.pi) / 180)
        
        let text30Rect = CGRect(x: -43.42, y: -24.59, width: 86.84, height: 49.18)
        let text30TextContent = NSString(string: "Reachability\nDevice \nAdaptor")
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text30Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text30Style.alignment = NSTextAlignment.center
        
        let text30FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 13)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text30Style]
        
        let text30TextHeight: CGFloat = text30TextContent.boundingRect(with: CGSize(width: text30Rect.width, height: CGFloat.infinity), options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: text30FontAttributes, context: nil).size.height
        context?.saveGState()
        context?.clip(to: text30Rect);
        text30TextContent.draw(in: CGRect(x: text30Rect.minX, y: text30Rect.minY + (text30Rect.height - text30TextHeight) / 2, width: text30Rect.width, height: text30TextHeight), withAttributes: text30FontAttributes)
        context?.restoreGState()
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Rectangle 37 Drawing
        let rectangle37Path = UIBezierPath(roundedRect: CGRect(x: 17, y: 463, width: 25, height: 133), cornerRadius: 6)
        color7.setFill()
        rectangle37Path.fill()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color3.setStroke()
        rectangle37Path.lineWidth = 2
        rectangle37Path.stroke()
        context?.restoreGState()
        
        
        //// Text 31 Drawing
        context?.saveGState()
        context?.translateBy(x: 29.97, y: 526.87)
        context?.rotate(by: -90 * CGFloat(Float.pi) / 180)
        
        let text31Rect = CGRect(x: -47.87, y: -8.85, width: 95.74, height: 17.69)
        context?.saveGState()
        context?.setShadow(offset: whiteShadow.shadowOffset, blur: whiteShadow.shadowBlurRadius, color: (whiteShadow.shadowColor as! UIColor).cgColor)
        let text31Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text31Style.alignment = NSTextAlignment.center
        
        let text31FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 14)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text31Style]
        
        "Reachability\ny\n".draw(in: text31Rect, withAttributes: text31FontAttributes)
        context?.restoreGState()
        
        
        context?.restoreGState()
        
        
        //// Text 8 Drawing
        let text8Rect = CGRect(x: 154, y: 282, width: 66, height: 16)
        let text8Path = UIBezierPath(rect: text8Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text8Path.fill()
        context?.restoreGState()
        
        let text8Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text8Style.alignment = NSTextAlignment.center
        
        let text8FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text8Style]
        
        "Location".draw(in: text8Rect, withAttributes: text8FontAttributes)
        
        
        //// Text 9 Drawing
        let text9Rect = CGRect(x: 141, y: 185, width: 94, height: 16)
        let text9Path = UIBezierPath(rect: text9Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text9Path.fill()
        context?.restoreGState()
        
        let text9Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text9Style.alignment = NSTextAlignment.center
        
        let text9FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text9Style]
        
        "Keyboard Text".draw(in: text9Rect, withAttributes: text9FontAttributes)
        
        
        //// Text 32 Drawing
        let text32Rect = CGRect(x: 152, y: 393, width: 80, height: 16)
        let text32Path = UIBezierPath(rect: text32Rect)
        context?.saveGState()
        context?.setShadow(offset: softBlackShadow.shadowOffset, blur: softBlackShadow.shadowBlurRadius, color: (softBlackShadow.shadowColor as! UIColor).cgColor)
        UIColor.white.setFill()
        text32Path.fill()
        context?.restoreGState()
        
        let text32Style = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        text32Style.alignment = NSTextAlignment.center
        
        let text32FontAttributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 12)!, NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.paragraphStyle: text32Style]
        
        "Reachability".draw(in: text32Rect, withAttributes: text32FontAttributes)
        
        
        //// Group 3
        //// Rectangle 17 Drawing
        context?.saveGState()
        context?.translateBy(x: 273.95, y: 301.46)
        context?.rotate(by: 179.68 * CGFloat(Float.pi) / 180)
        
        let rectangle17Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.39, height: 16.01))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle17Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle17Path.lineWidth = 1
        rectangle17Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 18 Drawing
        let rectangle18Path = UIBezierPath()
        rectangle18Path.move(to: CGPoint(x: 273.6, y: 306.5))
        rectangle18Path.addLine(to: CGPoint(x: 288.5, y: 292.5))
        rectangle18Path.addLine(to: CGPoint(x: 273.6, y: 278.5))
        rectangle18Path.addLine(to: CGPoint(x: 273.6, y: 306.5))
        rectangle18Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle18Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle18Path.lineWidth = 1
        rectangle18Path.stroke()
        
        
        
        
        //// Group 4
        //// Rectangle 39 Drawing
        let rectangle39Path = UIBezierPath()
        rectangle39Path.move(to: CGPoint(x: 269.6, y: 335.5))
        rectangle39Path.addLine(to: CGPoint(x: 284.5, y: 321.5))
        rectangle39Path.addLine(to: CGPoint(x: 269.6, y: 307.5))
        rectangle39Path.addLine(to: CGPoint(x: 269.6, y: 335.5))
        rectangle39Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle39Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle39Path.lineWidth = 1
        rectangle39Path.stroke()
        
        
        
        
        //// Rectangle 40 Drawing
        let rectangle40Path = UIBezierPath(rect: CGRect(x: 0, y: 661, width: 748, height: 12))
        UIColor.gray.setFill()
        rectangle40Path.fill()
        
        
        //// Group 5
        //// Rectangle 7 Drawing
        context?.saveGState()
        context?.translateBy(x: 379.73, y: 536.87)
        context?.rotate(by: 90.06 * CGFloat(Float.pi) / 180)
        
        let rectangle7Path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 6.1, height: 15.16))
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle7Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle7Path.lineWidth = 1
        rectangle7Path.stroke()
        
        context?.restoreGState()
        
        
        //// Rectangle 41 Drawing
        context?.saveGState()
        context?.translateBy(x: 359, y: 536.91)
        context?.rotate(by: -89.63 * CGFloat(Float.pi) / 180)
        
        let rectangle41Path = UIBezierPath()
        rectangle41Path.move(to: CGPoint(x: 0, y: 27))
        rectangle41Path.addLine(to: CGPoint(x: 14.9, y: 13.5))
        rectangle41Path.addLine(to: CGPoint(x: 0, y: 0))
        rectangle41Path.addLine(to: CGPoint(x: 0, y: 27))
        rectangle41Path.close()
        context?.saveGState()
        context?.setShadow(offset: hardBlackShadow.shadowOffset, blur: hardBlackShadow.shadowBlurRadius, color: (hardBlackShadow.shadowColor as! UIColor).cgColor)
        color4.setFill()
        rectangle41Path.fill()
        context?.restoreGState()
        
        UIColor.black.setStroke()
        rectangle41Path.lineWidth = 1
        rectangle41Path.stroke()
        
        context?.restoreGState()

        if (m_state != nil) && !m_state!.paths.pathsAreSet
        {
            m_state!.paths.fromKeyboard_To_POIKeywordMatcher = fromKeyboard_To_POIKeywordMatcherPath
            m_state!.paths.fromLocation_To_POIRequestor = fromLocation_To_POIRequestorPath
            m_state!.paths.fromReachability_To_POIRequestor = fromReachability_To_POIRequestorPath

            m_state!.paths.fromPOIKeywordMatcher_To_POIKeywordsResults = fromPOIKeywordMatcher_To_POIKeywordsResultsPath
            m_state!.paths.fromPOIKeywordMatcher_To_LogAdapter = fromPOIKeywordMatcher_To_LogAdapterPath

            m_state!.paths.fromPOILocator_To_LogAdaptor = fromPOILocator_To_LogAdaptorPath
            m_state!.paths.fromPOILocator_To_MapAdaptor = fromPOILocator_To_MapAdaptorPath

            m_state!.paths.fromPOIKeywordMatcher_To_POIRequestor = fromPOIKeywordMatcher_To_POIRequestorPath
            m_state!.paths.fromPOIKeywordMatcher_To_POILocator = fromPOIKeywordMatcher_To_POILocatorPath

            m_state!.paths.fromPOILocator_To_POIPortalRxPeer = fromPOILocator_To_POIPortalRxPeerPath
            m_state!.paths.fromPOIPortalRxPeer_To_POILocator = fromPOIPortalRxPeer_To_POILocatorPath

            m_state!.paths.fromPOIPortalRxPeer_To_URLSessionRxPeer = fromPOIPortalRxPeer_To_URLSessionRxPeerPath
            m_state!.paths.fromURLSessionRxPeer_To_POIPortalRxPeer = fromURLSessionRxPeer_To_POIPortalRxPeerPath

            m_state!.paths.pathsAreSet = true
        }
    }
}
