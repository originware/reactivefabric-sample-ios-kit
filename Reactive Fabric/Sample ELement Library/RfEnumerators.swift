
//
// Reactive Fabric Sample Code (Reactive Fabric Operators)
//
// Created by Terry Stillone on 25/6/18.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The RfEnumerators.swift public enumerator elements.
///
///     Enumerators are composed at the end of an expression and return an Iterable Sequence.
///

extension Rf.Enumerators // Enumeration Operators
{
    public enum eType<Item>
    {
        /// An enumerable that blocks until the evaluation emits another item, then returns that item.
        case eNext
        
        // An enumerable that blocks until the first element is received and then emits the most recent item, even if it was emitted before.
        case eLatest
        
        // An enumerable that emits the most recently received item. If the first item has not been received yet, then a given default item is given.
        case eMostRecent(Item)
    }
    
    public static func enumerable<Item>(_ traceID: Rf.TraceID, enumerationType: Rf.Enumerators.eType<Item>) -> Rf.Enumerable<Item>
    {
        typealias Element = Item
        typealias eEvalNotify = Rf.EvalScope.eNotify
        
        let enumeration = Rf.Enumerable<Item>(Rf.TraceID("enumerable"))
        
        switch enumerationType
        {
        case .eNext:
            
            let barrier = RfBarriers.Signalable()
            
            enumeration.consumer.onItem = { [weak enumeration] (item : Item, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                strongEnumeration.store.queue(item: item)
                
                if strongEnumeration.store.itemCount == 1
                {
                    barrier.signal()
                }
            }
            
            enumeration.consumer.onControl = { [weak enumeration] (control : IRfControl, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                switch control
                {
                case eEvalNotify.eEvalBegin(let evalType):
                    
                    if let evalQueue = evalType.evalQueue
                    {
                        strongEnumeration.store.mode = .eSync(evalQueue)
                    }
                    
                case eEvalNotify.eEvalEnd(let error):
                    
                    strongEnumeration.store.queue(control: eEvalNotify.eEvalEnd(error))
                    
                    barrier.signal()
                    
                default:
                    
                    break
                }
                
                strongEnumeration.producer.notify(control: control, vTime: vTime)
            }
            
            enumeration.generator = AnyIterator
                {
                    while true
                    {
                        while true
                        {
                            let count = enumeration.store.itemCount
                            
                            switch enumeration.store.termination
                            {
                            case .eNone where count > 0:
                                
                                // Emit the most recent item.
                                return enumeration.store.pop()!
                                
                            case .eCompletion(nil) where count > 0:
                                
                                return enumeration.store.pop()!
                                
                            case .eCompletion:
                                
                                // Terminate the enumeration.
                                return nil
                                
                            default:
                                
                                // Block until item or control notification.
                                barrier.wait()
                            }
                        }
                    }
            }
            
        case .eLatest:
            
            let barrier = RfBarriers.Signalable()
            var didSignal = false
            var lastEmittedItem : Item? = nil
            
            enumeration.consumer.onItem = { [weak enumeration] (item : Item, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                strongEnumeration.store.queue(item: item)
                
                if !didSignal
                {
                    didSignal = barrier.signal() != 0
                }
            }
            
            enumeration.consumer.onControl = { [weak enumeration] (control : IRfControl, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                switch control
                {
                case eEvalNotify.eEvalBegin(let evalType):
                    
                    if let evalQueue = evalType.evalQueue
                    {
                        strongEnumeration.store.mode = .eSync(evalQueue)
                    }
                    
                case eEvalNotify.eEvalEnd(let error):
                    
                    strongEnumeration.store.queue(control: eEvalNotify.eEvalEnd(error))
                    
                    didSignal = barrier.signal() != 0
                    
                default:
                    
                    break
                }
                
                strongEnumeration.producer.notify(control: control, vTime: vTime)
            }
            
            enumeration.generator = AnyIterator
                {
                    while true
                    {
                        let count = enumeration.store.itemCount
                        
                        switch enumeration.store.termination
                        {
                        case .eNone where count > 0:
                            
                            // Emit the most recent item.
                            lastEmittedItem = enumeration.store.popToLast()!
                            
                            return lastEmittedItem!
                            
                        case .eNone where lastEmittedItem != nil:
                            
                            return lastEmittedItem!
                            
                        case .eCompletion(nil) where count > 0:
                            
                            return enumeration.store.popToLast()!
                            
                        case .eCompletion:
                            
                            // Terminate the enumeration.
                            return nil
                            
                        default:
                            
                            // Block if the first item has not been received.
                            barrier.wait()
                        }
                    }
            }
            
        case .eMostRecent(let defaultItem):
            
            let barrier = RfBarriers.Signalable()
            
            enumeration.consumer.onItem = { [weak enumeration] (item : Item, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                strongEnumeration.store.queue(item: item)
            }
            
            enumeration.consumer.onControl = { [weak enumeration] (control : IRfControl, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                switch control
                {
                case eEvalNotify.eEvalBegin(let evalType):
                    
                    if let evalQueue = evalType.evalQueue
                    {
                        strongEnumeration.store.mode = .eSync(evalQueue)
                    }
                    
                case eEvalNotify.eEvalEnd(let error):
                    
                    strongEnumeration.store.queue(control: eEvalNotify.eEvalEnd(error))
                    
                default:
                    
                    break
                }
                
                strongEnumeration.producer.notify(control: control, vTime: vTime)
            }
            
            enumeration.generator = AnyIterator
                {
                    while true
                    {
                        if case .eSync = enumeration.store.mode
                        {
                            let count = enumeration.store.itemCount
                            
                            switch enumeration.store.termination
                            {
                            case .eNone where count > 0:
                                
                                return enumeration.store.popToLast()!
                                
                            case .eCompletion(nil) where count > 0:
                                
                                return enumeration.store.popToLast()!
                                
                            case .eCompletion:
                                
                                // Terminate the enumeration.
                                return nil
                                
                            default:
                                
                                return defaultItem
                            }
                        }
                        else
                        {
                            barrier.wait()
                        }
                    }
            }
        }
        
        return enumeration
    }
    
    public static func enumerableWithVTime<Item>(_ traceID: Rf.TraceID, enumerationType: Rf.Enumerators.eType<Item>) -> Rf.EnumerableWithVTime<Item>
    {
        typealias Element = Item
        typealias eEvalNotify = Rf.EvalScope.eNotify
        
        var enumeration = Rf.EnumerableWithVTime<Item>(Rf.TraceID("enumerable"))
        
        switch enumerationType
        {
        case .eNext:
            
            let barrier = RfBarriers.Signalable()
            let enumerationEvalQueue = Rf.Eval.Queue(traceID.appending("synchroniser"), queueType: .eSerial)
            
            var syncedEnumeration: Rf.EnumerableWithVTime<Item> {
                
                get
                {
                    var result : Rf.EnumerableWithVTime<Item>?
                    
                    enumerationEvalQueue.dispatch(sync: { result = enumeration })
                    
                    return result!
                }
            }
            
            enumeration.consumer.onItem = { (item : Item, vTime: Rf.VTime?) in
                
                let strongEnumeration = syncedEnumeration
                
                strongEnumeration.store.queue(item: (item, vTime))
                
                if strongEnumeration.store.itemCount == 1
                {
                    barrier.signal()
                }
            }
            
            enumeration.consumer.onControl = { (control : IRfControl, vTime: Rf.VTime?) in
                
                let strongEnumeration = syncedEnumeration
                
                switch control
                {
                case eEvalNotify.eEvalBegin(let evalType):
                    
                    if let evalQueue = evalType.evalQueue
                    {
                        strongEnumeration.store.mode = .eSync(evalQueue)
                    }
                    
                case eEvalNotify.eEvalEnd(let error):
                    
                    strongEnumeration.store.queue(control: eEvalNotify.eEvalEnd(error))
                    
                    barrier.signal()
                    
                default:
                    
                    break
                }
                
                strongEnumeration.producer.notify(control: control, vTime: vTime)
            }
            
            enumeration.generator = AnyIterator
                {
                    while true
                    {
                        while true
                        {
                            let strongEnumeration = syncedEnumeration
                            let count = strongEnumeration.store.itemCount
                            
                            switch strongEnumeration.store.termination
                            {
                            case .eNone where count > 0:
                                
                                // Emit the most recent item.
                                return strongEnumeration.store.pop()!
                                
                            case .eCompletion(nil) where count > 0:
                                
                                return strongEnumeration.store.pop()!
                                
                            case .eCompletion:
                                
                                // Terminate the enumeration.
                                return nil
                                
                            default:
                                
                                // Block until item or control notification.
                                barrier.wait()
                            }
                        }
                    }
            }
            
        case .eLatest:
            
            let barrier = RfBarriers.Signalable()
            var didSignal = false
            var lastEmittedItem : (Item, Rf.VTime?)? = nil
            let enumerationEvalQueue = Rf.Eval.Queue(traceID.appending("synchroniser"), queueType: .eSerial)
            
            var syncedEnumeration: Rf.EnumerableWithVTime<Item> {
                
                get
                {
                    var result : Rf.EnumerableWithVTime<Item>?
                    
                    enumerationEvalQueue.dispatch(sync: { result = enumeration })
                    
                    return result!
                }
            }
            
            enumeration.consumer.onItem = { [weak enumeration] (item : Item, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                syncedEnumeration.store.queue(item: (item, vTime))
                
                if !didSignal
                {
                    didSignal = barrier.signal() != 0
                }
            }
            
            enumeration.consumer.onControl = { (control : IRfControl, vTime: Rf.VTime?) in
                
                switch control
                {
                case eEvalNotify.eEvalBegin(let evalType):
                    
                    if let evalQueue = evalType.evalQueue
                    {
                        syncedEnumeration.store.mode = .eSync(evalQueue)
                    }
                    
                case eEvalNotify.eEvalEnd(let error):
                    
                    syncedEnumeration.store.queue(control: eEvalNotify.eEvalEnd(error))
                    
                    didSignal = barrier.signal() != 0
                    
                default:
                    
                    break
                }
                
                syncedEnumeration.producer.notify(control: control, vTime: vTime)
            }
            
            enumeration.generator = AnyIterator
                {
                    while true
                    {
                        let strongEnumeration = syncedEnumeration
                        let count = strongEnumeration.store.itemCount
                        
                        switch strongEnumeration.store.termination
                        {
                        case .eNone where count > 0:
                            
                            // Emit the most recent item.
                            lastEmittedItem = strongEnumeration.store.popToLast()!
                            
                            return lastEmittedItem!
                            
                        case .eNone where lastEmittedItem != nil:
                            
                            return lastEmittedItem!
                            
                        case .eCompletion(nil) where count > 0:
                            
                            return strongEnumeration.store.popToLast()!
                            
                        case .eCompletion:
                            
                            // Terminate the enumeration.
                            return nil
                            
                        default:
                            
                            // Block if the first item has not been received.
                            barrier.wait()
                        }
                    }
            }
            
        case .eMostRecent(let defaultItem):
            
            let barrier = RfBarriers.Signalable()
            let enumerationEvalQueue = Rf.Eval.Queue(traceID.appending("synchroniser"), queueType: .eSerial)
            
            var syncedEnumeration: Rf.EnumerableWithVTime<Item> {
                
                get
                {
                    var result : Rf.EnumerableWithVTime<Item>?
                    
                    enumerationEvalQueue.dispatch(sync: { result = enumeration })
                    
                    return result!
                }
            }
            
            enumeration.consumer.onItem = { [weak enumeration] (item : Item, vTime: Rf.VTime?) in
                
                guard let strongEnumeration = enumeration else { return }
                
                strongEnumeration.store.queue(item: (item, vTime))
            }
            
            enumeration.consumer.onControl = { (control : IRfControl, vTime: Rf.VTime?) in
                
                switch control
                {
                case eEvalNotify.eEvalBegin(let evalType):
                    
                    if let evalQueue = evalType.evalQueue
                    {
                        syncedEnumeration.store.mode = .eSync(evalQueue)
                    }
                    
                    barrier.signal()
                    
                case eEvalNotify.eEvalEnd(let error):
                    
                    syncedEnumeration.store.queue(control: eEvalNotify.eEvalEnd(error))
                    
                default:
                    
                    break
                }
                
                syncedEnumeration.producer.notify(control: control, vTime: vTime)
            }
            
            enumeration.generator = AnyIterator
                {
                    while true
                    {
                        let strongEnumeration = syncedEnumeration
                        
                        if case .eSync = strongEnumeration.store.mode
                        {
                            let count = strongEnumeration.store.itemCount
                            
                            switch strongEnumeration.store.termination
                            {
                            case .eNone where count > 0:
                                
                                return enumeration.store.popToLast()!
                                
                            case .eCompletion(nil) where count > 0:
                                
                                return strongEnumeration.store.popToLast()!
                                
                            case .eCompletion:
                                
                                // Terminate the enumeration.
                                return nil
                                
                            default:
                                
                                return (defaultItem, nil)
                            }
                        }
                        else
                        {
                            barrier.wait()
                        }
                    }
            }
        }
        
        return enumeration
    }
}

extension Rf.AElement
{
    /// Return an enumerator that supplies the most recent item.
    /// - Parameter defaultValue: A value to supply on an empty stream.
    public func next() -> Rf.Enumerable<OutItem>
    {
        let enumerator = Rf.Enumerators.enumerable(Rf.TraceID("next"), enumerationType: Rf.Enumerators.eType<OutItem>.eNext)
        
        compose(enumerator)
        
        enumerator.beginEval(.eAsync(nil))
        enumerator.wait(for: .eForEvalBegin)
        
        return enumerator
    }
    
    /// Return an enumerator that supplies the most recent item.
    /// - Parameter defaultValue: A value to supply on an empty stream.
    public func latest() -> Rf.Enumerable<OutItem>
    {
        let enumerator = Rf.Enumerators.enumerable(Rf.TraceID("latest"), enumerationType: Rf.Enumerators.eType<OutItem>.eLatest)
        
        compose(enumerator)
        
        enumerator.beginEval(.eAsync(nil))
        enumerator.wait(for: .eForEvalBegin)
        
        return enumerator
    }
    
    /// Return an enumerator that supplies the most recent item.
    /// - Parameter defaultValue: A value to supply on an empty stream.
    public func mostRecent(defaultValue: OutItem) -> Rf.Enumerable<OutItem>
    {
        let enumerator = Rf.Enumerators.enumerable(Rf.TraceID("mostRecent"), enumerationType: Rf.Enumerators.eType<OutItem>.eMostRecent(defaultValue))
        
        compose(enumerator)
        
        enumerator.beginEval(.eAsync(nil))
        enumerator.wait(for: .eForEvalBegin)
        
        return enumerator
    }
    
    /// Return an enumerator that supplies the most recent item.
    /// - Parameter defaultValue: A value to supply on an empty stream.
    public func nextWithVTime() -> Rf.EnumerableWithVTime<OutItem>
    {
        let enumerator = Rf.Enumerators.enumerableWithVTime(Rf.TraceID("next"), enumerationType: Rf.Enumerators.eType<OutItem>.eNext)
        
        compose(enumerator)
        
        enumerator.beginEval(.eAsync(nil))
        enumerator.wait(for: .eForEvalBegin)
        
        return enumerator
    }
    
    /// Return an enumerator that supplies the most recent item.
    /// - Parameter defaultValue: A value to supply on an empty stream.
    public func latestWithVTime() -> Rf.EnumerableWithVTime<OutItem>
    {
        let enumerator = Rf.Enumerators.enumerableWithVTime(Rf.TraceID("latest"), enumerationType: Rf.Enumerators.eType<OutItem>.eLatest)
        
        compose(enumerator)
        
        enumerator.beginEval(.eAsync(nil))
        enumerator.wait(for: .eForEvalBegin)
        
        return enumerator
    }
    
    /// Return an enumerator that supplies the most recent item.
    /// - Parameter defaultValue: A value to supply on an empty stream.
    public func mostRecentWithVTime(defaultValue: OutItem) -> Rf.EnumerableWithVTime<OutItem>
    {
        let enumerator = Rf.Enumerators.enumerableWithVTime(Rf.TraceID("mostRecent"), enumerationType: Rf.Enumerators.eType<OutItem>.eMostRecent(defaultValue))
        
        compose(enumerator)
        
        enumerator.beginEval(.eAsync(nil))
        enumerator.wait(for: .eForEvalBegin)
        
        return enumerator
    }
}
