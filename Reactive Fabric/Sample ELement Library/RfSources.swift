
//
// Reactive Fabric Sample Code (Reactive Fabric Sources)
//
// Created by Terry Stillone on 12/3/18.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Sample sources given here for user interest. These sources are not employed in the App.
///

extension Rf.Sources // Emit From Source
{
    ///
    /// Source notifySource:  Helper source that employs a given notifyFunc to generate the source notifications.
    ///
    
    public static func notifySource<Item>(_ traceID : Rf.TraceID, _ notifyFunc: @escaping (Rf.eSourceNotification<Item>, Rf.AElement<Void, Item>) -> Void) -> Rf.ASource<Item>
    {
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let notifySource: Rf.ASource<Item> = RfSDK.factory.Source(traceID)

        notifySource.consumer.onControl = { [weak notifySource] (control, vTime) in

            guard let strongOperator = notifySource else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType):

                    if let keys = strongOperator.tools?.keys
                    {
                        for name in keys
                        {
                            notifyFunc(.eFabricTool(name, strongOperator), strongOperator)
                        }
                    }

                    notifyFunc(.eEvalControl(.eEvalBegin(evalType), vTime), strongOperator)

                case eEvalNotify.eEvalRestart(let evalType):

                    notifyFunc(.eEvalControl(.eEvalRestart(evalType), vTime), strongOperator)

                case eEvalNotify.eEvalEnd(let error):

                    notifyFunc(.eEvalControl(.eEvalEnd(error), vTime), strongOperator)

                default:

                    strongOperator.producer.notify(control: control, vTime: vTime)
            }
        }

        return notifySource
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// emit:  Emit synchronous notifications as defined by the Rf.eEmit enumeration.
    ///
    ///         This includes:
    ///
    ///               -  case eEmpty:                               An empty sequence with a completed notifications.
    ///               -  case eEmptyWithNoComplete:                 No notifications.
    ///               -  case eItem(Item):                          A single item with completion.
    ///               -  case eError(IRfError):                     An error completion.
    ///               -  case eItemWithError(Item, IRfError):       An item with an error completion.
    ///               -  case eItemArray(ContiguousArray<Item>):    A sequence of items with completion.
    ///               -  case eItemArrayWithError(ContiguousArray<Item>, IRfError):  A sequence of items with an error completion.
    ///               -  case eStore(Rf.Notifications.Store<Item>):  A sequence of items given in a store.

    public static func emit<Item>(_ traceID: Rf.TraceID = Rf.TraceID("emit"), type: Rf.eEmit<Item>) -> Rf.ASource<Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalType = Rf.EvalScope.eEvalType

        let itemGenerator             = type.generator
        let source : Rf.ASource<Item> = RfSDK.factory.Source(traceID)

        var activeEvalType: eEvalType?          = nil
        var requestedToStop                     = false
        var index                               = 0

        source.consumer.onControl = { [weak source] (control, vTime) in

            guard let strongSource = source else { return }

            func startEval(_ evalTypeParam : eEvalType)
            {
                activeEvalType = evalTypeParam

                while !requestedToStop && itemGenerator(index, vTime, strongSource)
                {
                    index += 1
                }
            }

            func stopEval()
            {
                activeEvalType = nil
            }

            // Pre-propagation control handling.
            if case eEvalNotify.eEvalEnd = control, !requestedToStop
            {
                requestedToStop = true
                stopEval()
            }

            // Post-propagation control handling.
            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType) where !requestedToStop:

                    strongSource.producer.notify(control: control, vTime: vTime)

                    startEval(evalType)

                case eEvalNotify.eEvalBegin where requestedToStop:

                    RfSDK.assertionFailure("\(self): requested to begin while in stopped state.")

                case eEvalNotify.eEvalRestart(let evalType):

                    strongSource.producer.notify(control: control, vTime: vTime)

                    requestedToStop = false
                    stopEval()
                    startEval(evalType)

                case eEvalNotify.eEvalEnd:

                    strongSource.producer.notify(control: control, vTime: vTime)

                case eFabricNotify.eInstallTool:

                    strongSource.producer.notify(control: control, vTime: vTime)

                default:

                    strongSource.producer.notify(control: control, vTime: vTime)
            }
        }

        return source
    }

    public static func emit<Item>(_ traceID: Rf.TraceID = Rf.TraceID("emit(async)"), asyncType: Rf.eEmitAsync<Item>) -> Rf.ASource<Item>
    {
        typealias eEvalType = Rf.EvalScope.eEvalType
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let source : Rf.ASource<Item> = RfSDK.factory.Source(traceID)

        var activeFabric : Rf.AFabric? = nil
        var requestedToStop                 = false
        var isGenerating                    = false

        source.consumer.onControl = { [weak source] (control, vTime) in

            guard let strongSource = source else { return }

            func beginFabric()
            {
                switch asyncType
                {
                    case .eStore:

                        break

                    case .eFromNotifier(let notifier):

                        notifier.onItem = { (item, vTime) in strongSource.producer.notify(item: item, vTime: vTime) }
                        notifier.onControl = { (control, vTime) in strongSource.producer.notify(control: control, vTime: vTime) }
                }
            }

            func beginEval(_ evalType : eEvalType, vTime: Rf.VTime?)
            {
                guard !isGenerating else { return }

                isGenerating = true

                switch asyncType
                {
                    case .eStore:
                        
                        let playedContentNotifier = Rf.Notifiers.DelegateTarget<Item>(traceID.appending("playedContentNotifier"))

                        playedContentNotifier.onItem = { (item, vTime) in

                            guard let strongSource2 = source else { return }

                            strongSource2.producer.notify(item: item, vTime: vTime)
                        }

                        playedContentNotifier.onControl = { (control, vTime) in

                            if case eEvalNotify.eEvalEnd(let error) = control
                            {
                                guard let strongSource2 = source else { return }

                                strongSource2.endEval(error, vTime: vTime)
                            }
                        }

                    default:

                        break
                }
            }

            func restartEval(_ evalType : eEvalType, vTime: Rf.VTime?)
            {
                switch asyncType
                {
                    case .eStore:

                        activeFabric?.restartEval(evalType, vTime: vTime)

                    case .eFromNotifier(let notifier):

                        notifier.decomposeAll()
                }

                isGenerating = false
            }

            func endEval(vTime: Rf.VTime?)
            {
                guard isGenerating else { return }

                requestedToStop = true

                switch asyncType
                {
                    case .eStore:

                        activeFabric?.endEval(vTime: vTime)

                    case .eFromNotifier(let notifier):

                        notifier.decomposeAll()
                }

                isGenerating = false
            }

            func endFabric()
            {
                switch asyncType
                {
                    case .eStore:

                        activeFabric = nil

                    case .eFromNotifier(let notifier):

                        notifier.onItem = { (_, _) in }
                        notifier.onControl = { (_, _) in }
                }
            }

            // Pre-propagation control handling.
            switch control
            {
                case eFabricNotify.eInstallTool(let tool):

                    strongSource.set(tool: tool)

                case eFabricNotify.eFabricEnd:

                    endFabric()

                case eEvalNotify.eEvalEnd where !requestedToStop:

                    requestedToStop = true
                    endEval(vTime: vTime)

                default:

                    break
            }

            strongSource.producer.notify(control: control, vTime: vTime)

            // Post-propagation control handling.
            switch control
            {
                case eFabricNotify.eFabricBegin:

                    beginFabric()

                case eEvalNotify.eEvalBegin(let evalType) where !requestedToStop:

                    beginEval(evalType, vTime: vTime)

                case eEvalNotify.eEvalRestart(let evalType):

                    requestedToStop = false
                    restartEval(evalType, vTime: vTime)

                default:

                    break
            }
        }

        return source
    }

    ///
    /// emit:  Throw an error on evaluation begin.
    ///
    
    public static func emit<Item>(errorToThrow : Error) -> Rf.ASource<Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID                     = Rf.TraceID("emit")
        let source : Rf.ASource<Item> = RfSDK.factory.Source(traceID)

        source.consumer.onControl = { [weak source] (control : IRfControl, vTime: Rf.VTime?) in

            guard let strongSource = source else { return }

            do {

                strongSource.producer.notify(control: control, vTime: vTime)

                if case eEvalNotify.eEvalBegin = control
                {
                    throw errorToThrow
                }
            }
            catch let error as IRfError
            {
                strongSource.endEval(error, vTime: vTime)
            }
            catch let error
            {
                strongSource.endEval(Rf.eError(String(describing: error)), vTime: vTime)
            }
        }

        return source
    }

    public static func fromArray<Item>(_ traceID: Rf.TraceID = Rf.TraceID("array"), _ array: ContiguousArray<Item>) -> Rf.ASource<Item>
    {
        return emit(traceID, type: .eItemArray(array))
    }
}

extension Rf.Sources // Generator Sources
{
    ///
    /// generate:  Generate a non-timed sequence as given by a generator func.
    ///
    
    public static func generate<Item>(_ generatorFunc: @escaping (Int) -> Item?) -> Rf.ASource<Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID                     = Rf.TraceID("generate")
        let source : Rf.ASource<Item> = RfSDK.factory.Source(traceID)

        var index    = 0
        var stopEval = false

        source.consumer.onControl = { [weak source] (control, vTime) in

            guard let strongSource = source else { return }

            strongSource.producer.notify(control: control, vTime: vTime)

            switch control
            {
                case eEvalNotify.eEvalBegin:

                    index = 0

                    while let item = generatorFunc(index), !stopEval
                    {
                        strongSource.producer.notify(item: item, vTime: vTime)
                        index += 1
                    }

                    if !stopEval
                    {
                        strongSource.endEval(vTime: vTime)
                    }

                case eEvalNotify.eEvalRestart:

                    index = 0

                case eEvalNotify.eEvalEnd:

                    stopEval = true

                default:
                    break
            }
        }

        return source
    }

    ///
    /// generate:  Generate a timed sequence as given by a generator func.
    ///

    public static func generate<Item>(_ traceID: Rf.TraceID, startTime: Date, settings : Rf.SchedulerSettings, _ generatorFunc: @escaping (Int) -> (Rf.VTime, Item)?) -> Rf.ASource<Item>
    {
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalNotify = Rf.EvalScope.eNotify
        typealias eSchedulerRequest = Rf.Sockets.AScheduler.eRequest
        typealias eSchedulerReply = Rf.Sockets.AScheduler.eReply

        let source:      Rf.ASource<Item> = RfSDK.factory.Source(traceID)
        var schedulerId: Int?             = nil
        var hasTerminated                 = false
        var index                         = 0

        source.consumer.onControl = { [weak source] (control, vTime) in

            guard let strongSource = source else { return }

            let outputNotifier = strongSource.producer

            func startScheduler(_ scheduler: Rf.Sockets.AScheduler)
            {
                scheduler.consumer.notify(item: eSchedulerRequest.eBeginSession(settings, { [weak scheduler] (reply) in

                    guard let strongScheduler = scheduler else { return }

                    func scheduleTick(_ sessionId: Int)
                    {
                        if let (nextTick, item) = generatorFunc(index)
                        {
                            outputNotifier.notify(item: item, vTime: vTime)

                            index += 1

                            strongScheduler.consumer.notify(item: eSchedulerRequest.eScheduleTickActionByVTime(sessionId, nil, strongScheduler.vTimeSince(startTime.addingTimeInterval(nextTick)), { (time) in

                                guard !hasTerminated, let strongSource2 = source else { return }
                                guard let evalQueue = strongSource2.evalQueue else
                                {
                                    RfSDK.assertionFailure("\(strongSource2): No evalqueue assigned to source.")
                                    return
                                }

                                evalQueue.dispatch(async: { scheduleTick(sessionId) })

                            }), vTime: vTime)
                        }
                        else
                        {
                            strongScheduler.consumer.notify(item: eSchedulerRequest.eEndSession(sessionId), vTime: vTime)
                        }
                    }

                    switch reply
                    {
                        case .eDidBeginSession(let sessionId):

                            schedulerId = sessionId
                            index = 0

                            scheduleTick(sessionId)

                        case .eDidEndSession:

                            schedulerId = nil
                            index = 0
                    }
                }), vTime: nil)
            }

            outputNotifier.notify(control: control, vTime: vTime)

            switch control
            {
                case eEvalNotify.eEvalBegin:

                    hasTerminated = false

                    guard let scheduler = strongSource.scheduler else
                    {
                        RfSDK.assertionFailure("\(strongSource): Scheduler not available")
                        return
                    }

                    startScheduler(scheduler)

                case eEvalNotify.eEvalRestart:

                    if let scheduler = strongSource.scheduler, let schedulerId = schedulerId
                    {
                        scheduler.consumer.notify(item: eSchedulerRequest.eRestart(schedulerId), vTime: vTime)
                    }

                case eEvalNotify.eEvalEnd:

                    if let scheduler = strongSource.scheduler, let schedulerId = schedulerId
                    {
                        scheduler.consumer.notify(item: eSchedulerRequest.eEndSession(schedulerId), vTime: vTime)
                    }

                    hasTerminated = true

                default:

                    break
            }
        }

        return source
    }
}

extension Rf.Sources // Interval sources.
{
    ///
    /// interval: The standard Reactive Extensions interval timed-sequence source as a first-principles implementation.
    ///
    
    public static func interval(_ traceID : Rf.TraceID = Rf.TraceID("interval"), startTime: Date, period: TimeInterval, settings : Rf.SchedulerSettings = Rf.SchedulerSettings.SourceDef) -> Rf.ASource<Int>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let sourceIdentity = Rf.Identity(traceID, elementType: .eSource("Source<(Int)>"))
        var scheduleTool : Rf.Tools.Schedule? = nil
        var hasTerminated = false

        let intervalSource: Rf.ASource<Int> = Rf.Sources.notifySource(traceID, { (inputNotification, element) in

            switch inputNotification
            {
                case .eFabricTool(let (name, tools)):

                    if name == "scheduler"
                    {
                        scheduleTool = tools.getScheduleTool(traceID)
                    }

                case .eEvalControl(.eEvalBegin(let evalType), let (vTime)):  // On a begin evaluation Control notification.

                    guard let scheduleTool = scheduleTool, let evalQueue = evalType.evalQueue  else
                    {
                        RfSDK.assertionFailure("\(sourceIdentity): No scheduler or evalQueue available")
                        return
                    }

                    // Propagate the Eval Begin notification.
                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                    hasTerminated = false
                    scheduleTool.evalQueue = evalQueue
                    scheduleTool.subscribe(settings: settings)

                    scheduleTool.schedulePeriodic(startTime: startTime, period: period, action: { [weak element] (index, vTime) in

                        guard !hasTerminated, let strongElement = element else { return }

                        strongElement.producer.notify(item: index, vTime: vTime)
                    })

                case .eEvalControl(.eEvalEnd(let error), let(vTime)):      // On any other control events.

                    hasTerminated = true

                    if let scheduleTool = scheduleTool
                    {
                        scheduleTool.unsubscribe(vTime: vTime)
                    }

                    // Propagate the notification.
                    element.producer.notify(control: eEvalNotify.eEvalEnd(error), vTime: vTime)

                default:

                    RfSDK.assertionFailure("\(self): Unexpected control event: \(inputNotification)")
            }
        })

        return intervalSource
    }
    
    ///
    /// generatedInterval: The standard Reactive Extensions interval timed-sequence source which employs the Rf.Sources.generate source to generate notifications.
    ///
    
    public static func generatedInterval(_ traceID: Rf.TraceID, startTime: Date, period: TimeInterval, settings : Rf.SchedulerSettings) -> Rf.ASource<Int>
    {
        return Rf.Sources.generate(traceID, startTime: startTime, settings: settings, { (index) in return (period, index) })
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Source compositions, binds the AElement instance method to the static function equivalent.
///

extension Rf.AFabric
{
    @discardableResult public func emit<Item>(traceID: Rf.TraceID = Rf.TraceID("emit"), _ emitType : Rf.eEmit<Item>) -> Rf.ASource<Item>
    {
        let source = Rf.Sources.emit(traceID, type: emitType)

        compose(source)

        return source
    }

    @discardableResult public func emitAsync<Item>(traceID: Rf.TraceID = Rf.TraceID("emit(async)"), _ asyncType: Rf.eEmitAsync<Item>) -> Rf.ASource<Item>
    {
        let source = Rf.Sources.emit(traceID, asyncType: asyncType)

        compose(source)

        return source
    }

    @discardableResult public func emit<Item>(_ errorToThrow : Error) -> Rf.ASource<Item>
    {
        let source : Rf.ASource<Item> = Rf.Sources.emit(errorToThrow: errorToThrow)

        compose(source)

        return source
    }

    @discardableResult public func fromArray<Item>(_ traceID: Rf.TraceID = Rf.TraceID("array"), _ array: ContiguousArray<Item>) -> Rf.ASource<Item>
    {
        let source = Rf.Sources.emit(traceID, type: .eItemArray(array))

        compose(source)

        return source
    }

    @discardableResult public func generate<Item>(_ generatorFunc: @escaping (Int) -> Item?) -> Rf.ASource<Item>
    {
        let source : Rf.ASource<Item> = Rf.Sources.generate(generatorFunc)

        compose(source)

        return source
    }

    @discardableResult public func generate<Item>(_ traceID: Rf.TraceID, startTime: Date, settings : Rf.SchedulerSettings, _ generatorFunc: @escaping (Int) -> (Rf.VTime, Item)?) -> Rf.ASource<Item>
    {
        let source : Rf.ASource<Item> = Rf.Sources.generate(traceID, startTime: startTime, settings: settings, generatorFunc)

        compose(source)

        return source
    }
}

extension Rf.AFabric
{
    public func generatedInterval(startTime: Date, period: TimeInterval, settings : Rf.SchedulerSettings = Rf.SchedulerSettings.SourceDef) -> Rf.ASource<Int>
    {
        let source = Rf.Sources.generatedInterval(Rf.TraceID("interval"), startTime: startTime, period: period, settings: settings)

        compose(source)

        return source
    }

    public func interval(startTime: Date, period: TimeInterval, settings : Rf.SchedulerSettings = Rf.SchedulerSettings.SourceDef) -> Rf.ASource<Int>
    {
        let source = Rf.Sources.interval(Rf.TraceID("interval"), startTime: startTime, period: period, settings: settings)

        compose(source)

        return source
    }
}

