
//
// Reactive Fabric Sample Code (Reactive Fabric Operators)
//
// Created by Terry Stillone on 12/3/18.
// Copyright (c) 2018 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import Foundation

import ReactiveFabric

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Sample operators given here for user interest. A small set of these operators are used in the demonstrator App.
///

extension Rf.Operators  // Event operators
{
    ///
    /// observe item notifications by executing a given onItemFunc closure.
    ///

    public static func observe<Item>(_ traceID: Rf.TraceID, item onItemFunc: @escaping (Item, Rf.VTime?) -> Void) -> Rf.AOperator<Item, Item>
    {
        let onItemOperator : Rf.AOperator<Item, Item> = RfSDK.factory.Operator(traceID)

        onItemOperator.onItem = { [weak onItemOperator] (item: Item, vTime: Rf.VTime?) in

            guard let strongOperator = onItemOperator else { return }

            onItemFunc(item, vTime)

            strongOperator.producer.notify(item: item, vTime: vTime)
        }

        return onItemOperator
    }

    ///
    /// onNotify: Helper operator that employs a given notifyFunc to handle the received notifications.
    ///

    public static func onNotify<InItem, OutItem>(_ traceID : Rf.TraceID, _ notifyFunc: @escaping (Rf.eOperatorNotification<InItem>, Rf.AElement<InItem, OutItem>) -> Void) -> Rf.AOperator<InItem, OutItem>
    {
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let onNotifyOperator : Rf.AOperator<InItem, OutItem> = RfSDK.factory.Operator(traceID)

        var currentIndex = 0

        onNotifyOperator.consumer.onItem = { [weak onNotifyOperator] (item: InItem, vTime: Rf.VTime?) in

            guard let strongOperator = onNotifyOperator else { return }

            notifyFunc(.eItem(currentIndex, item, vTime), strongOperator)

            currentIndex += 1
        }

        onNotifyOperator.consumer.onControl = { [weak onNotifyOperator] (control, vTime) in

            guard let strongOperator = onNotifyOperator else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType):

                    if let keys = strongOperator.tools?.keys
                    {
                        for name in keys
                        {
                            notifyFunc(.eFabricTool(name, strongOperator), strongOperator)
                        }
                    }

                    notifyFunc(.eEvalControl(.eEvalBegin(evalType), vTime), strongOperator)

                case eEvalNotify.eEvalRestart(let evalType):

                    currentIndex = 0

                    notifyFunc(.eEvalControl(.eEvalRestart(evalType), vTime), strongOperator)

                case eEvalNotify.eEvalEnd(let error):

                    notifyFunc(.eEvalControl(.eEvalEnd(error), vTime), strongOperator)

                case eFabricNotify.eInstallTool:

                    strongOperator.producer.notify(control: control, vTime: vTime)

                default:

                    strongOperator.producer.notify(control: control, vTime: vTime)
            }
        }

        return onNotifyOperator
    }

    ///
    /// onThrowableNotify: Helper operator that employs a given notifyFunc to handle the received notifications. Throws on notifyFunc exception.
    ///

    public static func onThrowableNotify<InItem, OutItem>(_ traceID : Rf.TraceID, _ notifyFunc: @escaping (Rf.eOperatorNotification<InItem>, Rf.AElement<InItem, OutItem>) throws -> Void) -> Rf.AOperator<InItem, OutItem>
    {
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalNotify = Rf.EvalScope.eNotify
        typealias eEvalRequest = Rf.EvalScope.eRequest

        let onNotifyOperator : Rf.AOperator<InItem, OutItem> = RfSDK.factory.Operator(traceID)

        var currentIndex = 0

        onNotifyOperator.consumer.onItem = { [weak onNotifyOperator] (item: InItem, vTime: Rf.VTime?) in

            guard let strongOperator = onNotifyOperator else { return }

            do
            {
                try notifyFunc(.eItem(currentIndex, item, vTime), strongOperator)
            }
            catch let ex
            {
                switch ex
                {
                    case Rf.eError.eEvalEndRequest(let error):

                        strongOperator.endEval(error, vTime: vTime)

                    default:

                        strongOperator.endEval(Rf.eError.eException(ex), vTime: vTime)
                }
            }

            currentIndex += 1
        }

        onNotifyOperator.consumer.onControl = { [weak onNotifyOperator] (control, vTime) in

            guard let strongOperator = onNotifyOperator else { return }

            func callNotifyFunc(_ notification: Rf.eOperatorNotification<InItem>, _ element: Rf.AElement<InItem, OutItem>)
            {
                do
                {
                    try notifyFunc(notification, element)
                }
                catch let ex
                {
                    switch ex
                    {
                        case Rf.eError.eEvalEndRequest(let error):

                            strongOperator.endEval(error, vTime: vTime)

                        default:

                            strongOperator.endEval(Rf.eError.eException(ex), vTime: vTime)
                    }
                }
            }

            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType):

                    if let keys = strongOperator.tools?.keys
                    {
                        for name in keys
                        {
                            callNotifyFunc(.eFabricTool(name, strongOperator), strongOperator)
                        }
                    }

                    callNotifyFunc(.eEvalControl(.eEvalBegin(evalType), vTime), strongOperator)

                case eEvalNotify.eEvalRestart(let evalType):

                    currentIndex = 0

                    callNotifyFunc(.eEvalControl(.eEvalRestart(evalType), vTime), strongOperator)

                case eEvalNotify.eEvalEnd(let error):

                    callNotifyFunc(.eEvalControl(.eEvalEnd(error), vTime), strongOperator)

                default:

                    strongOperator.producer.notify(control: control, vTime: vTime)
            }
        }

        return onNotifyOperator
    }

}

extension Rf.Operators  // Event operators
{
    ///
    /// finally: Execute a given closure on completion.
    ///

    public static func finally<Item>(_ action: @escaping () -> Void) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let finallyOperator : Rf.AOperator<Item, Item> = RfSDK.factory.Operator(Rf.TraceID("finally"))

        finallyOperator.consumer.onControl = { [weak finallyOperator] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongOperator = finallyOperator else { return }

            if case eEvalNotify.eEvalEnd = control
            {
                action()
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return finallyOperator
    }
}

extension Rf.Operators  // Boolean operators
{
    ///
    /// isEmpty: Emit a boolean indicator of whether the received notifications were empty.
    ///

    public static func isEmpty<Item>() -> Rf.AOperator<Item, Bool>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let isEmptyOperator : Rf.AOperator<Item, Bool> = RfSDK.factory.Operator(Rf.TraceID("isEmpty"))
        var haveReceivedItem = false

        isEmptyOperator.consumer.onItem = { [weak isEmptyOperator] (item: Item, vTime: Rf.VTime?) in

            guard let strongOperator = isEmptyOperator else { return }

            haveReceivedItem = true

            strongOperator.producer.notify(item: false, vTime: vTime)
            strongOperator.endEval(vTime: vTime)
        }

        isEmptyOperator.consumer.onControl = { [weak isEmptyOperator] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongOperator = isEmptyOperator else { return }

            if case eEvalNotify.eEvalEnd = control, !haveReceivedItem
            {
                strongOperator.producer.notify(item: true, vTime: vTime)
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return isEmptyOperator
    }

    ///
    /// any: Emit a boolean indicator of whether any of the received notifications match the given predicate.
    ///

    public static func any<Item>(_ predicate: @escaping (Item) -> Bool) -> Rf.AOperator<Item, Bool>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let anyOperator : Rf.AOperator<Item, Bool> = RfSDK.factory.Operator(Rf.TraceID("any"))

        var haveEmittedResult = false

        anyOperator.consumer.onItem = { [weak anyOperator] (item: Item, vTime: Rf.VTime?) in

            guard let strongOperator = anyOperator else { return }

            if predicate(item)
            {
                haveEmittedResult = true
                strongOperator.producer.notify(item: true, vTime: vTime)
                strongOperator.endEval(vTime: vTime)
            }
        }

        anyOperator.consumer.onControl = { [weak anyOperator] (control, vTime) in

            guard let strongOperator = anyOperator else { return }

            if case eEvalNotify.eEvalEnd = control, !haveEmittedResult
            {
                strongOperator.producer.notify(item: false, vTime: vTime)
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return anyOperator
    }

    ///
    /// all: Emit a boolean indicator of whether all of the received notifications match the given item.
    ///

    public static func all<Item>(_ predicate: @escaping (Item) -> Bool) -> Rf.AOperator<Item, Bool>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let allOperator : Rf.AOperator<Item, Bool> = RfSDK.factory.Operator(Rf.TraceID("all"))

        var haveEmittedResult = false
        var didHaveItems = false

        allOperator.consumer.onItem = { [weak allOperator] (item: Item, vTime: Rf.VTime?) in

            guard let strongOperator = allOperator else { return }

            didHaveItems = true

            if !predicate(item)
            {
                haveEmittedResult = true
                strongOperator.producer.notify(item: false, vTime: vTime)
                strongOperator.endEval(vTime: vTime)
            }
        }

        allOperator.consumer.onControl = { [weak allOperator] (control, vTime) in

            guard let strongOperator = allOperator else { return }

            if case eEvalNotify.eEvalEnd = control, !haveEmittedResult
            {
                strongOperator.producer.notify(item: didHaveItems, vTime: vTime)
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return allOperator
    }

    ///
    /// contains: Emit a boolean indicator of whether any of the received notifications match the given item.
    ///

    public static func contains<Item: Equatable>(_ matchItem: Item) -> Rf.AOperator<Item, Bool>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let containsOperator : Rf.AOperator<Item, Bool> = RfSDK.factory.Operator(Rf.TraceID("contains"))
        var haveMatchedItem  = false

        containsOperator.consumer.onItem = { [weak containsOperator] (item: Item, vTime: Rf.VTime?) in

            guard let strongOperator = containsOperator else { return }

            if matchItem == item
            {
                haveMatchedItem = true
                strongOperator.producer.notify(item: true, vTime: vTime)
                strongOperator.endEval(vTime: vTime)
            }
        }

        containsOperator.consumer.onControl = { [weak containsOperator] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongOperator = containsOperator else { return }

            if case eEvalNotify.eEvalEnd = control, !haveMatchedItem
            {
                strongOperator.producer.notify(item: false, vTime: vTime)
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return containsOperator
    }
}

extension Rf.Operators // Filtering and mapping operators
{
    ///
    /// take: Emit count items from the beginning.
    ///

    public static func take<Item>(_ count: UInt) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID = Rf.TraceID("take")
        let takeOperator : Rf.AOperator<Item, Item> = Rf.Operators.onNotify(traceID, { (inputNotification, element) in

            switch inputNotification
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):  // On a begin evaluation Control notification.

                    // Propagate the Eval Begin event.
                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                    if count == 0
                    {
                        // Immediately request evaluation end.
                        element.endEval(vTime: vTime)
                    }

                case .eItem(let (index, item, vTime)):             // On an Item notification.

                    let count1 = Int(count - 1)

                    switch index
                    {
                            // Propagate the item.
                        case 0..<count1:    element.producer.notify(item: item, vTime: vTime)

                            // Propagate the item and then request evaluation end.
                        case count1:        element.producer.notify(item: item, vTime: vTime)
                            element.endEval(tag: traceID.description, vTime: vTime)

                            // End of evaluation with an error.
                        default:            RfSDK.assertionFailure("\(traceID.description): Unexpected index.")
                            element.endEval(Rf.eError("take: Unexpected index."), vTime: vTime)
                    }

                case .eEvalControl(let (evalEvent, vTime)):      // On any other control notification.

                    // Propagate the event.
                    element.producer.notify(control: evalEvent, vTime: vTime)

                case .eFabricTool:                      // On a tool availability event.

                    // No tools required.
                    break
            }
        })

        return takeOperator
    }

    ///
    /// throwableTake: Emit count items from the beginning and throw an exception of less than count received.
    ///

    public static func throwableTake<Item>(_ count: UInt) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID = Rf.TraceID("take")
        let takeOperator : Rf.AOperator<Item, Item> = Rf.Operators.onThrowableNotify(traceID, { (inputNotification, element) in

            switch inputNotification
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):  // On a begin evaluation Control notification.

                    // Propagate the Eval Begin event.
                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                    if count == 0
                    {
                        // Immediately raise an end of evaluation.
                        throw Rf.eError.eEvalEndRequest(nil)
                    }

                case .eItem(let (index, item, vTime)):             // On an Item notification.

                    let count1 = Int(count - 1)

                    switch index
                    {
                            // Propagate the item.
                        case 0..<count1:   element.producer.notify(item: item, vTime: vTime)

                            // Propagate the item and then request evaluation end.
                        case count1:       element.producer.notify(item: item, vTime: vTime); throw Rf.eError.eEvalEndRequest(nil)

                            // Request end of evaluation with an error.
                        default:           throw Rf.eError.eEvalEndRequest(Rf.eError("take: Unexpected index"))
                    }

                case .eEvalControl(let (control, vTime)):       // On any other control notification.

                    // Propagate the event.
                    element.producer.notify(control: control, vTime: vTime)

                case .eFabricTool:                              // On a tool availability event.

                    // No tools required.
                    break
            }
        })

        return takeOperator
    }

    ///
    /// takeLast: Emit count items from the end.
    ///

    public static func takeLast<Item>(_ count: UInt) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID = Rf.TraceID("takeLast")
        let buffer = Rf.Notifications.Store<Item >(traceID.appending("buffer"))

        let takeLastOperator: Rf.AOperator<Item, Item> = Rf.Operators.onNotify(traceID, { (inputNotification, element) in

            switch inputNotification
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):

                    buffer.removeAll()

                    if count != 0
                    {
                        element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)
                    }
                    else
                    {
                        // Propagate the eval begin and then end eval.
                        element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)
                        element.endEval(vTime: vTime)
                    }

                case .eItem(let (_, item, _)):

                    buffer.queue(item: item)

                    if buffer.itemCount > count
                    {
                        buffer.pop()
                    }

                case .eEvalControl(.eEvalEnd(let error), (let vTime)):

                    buffer.send(itemsTo: element.producer, vTime: vTime)
                    element.producer.notify(control: eEvalNotify.eEvalEnd(error), vTime: vTime)

                case .eEvalControl(let (event, vTime)):

                    element.producer.notify(control: event, vTime: vTime)

                case .eFabricTool:

                    // No tools required.
                    break
            }
        })

        return takeLastOperator
    }

    ///
    /// take: Emit items while the predicate is true.
    ///

    public static func take<Item>(while predicate: @escaping (Int, Item, Rf.VTime?) -> Bool) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID = Rf.TraceID("takeWhile")
        let takeWhileOperator: Rf.AOperator<Item, Item> = Rf.Operators.onNotify(traceID, { (inputNotification, element) in

            switch inputNotification
            {
                case .eItem(let (index, item, vTime)):             // On an Item notification.

                    if predicate(index, item, vTime)
                    {
                        // Propagate the item notification.
                        element.producer.notify(item: item, vTime: vTime)
                    }
                    else
                    {
                        // Request an end of evaluation.
                        element.endEval(nil, vTime: vTime)
                    }

                case .eEvalControl(let (control, vTime)):       // On any control notification.

                    // Propagate the event.
                    element.producer.notify(control: control, vTime: vTime)

                case .eFabricTool:                              // On a tool availability event.

                    // No tools required.
                    break
            }
        })

        return takeWhileOperator
    }

    ///
    /// skip: Skip count items from the beginning.
    ///

    public static func skip<Item>(_ count: UInt) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let intCount = Int(count)

        let skipOperator : Rf.AOperator<Item, Item> = Rf.Operators.onNotify(Rf.TraceID("skip"), { (request, element) in

            switch request
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):

                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                case .eItem(let (index, item, vTime)):

                    switch index
                    {
                        case 0..<intCount:   break
                        default:             element.producer.notify(item: item, vTime: vTime)
                    }

                case .eEvalControl(.eEvalEnd(let error), let(vTime)):

                    element.producer.notify(control: eEvalNotify.eEvalEnd(error), vTime: vTime)

                case .eEvalControl(let (event, vTime)):

                    element.producer.notify(control: event, vTime: vTime)

                case .eFabricTool:

                    // No tools required.
                    break
            }
        })

        return skipOperator
    }

    ///
    /// skipLast: Skip count items from the end.
    ///

    public static func skipLast<Item>(_ count: UInt) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID = Rf.TraceID("skipLast")
        let buffer = Rf.Notifications.Store<Item >(traceID.appending("buffer"))

        let skipLastOperator: Rf.AOperator<Item, Item> = Rf.Operators.onNotify(traceID, { (request, element) in

            switch request
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):

                    buffer.removeAll()
                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                case .eItem(let (_, item, vTime)):

                    buffer.queue(item: item)

                    while buffer.itemCount > count
                    {
                        let queuedItem = buffer.unqueue()

                        element.producer.notify(item: queuedItem!, vTime: vTime)
                    }

                case .eEvalControl(.eEvalEnd(let error), (let vTime)):

                    element.producer.notify(control: eEvalNotify.eEvalEnd(error), vTime: vTime)

                case .eEvalControl(let (event, vTime)):

                    element.producer.notify(control: event, vTime: vTime)

                case .eFabricTool:

                    // No tools required.
                    break
            }
        })

        return skipLastOperator
    }

    ///
    /// filter: Emit only items that are returned by a given filter closure.
    ///

    public static func filter<Item>(_ filterFunc: @escaping (Item, Rf.VTime?) -> Bool?) -> Rf.AOperator<Item, Item>
    {
        let filterOperator : Rf.AOperator<Item, Item> = RfSDK.factory.Operator(Rf.TraceID("filter"))

        filterOperator.consumer.onItem = { [weak filterOperator] (item: Item, vTime: Rf.VTime?) in

            guard let strongOperator = filterOperator else { return }

            if let accept = filterFunc(item, vTime)
            {
                if accept
                {
                    strongOperator.producer.notify(item: item, vTime: vTime)
                }
            }
            else
            {
                strongOperator.endEval(vTime: vTime)
            }
        }

        return filterOperator
    }

    ///
    /// distinctUntilChanged: Emit only items that are unique to their previous item.
    ///

    public static func distinctUntilChanged<Item: Equatable>() -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        var lastItem : Item? = nil

        let distinctOperator: Rf.AOperator<Item, Item> = Rf.Operators.onNotify(Rf.TraceID("distinctUntilChanged"), { (request, element) in

            switch request
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):

                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                case .eItem(let (_, item, vTime)):

                    if lastItem == nil || lastItem! != item
                    {
                        lastItem = item
                        element.producer.notify(item: item, vTime: vTime)
                    }

                case .eEvalControl(let (event, vTime)):

                    element.producer.notify(control: event, vTime: vTime)

                case .eFabricTool:

                    // No tools required.
                    break
            }
        })

        return distinctOperator
    }

    ///
    /// first: Emit only the first item. Failure handling defined by the onFailure parameter.
    ///

    public static func first<Item>(onFailure: Rf.eFailureOptions<Item> = .eNothing) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        var hasEmittedItem = false

        let traceID = Rf.TraceID("first")
        let firstOperator: Rf.AOperator<Item, Item> = Rf.Operators.onNotify(traceID, { (request, element) in

            switch request
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):

                    hasEmittedItem = false
                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                case .eItem(let (_, item, vTime)):

                    hasEmittedItem = true
                    element.producer.notify(item: item, vTime: vTime)
                    element.endEval(vTime: vTime)

                case .eEvalControl(.eEvalEnd(let error), (let vTime)):

                    if (!hasEmittedItem)
                    {
                        switch onFailure
                        {
                            case .eEmit(let failureItem):

                                element.producer.notify(item: failureItem, vTime: vTime)

                            case .eEmitError(let onFailureError):

                                element.producer.notify(control: eEvalNotify.eEvalEnd(onFailureError), vTime: vTime)
                                return

                            case .eNothing:

                                break
                        }
                    }

                    element.producer.notify(control: eEvalNotify.eEvalEnd(error), vTime: vTime)

                case .eEvalControl(let (event, vTime)):

                    element.producer.notify(control: event, vTime: vTime)

                case .eFabricTool:

                    // No tools required.
                    break
            }
        })

        return firstOperator
    }

    ///
    /// last: Emit only the last item. Failure handling defined by the onFailure parameter.
    ///

    public static func last<Item>(onFailure: Rf.eFailureOptions<Item> = .eNothing) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        var lastItem : Item? = nil

        let lastOperator: Rf.AOperator<Item, Item> = Rf.Operators.onNotify(Rf.TraceID("last"), { (request, element) in

            switch request
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):

                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                case .eItem(let (_, item, _)):

                    lastItem = item

                case .eEvalControl(.eEvalEnd(let error), (let vTime)):

                    if let lastItem = lastItem
                    {
                        element.producer.notify(item: lastItem, vTime: vTime)
                    }
                    else
                    {
                        switch onFailure
                        {
                            case .eEmit(let failureItem):

                                element.producer.notify(item: failureItem, vTime: vTime)

                            case .eEmitError(let onFailureError):

                                element.producer.notify(control: eEvalNotify.eEvalEnd(onFailureError), vTime: vTime)
                                return

                            case .eNothing:

                                break
                        }
                    }

                    element.producer.notify(control: eEvalNotify.eEvalEnd(error), vTime: vTime)

                case .eEvalControl(let (event, vTime)):

                    element.producer.notify(control: event, vTime: vTime)

                case .eFabricTool:

                    // No tools required.
                    break
            }
        })

        return lastOperator
    }

    ///
    /// map: Synchronous map received items as given by a mapping closure.
    ///

    public static func map<InItem, OutItem>(_ mapFunc: @escaping (InItem) -> OutItem?) -> Rf.AOperator<InItem, OutItem>
    {
        let mapOperator : Rf.AOperator<InItem, OutItem> = RfSDK.factory.Operator(Rf.TraceID("map"))

        mapOperator.consumer.onItem = { [weak mapOperator] (item: InItem, vTime: Rf.VTime?) in

            guard let strongOperator = mapOperator else { return }

            if let outItem = mapFunc(item)
            {
                strongOperator.producer.notify(item: outItem, vTime: vTime)
            }
            else
            {
                strongOperator.endEval(vTime: vTime)
            }
        }

        return mapOperator
    }

    ///
    /// map: Asynchronous map received items as given by a mapping closure.
    ///

    public static func map<InItem, OutItem>(_ mapFunc: @escaping (InItem, Rf.VTime?) -> (OutItem, Rf.VTime?)?) -> Rf.AOperator<InItem, (OutItem, Rf.VTime?)>
    {
        let mapOperator : Rf.AOperator<InItem, (OutItem, Rf.VTime?)> = RfSDK.factory.Operator(Rf.TraceID("map"))

        mapOperator.consumer.onItem = { [weak mapOperator] (item: InItem, vTime: Rf.VTime?) in

            guard let strongOperator = mapOperator else { return }

            if let (outItem, outVTime) = mapFunc(item, vTime)
            {
                strongOperator.producer.notify(item: (outItem, outVTime), vTime: vTime)
            }
            else
            {
                strongOperator.endEval(vTime: vTime)
            }
        }

        return mapOperator
    }
}

extension Rf.Operators // Aggregation and Summary operators
{
    ///
    /// count: Emit the count of items received.
    ///

    public static func count<Item>(_ countFunc: ((Item) -> Bool)? = nil) -> Rf.AOperator<Item, Int>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let countOperator : Rf.AOperator<Item, Int> = RfSDK.factory.Operator(Rf.TraceID("count"))
        var currentCount = 0

        if let countFunc = countFunc
        {
            countOperator.consumer.onItem = { (item: Item, vTime: Rf.VTime?) in

                if countFunc(item)
                {
                    currentCount += 1
                }
            }
        }
        else
        {
            countOperator.consumer.onItem = { (item: Item, vTime: Rf.VTime?) in

                currentCount += 1
            }
        }

        countOperator.consumer.onControl = { [weak countOperator] (control, vTime) in

            guard let strongOperator = countOperator else { return }

            if case eEvalNotify.eEvalEnd = control
            {
                strongOperator.producer.notify(item: currentCount, vTime: vTime)
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return countOperator
    }

    ///
    /// average: Emit the average of items received.
    ///

    public static func average<InItem : Numeric, OutItem : FloatingPoint>(averageFunc: @escaping (InItem, Int) -> OutItem) -> Rf.AOperator<InItem, OutItem>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        var sum : InItem? = nil
        var count = Int(0)

        let averageOperator: Rf.AOperator<InItem, OutItem> = Rf.Operators.onNotify(Rf.TraceID("average"), { (request, element) in

            switch request
            {
                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):

                    sum = nil
                    count = 0

                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                case .eItem(let (_, item, _)):

                    if sum == nil
                    {
                        sum = item
                    }
                    else
                    {
                        sum! += item
                    }

                    count += 1

                case .eEvalControl(.eEvalEnd(let error), (let vTime)):

                    var effectiveError = error

                    if count == 0
                    {
                        effectiveError = effectiveError ?? Rf.eError("No items")
                    }
                    else if let sum = sum
                    {
                        element.producer.notify(item: averageFunc(sum, count), vTime: vTime)
                    }
                    else
                    {
                        effectiveError = effectiveError ?? Rf.eError("Average: Cannot convert result")
                    }

                    element.producer.notify(control: eEvalNotify.eEvalEnd(effectiveError), vTime: vTime)

                case .eEvalControl(let (event, vTime)):

                    element.producer.notify(control: event, vTime: vTime)

                case .eFabricTool:

                    // No tools required.
                    break
            }
        })

        return averageOperator
    }
}

extension Rf.Operators // Temporal operators
{
    ///
    /// throttle: Throttle emission to once per duration period.
    ///

    public static func throttle<Item>(_ duration : TimeInterval, settings : Rf.SchedulerSettings) -> Rf.AOperator<Item, Item>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID                           = Rf.TraceID("throttle")
        var scheduleTool : Rf.Tools.Schedule? = nil             // The fabric scheduler.
        var allowItemsToPass                  = true            // Indicator of whether to pass item notifications.

        // Use the onNotify operator to service input notifications.
        let throttleOperator : Rf.AOperator<Item, Item> = Rf.Operators.onNotify(traceID, { (inputNotification, element) in

            switch inputNotification
            {
                case .eFabricTool(let (name, tools)):

                    if name == "scheduler", let tool = tools.getScheduleTool(traceID)
                    {
                        scheduleTool = tool
                    }

                case .eEvalControl(.eEvalBegin(let evalType), (let vTime)):  // On a begin evaluation Control notification.

                    guard let scheduleTool = scheduleTool, let evalQueue = evalType.evalQueue  else
                    {
                        RfSDK.assertionFailure("\(traceID): No scheduler or evalQueue available")
                        return
                    }

                    evalQueue.dispatch(sync: {
                        allowItemsToPass = true
                    })

                    // Subscribe to the scheduleTool.
                    scheduleTool.evalQueue = evalQueue
                    scheduleTool.subscribe(settings: settings)

                    // Propagate the Eval Begin event.
                    element.producer.notify(control: eEvalNotify.eEvalBegin(evalType), vTime: vTime)

                case .eItem(let (_, item, vTime)):             // On an Item notification.

                    // Selectively propagate item notifications given by allowItemsToPass.
                    if allowItemsToPass
                    {
                        allowItemsToPass = false

                        // Schedule a throttle time window, depending on the type of time reference we have for the current time.
                        if let vTime = vTime
                        {
                            // A virtual time reference is available, use it to schedule the next processing event.
                            scheduleTool!.schedule(atVTime: duration + vTime, action: { (time) in

                                allowItemsToPass = true
                            })
                        }
                        else
                        {
                            // Use a real time reference instead to schedule the next processing event.
                            scheduleTool!.schedule(atTime: Date(timeIntervalSinceNow: duration), action: { (time) in

                                allowItemsToPass = true
                            })
                        }

                        element.producer.notify(item: item, vTime: vTime)
                    }

                case .eEvalControl(let (evalEvent, vTime)):      // On any other control notification.

                    if let scheduleTool = scheduleTool
                    {
                        // Unsubscribe from the scheduleTool.
                        scheduleTool.unsubscribe(vTime: vTime)
                    }

                    // Propagate the event.
                    element.producer.notify(control: evalEvent, vTime: vTime)
            }
        })

        return throttleOperator
    }

    ///
    /// delay: Delay emission of received item notifications by a given duration.
    ///

    public static func delay<Item>(_ duration: TimeInterval, settings: Rf.SchedulerSettings) -> Rf.AOperator<Item, Item>
    {
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID                                 = Rf.TraceID("delay")
        let delayOperator: Rf.AOperator<Item, Item> = RfSDK.factory.Operator(traceID)

        var scheduleTool: Rf.Tools.Schedule? = nil

        // Schedule a delayed action. The scheduled time depends on whether a virtual time reference is available.
        func schedule(_ vTime: Rf.VTime?, _ action: @escaping (Rf.VTime) -> Void)
        {
            guard let scheduleTool = scheduleTool else { return }

            if let vTime = vTime
            {
                // Schedule by virtual time.
                scheduleTool.schedule(atVTime: vTime + duration, action: action)
            }
            else
            {
                // Schedule by real time.
                scheduleTool.schedule(atTime: Date(timeIntervalSinceNow: duration), action: action)
            }
        }

        delayOperator.consumer.onItem = { [weak delayOperator] (item: Item, vTime: Rf.VTime?) in

            // Schedule the output notification.
            schedule(vTime, { (scheduledVTime) in

                guard let strongOperator2 = delayOperator else { return }

                strongOperator2.producer.notify(item: item, vTime: scheduledVTime)
            })
        }

        delayOperator.consumer.onControl = { [weak delayOperator] (control: IRfControl, vTime: Rf.VTime?) in

            guard let strongOperator = delayOperator else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType):

                    guard let scheduleTool = scheduleTool else
                    {
                        strongOperator.producer.notify(control: control, vTime: vTime)
                        strongOperator.endEval(Rf.eError("\(strongOperator): No Scheduler defined in fabric"), vTime: vTime)
                        return
                    }

                    scheduleTool.subscribe(settings: Rf.SchedulerSettings.OperatorDef)
                    scheduleTool.evalQueue = evalType.evalQueue

                case eEvalNotify.eEvalRestart:

                    guard let scheduleTool = scheduleTool else { return }

                    scheduleTool.restart(vTime: vTime)

                case eEvalNotify.eEvalEnd:

                    // Schedule the output notification.
                    schedule(vTime, { [weak scheduleTool] (scheduledVTime) in

                        guard let strongOperator2 = delayOperator else { return }

                        strongOperator2.producer.notify(control: control, vTime: scheduledVTime)

                        if let scheduleTool = scheduleTool
                        {
                            scheduleTool.unsubscribe(vTime: vTime)
                        }
                    })

                    return

                case eFabricNotify.eInstallTool(.eScheduler(let (_, name, _))):

                    if name == "scheduler", let tool = strongOperator.getScheduleTool(traceID)
                    {
                        scheduleTool = tool
                    }

                default:

                    break
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return delayOperator
    }
}

extension Rf.Operators // Play Operators.
{
    ///
    /// playNotifications: Emit the recevied timestamped item notifications on/at their timestamps.
    ///

    public static func playNotifications<Item>() -> Rf.AOperator<Rf.eTimestampedNotification<Item>, Rf.eTimestampedNotification<Item>>
    {
        typealias ItemType = Rf.eTimestampedNotification<Item>
        typealias eFabricNotify = Rf.FabricScope.eNotify
        typealias eEvalRequest = Rf.EvalScope.eRequest
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceID                                         = Rf.TraceID("playNotifications")
        var scheduleTool : Rf.Tools.Schedule!
        var startVTime : Rf.VTime!
        let playOperator : Rf.AOperator<ItemType, ItemType> = RfSDK.factory.Operator(traceID)

        playOperator.consumer.onItem = { [weak playOperator] (timedNotification: ItemType, vTime: Rf.VTime?) in

            assert(scheduleTool != nil, "\(traceID): No assigned scheduler")

            switch timedNotification
            {
                case .eItem(let (_, timeOffset)):

                    scheduleTool.schedule(atVTime: startVTime + timeOffset!, action: { (vTime) in

                        guard let strongOperator = playOperator else { return }

                        strongOperator.producer.notify(item: timedNotification, vTime: vTime)
                    })

                case .eControl(let (control, timeOffset)):

                    scheduleTool.schedule(atVTime: startVTime + timeOffset!, action: { (vTime) in

                        guard let strongOperator = playOperator, case eEvalRequest.eRequestEvalEnd(let (error, _)) = control else { return }

                        strongOperator.endEval(error, vTime: vTime)
                    })
            }
        }

        playOperator.consumer.onControl = { [weak playOperator] (control, vTime) in

            guard let strongOperator = playOperator else { return }

            switch control
            {
                case eEvalNotify.eEvalBegin(let evalType):

                    assert(scheduleTool != nil, "\(traceID): No assigned scheduler")

                    // Set the start reference time.
                    startVTime = vTime ?? scheduleTool.currentVTime

                    // Subscribe to the scheduler.
                    scheduleTool.evalQueue = evalType.evalQueue
                    scheduleTool.subscribe(settings: Rf.SchedulerSettings.SourceDef)

                    // Propagate the reference time through the begin eval control notification.
                    strongOperator.producer.notify(control: control, vTime: startVTime)

                    return

                case eEvalNotify.eEvalRestart:

                    guard let scheduleTool = scheduleTool else { return }

                    scheduleTool.restart(vTime: vTime)

                case eEvalNotify.eEvalEnd:

                    scheduleTool.unsubscribe(vTime: vTime)

                case eFabricNotify.eInstallTool(.eScheduler(let (_, name, _))):

                    if name == "scheduler", let tool = strongOperator.getScheduleTool(traceID)
                    {
                        scheduleTool = tool
                    }

                default:

                    break
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return playOperator
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Operator compositions, binds the AElement instance method to the static function equivalent.
///

extension Rf.AElement
{
    @discardableResult public func onNotify<TargetType>(_ traceID : Rf.TraceID, _ filterFunc: @escaping (Rf.eOperatorNotification<OutItem>, Rf.AElement<OutItem, TargetType>) -> Void) -> Rf.AOperator<OutItem, TargetType>
    {
        let onNotifyOperator = Rf.Operators.onNotify(traceID, filterFunc)

        compose(onNotifyOperator)

        return onNotifyOperator
    }

    @discardableResult public func observe(_ traceID: Rf.TraceID = Rf.TraceID("observeItem"), item onItemFunc: @escaping (OutItem, Rf.VTime?) -> Void) -> Rf.AOperator<OutItem, OutItem>
    {
        let onItemOperator = Rf.Operators.observe(traceID, item: onItemFunc)

        compose(onItemOperator)

        return onItemOperator
    }
}

extension Rf.AElement   // Filter and Map operators
{
    @discardableResult public func take(_ count: UInt) -> Rf.AOperator<OutItem, OutItem>
    {
        // Instantiate take operator.
        let takeOperator : Rf.AOperator<OutItem, OutItem> = Rf.Operators.take(count)

        // Compose Take operator with the self operator.
        compose(takeOperator)

        // Return the take operator for composure with subsequent operators.
        return takeOperator
    }

    @discardableResult public func throwableTake(_ count: UInt) -> Rf.AOperator<OutItem, OutItem>
    {
        // Instantiate take operator.
        let takeOperator : Rf.AOperator<OutItem, OutItem> = Rf.Operators.throwableTake(count)

        // Compose Take operator with the self operator.
        compose(takeOperator)

        // Return the take operator for composure with subsequent operators.
        return takeOperator
    }

    @discardableResult public func takeLast(_ count: UInt) -> Rf.AOperator<OutItem, OutItem>
    {
        let takeLastOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.takeLast(count)

        compose(takeLastOperator)

        return takeLastOperator
    }

    @discardableResult public func skip(_ count: UInt) -> Rf.AOperator<OutItem, OutItem>
    {
        let skipOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.skip(count)

        compose(skipOperator)

        return skipOperator
    }

    @discardableResult public func skipLast(_ count: UInt) -> Rf.AOperator<OutItem, OutItem>
    {
        let skipLastOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.skipLast(count)

        compose(skipLastOperator)

        return skipLastOperator
    }

    @discardableResult public func take(while predicate: @escaping (Int, OutItem, Rf.VTime?) -> Bool) -> Rf.AOperator<OutItem, OutItem>
    {
        let takeWhileOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.take(while: predicate)

        compose(takeWhileOperator)

        return takeWhileOperator
    }

    @discardableResult public func filter(_ filterFunc: @escaping (OutItem, Rf.VTime?) -> Bool?) -> Rf.AOperator<OutItem, OutItem>
    {
        let filterOperator = Rf.Operators.filter(filterFunc)

        compose(filterOperator)

        return filterOperator
    }

    @discardableResult public func first(onFailure: Rf.eFailureOptions<OutItem> = .eNothing) -> Rf.AOperator<OutItem, OutItem>
    {
        let firstOperator = Rf.Operators.first(onFailure: onFailure)

        compose(firstOperator)

        return firstOperator
    }

    @discardableResult public func last(onFailure: Rf.eFailureOptions<OutItem> = .eNothing) -> Rf.AOperator<OutItem, OutItem>
    {
        let lastOperator = Rf.Operators.last(onFailure: onFailure)

        compose(lastOperator)

        return lastOperator
    }

    @discardableResult public func map<MappedItem>(_ mapFunc: @escaping (OutItem) -> MappedItem?) -> Rf.AOperator<OutItem, MappedItem>
    {
        let mapOperator = Rf.Operators.map(mapFunc)

        compose(mapOperator)

        return mapOperator
    }

    @discardableResult public func map<MappedItem>(_ mapFunc: @escaping (OutItem, Rf.VTime?) -> (MappedItem, Rf.VTime?)?) -> Rf.AOperator<OutItem, (MappedItem, Rf.VTime?)>
    {
        let mapOperator : Rf.AOperator<OutItem, (MappedItem, Rf.VTime?)> = Rf.Operators.map(mapFunc)

        compose(mapOperator)

        return mapOperator
    }
}

extension Rf.AElement   // Bool operators
{
    @discardableResult public func isEmpty() -> Rf.AOperator<OutItem, Bool>
    {
        let isEmptyOperator : Rf.AOperator<OutItem, Bool> = Rf.Operators.isEmpty()

        compose(isEmptyOperator)

        return isEmptyOperator
    }

    @discardableResult public func any(_ predicate: @escaping (OutItem) -> Bool) -> Rf.AOperator<OutItem, Bool>
    {
        let anyOperator : Rf.AOperator<OutItem, Bool> = Rf.Operators.any(predicate)

        compose(anyOperator)

        return anyOperator
    }

    @discardableResult public func all(_ predicate: @escaping (OutItem) -> Bool) -> Rf.AOperator<OutItem, Bool>
    {
        let allOperator: Rf.AOperator<OutItem, Bool> = Rf.Operators.all(predicate)

        compose(allOperator)

        return allOperator
    }
}

extension Rf.AElement where OutItem : Equatable  // Equatable Bool operators
{
    @discardableResult public func contains(_ item: OutItem) -> Rf.AOperator<OutItem, Bool>
    {
        let containsOperator: Rf.AOperator<OutItem, Bool> = Rf.Operators.contains(item)

        compose(containsOperator)

        return containsOperator
    }
}

extension Rf.AElement // Aggregation and Summary operators
{
    @discardableResult public func count(_ countFunc: ((OutItem) -> Bool)? = nil) -> Rf.AOperator<OutItem, Int>
    {
        let countOperator: Rf.AOperator<OutItem, Int> = Rf.Operators.count(countFunc)

        compose(countOperator)

        return countOperator
    }
}

extension Rf.AElement where OutItem : Numeric
{
    @discardableResult public func average<AverageType : FloatingPoint>(averageFunc: @escaping (OutItem, Int) -> AverageType) -> Rf.AOperator<OutItem, AverageType>
    {
        let averageOperator: Rf.AOperator<OutItem, AverageType> = Rf.Operators.average(averageFunc: averageFunc)

        compose(averageOperator)

        return averageOperator
    }
}

extension Rf.AElement // Temporal operators
{
    @discardableResult public func throttle(_ duration : TimeInterval, settings : Rf.SchedulerSettings = Rf.SchedulerSettings.OperatorDef) -> Rf.AOperator<OutItem, OutItem>
    {
        let throttleOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.throttle(duration, settings: settings)

        compose(throttleOperator)

        return throttleOperator
    }

    @discardableResult public func delay(_ duration : TimeInterval, settings : Rf.SchedulerSettings = Rf.SchedulerSettings.OperatorDef) -> Rf.AOperator<OutItem, OutItem>
    {
        let delayOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.delay(duration, settings: settings)

        compose(delayOperator)

        return delayOperator
    }
}

extension Rf.AElement
{
    @discardableResult public func playNotifications<Item>() -> Rf.AOperator<Rf.eTimestampedNotification<Item>, Rf.eTimestampedNotification<Item>> where OutItem == Rf.eTimestampedNotification<Item>
    {
        let playThroughOperator : Rf.AOperator<Rf.eTimestampedNotification<Item>, Rf.eTimestampedNotification<Item>> = Rf.Operators.playNotifications()

        compose(playThroughOperator)

        return playThroughOperator
    }
}

extension Rf.AElement //Element functions that return evaluation results.
{
    /// Return an evaluation as an array of items, optionally nil on error.
    /// - Parameter timeout: The timeout for the evaluation.
    /// - Returns: The optional array of item results or nil on error.
    public func asArray(timeout: TimeInterval? = nil) -> ContiguousArray<OutItem>?
    {
        let buffer = Rf.Notifications.Store<OutItem>(traceID.appending("buffer"))

        // Channel the notifications to the buffer (store).
        compose(withNotifier: buffer)

        // Begin evaluation and return when completed.
        beginEval(.eSync(nil, timeout))

        // Return the result as an array, or nil on error.
        switch buffer.termination
        {
            case .eNone:                    return buffer.items
            case .eCompletion(let error):   return error == nil ? buffer.items : nil
        }
    }

    /// Return an evaluation as a store.
    /// - Parameter timeout: The timeout for the evaluation.
    /// - Returns: The evaluation result as a Store.
    public func asStore(timeout: TimeInterval? = nil) -> Rf.Notifications.Store<OutItem>
    {
        let buffer = Rf.Notifications.Store<OutItem>(traceID.appending("buffer"))

        // Channel the result of this element to the store.
        compose(withNotifier: buffer)

        // Begin evaluation and return when completed.
        beginEval(.eSync(nil, timeout))

        // Return the result.
        return buffer
    }
}

extension Rf.Operators // Unit Testing Support
{
    ///
    /// onProgress: Report the progress of item notifications to a given closure.
    ///

    public static func onProgress(_ progressFunc : @escaping (String, Float, Int?) -> Void) -> Rf.AOperator<Rf.eNamedResult, Rf.eNamedResult>
    {
        let onProgressOperator : Rf.AOperator<Rf.eNamedResult, Rf.eNamedResult> = RfSDK.factory.Operator(Rf.TraceID("onProgress"))
        
        onProgressOperator.consumer.onItem = { [weak onProgressOperator] (item, vTime) in
            
            guard let strongOperator = onProgressOperator else { return }
            
            if case Rf.eNamedResult.eProgress(let (testSet, fractionDone, totalCount)) = item
            {
                progressFunc(testSet, fractionDone, totalCount)
            }
            
            strongOperator.producer.notify(item: item, vTime: vTime)
        }
        
        return onProgressOperator
    }

    ///
    /// onFailure: Report a failure to a given closure.
    ///

    public static func onFailure(_ failureFunc : @escaping (String, String) -> Void) -> Rf.AOperator<Rf.eNamedResult, Rf.eNamedResult>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify
        typealias eFabricNotify = Rf.FabricScope.eNotify

        var didGetReply = false
        let onFailureOperator : Rf.AOperator<Rf.eNamedResult, Rf.eNamedResult> = RfSDK.factory.Operator(Rf.TraceID("onFailure"))

        onFailureOperator.consumer.onItem = { [weak onFailureOperator] (item, vTime) in

            guard let strongOperator = onFailureOperator else { return }

            didGetReply = true

            if case Rf.eNamedResult.eFailure(let (testName, message)) = item
            {
                failureFunc(testName, message)
            }

            strongOperator.producer.notify(item: item, vTime: vTime)
        }

        onFailureOperator.consumer.onControl = { [weak onFailureOperator] (control, vTime) in

            guard let strongOperator = onFailureOperator else { return }

            switch control
            {
                case eEvalNotify.eEvalEnd(let error):

                    if error != nil
                    {
                        failureFunc("", "\(strongOperator): Error(\(String(describing: error)) received.")
                    }

                case eFabricNotify.eFabricEnd:

                    if !didGetReply
                    {
                        failureFunc("", "\(strongOperator): No result received on fabric end.")
                    }

                case eFabricNotify.eInstallTool, eFabricNotify.eInstrument:

                    break

                case eFabricNotify.eFabricBegin, eEvalNotify.eEvalBegin:

                    break

                default:

                    break
            }

            strongOperator.producer.notify(control: control, vTime: vTime)
        }

        return onFailureOperator
    }

    ///
    /// match: Match received item notifications with a given store.
    ///

    public static func match<Item : Equatable>(_ testName: String, _ store: Rf.Notifications.Store<Item>) -> Rf.AOperator<Item, Rf.eNamedResult>
    {
        typealias eEvalNotify = Rf.EvalScope.eNotify

        let traceId                                             = Rf.TraceID("match")
        let matchOperator : Rf.AOperator<Item, Rf.eNamedResult> = RfSDK.factory.Operator(traceId)
        let itemStore                                           = store

        var storeIndex       = 0
        var itemMatchDidFail = false

        matchOperator.consumer.onItem = { [weak matchOperator] (item: Item, vTime: Rf.VTime?) in

            guard let strongOperator = matchOperator else { return }

            let outNotifier = strongOperator.producer

            if let nextMatchItem = itemStore[storeIndex]
            {
                storeIndex += 1

                if item != nextMatchItem
                {
                    // Item mismatch.
                    itemMatchDidFail = true
                    outNotifier.notify(item: .eFailure(testName, "Expected: \(nextMatchItem), Got: \(item)"), vTime: vTime)
                    strongOperator.endEval(vTime: vTime)
                }
            }
            else
            {
                // Match items missing.
                itemMatchDidFail = true
                outNotifier.notify(item: .eFailure(testName, "Got unexpected trailing item: <\(item)>"), vTime: vTime)
                strongOperator.endEval(vTime: vTime)
            }
        }

        matchOperator.consumer.onControl = { [weak matchOperator] (control, vTime) in

            guard let strongOperator = matchOperator else { return }

            let outNotifier = strongOperator.producer
            let remainingItemCount = itemStore.itemCount - storeIndex

            if case eEvalNotify.eEvalEnd(let error) = control, !itemMatchDidFail
            {
                switch (itemStore.termination.error, error)
                {
                    case (nil, nil):

                        if remainingItemCount > 0
                        {
                            let missingItems = itemStore.items[storeIndex..<itemStore.itemCount]

                            // Extra match items not matched.
                            outNotifier.notify(item: .eFailure(testName, "Did not get all expected items, missing \(remainingItemCount) \(remainingItemCount == 1 ? "item" : "items"): \(missingItems)"), vTime: vTime)
                        }
                        else
                        {
                            outNotifier.notify(item: .eSuccess(testName), vTime: vTime)
                        }

                    case (nil, let selfError?):

                        outNotifier.notify(item: .eFailure(testName, "Expected no error on completion, got \"\(selfError)\""), vTime: vTime)

                    case (let itemStoreError?, nil):

                        outNotifier.notify(item: .eFailure(testName, "Expected \"\(itemStoreError)\" error on completion, got no error"), vTime: vTime)

                    case (let itemStoreError?, let selfError?):

                        if !itemStoreError.isEqual(other: selfError)
                        {
                            outNotifier.notify(item: .eFailure(testName, "Expected \"\(itemStoreError)\" error on completion, got \"\(selfError)\" error"), vTime: vTime)
                        }
                        else if remainingItemCount > 0
                        {
                            let missingItems = itemStore.items[storeIndex..<itemStore.itemCount]

                            // Extra match items not matched.
                            outNotifier.notify(item: .eFailure(testName, "Did not get all expected items, missing \(remainingItemCount) \(remainingItemCount == 1 ? "item" : "items"): \(missingItems)"), vTime: vTime)
                        }
                        else
                        {
                            outNotifier.notify(item: .eSuccess(testName), vTime: vTime)
                        }
                }

                outNotifier.notify(control: eEvalNotify.eEvalEnd(nil), vTime: vTime)
            }
            else
            {
                outNotifier.notify(control: control, vTime: vTime)
            }
        }

        return matchOperator
    }

    ///
    /// match: Match received item notifications with a given item array.
    ///

    public static func match<Item : Equatable>(_ testName: String, _ items: ContiguousArray<Item>, _ error : IRfError? = nil) -> Rf.AOperator<Item, Rf.eNamedResult>
    {
        let store = Rf.Notifications.Store<Item>(Rf.TraceID("matchstore"), items, termination: Rf.eTermination.eCompletion(error))

        return Rf.Operators.match(testName, store)
    }

    ///
    /// match: Match received item notifications with a given store.
    ///

    public static func matchNotifications<Item : Equatable>(_ testName: String, _ store: Rf.Notifications.Store<Rf.eTimestampedNotification<Item>>) -> Rf.AOperator<Rf.eTimestampedNotification<Item>, Rf.eNamedResult>
    {
        typealias ItemType = Rf.eTimestampedNotification<Item>
        typealias eEvalNotify = Rf.EvalScope.eNotify
        typealias eFabricNotify = Rf.FabricScope.eNotify

        let traceId                                                 = Rf.TraceID("match")
        let matchOperator : Rf.AOperator<ItemType, Rf.eNamedResult> = RfSDK.factory.Operator(traceId)
        let itemStore                                               = store

        var storeIndex             = 0
        var startVTime : Rf.VTime? = nil
        var itemMatchDidFail       = false

        matchOperator.consumer.onItem = { [weak matchOperator] (timedNotification: ItemType, vTime: Rf.VTime?) in

            guard let strongOperator = matchOperator else { return }
            guard let startVTime = startVTime else
            {
                RfSDK.assertionFailure("\(strongOperator): Start Virtual Time is invalid")
                return
            }

            guard let vTime = vTime else
            {
                RfSDK.assertionFailure("\(strongOperator): Item Virtual Time is invalid")
                return
            }

            let outNotifier = strongOperator.producer
            let timeOffset = vTime - startVTime

            if let nextMatchItem = itemStore[storeIndex]
            {
                let timeLeeway = abs(timeOffset - nextMatchItem.timeOffset!)

                storeIndex += 1

                if timedNotification != nextMatchItem
                {
                    // Item mismatch.
                    itemMatchDidFail = true
                    outNotifier.notify(item: .eFailure(testName, "Expected: \(nextMatchItem), Got: \(timedNotification)"), vTime: vTime)
                    strongOperator.endEval(vTime: vTime)
                }

                if timeLeeway > RfSDK.Constant.TimerLeewayThreshold
                {
                    itemMatchDidFail = true
                    outNotifier.notify(item: .eFailure(testName, "Scheduling leeway: \(timeLeeway) over threshold: \(RfSDK.Constant.TimerLeewayThreshold)"), vTime: vTime)
                }
            }
            else
            {
                // Match items missing.
                itemMatchDidFail = true
                outNotifier.notify(item: .eFailure(testName, "Got unexpected trailing notification: <\(timedNotification)>"), vTime: vTime)
                strongOperator.endEval(vTime: vTime)
            }
        }

        matchOperator.consumer.onControl = { [weak matchOperator] (control, vTime) in

            guard let strongOperator = matchOperator else { return }

            let outNotifier = strongOperator.producer
            let remainingItemCount = itemStore.itemCount - storeIndex

            switch control
            {
                case eEvalNotify.eEvalBegin:

                    startVTime = vTime

                    outNotifier.notify(control: control, vTime: vTime)

                case eEvalNotify.eEvalEnd(let error) where !itemMatchDidFail:

                    guard let vTime = vTime else
                    {
                        RfSDK.assertionFailure("\(strongOperator): Item Virtual Time is invalid")
                        return
                    }

                    guard let startVTime = startVTime else
                    {
                        RfSDK.assertionFailure("\(strongOperator): Start Virtual Time is invalid")
                        return
                    }

                    if let nextMatchItem = itemStore[storeIndex]
                    {
                        let timeOffset = vTime - startVTime
                        let timeLeeway = abs(timeOffset - nextMatchItem.timeOffset!)

                        if nextMatchItem != control
                        {
                            // Item mismatch.
                            outNotifier.notify(item: .eFailure(testName, "Expected: \(nextMatchItem), Got: \(control)"), vTime: vTime)
                        }
                        else if remainingItemCount > 1
                        {
                            let missingItems = itemStore.items[storeIndex..<itemStore.itemCount]

                            // Extra match items not matched.
                            outNotifier.notify(item: .eFailure(testName, "Did not get all expected items, missing \(remainingItemCount) \(remainingItemCount == 1 ? "item" : "items"): \(missingItems)"), vTime: vTime)
                        }
                        else if timeLeeway > RfSDK.Constant.TimerLeewayThreshold
                        {
                            outNotifier.notify(item: .eFailure(testName, "Scheduling leeway: \(timeLeeway) over threshold: \(RfSDK.Constant.TimerLeewayThreshold)"), vTime: vTime)
                        }
                        else
                        {
                            outNotifier.notify(item: .eSuccess(testName), vTime: vTime)
                        }

                        outNotifier.notify(control: eEvalNotify.eEvalEnd(nil), vTime: vTime)
                        break
                    }

                    switch (itemStore.termination.error, error)
                    {
                        case (nil, nil):

                            outNotifier.notify(item: .eSuccess(testName), vTime: vTime)

                        case (nil, let selfError?):

                            outNotifier.notify(item: .eFailure(testName, "Expected no error on completion, got \"\(selfError)\""), vTime: vTime)

                        case (let itemStoreError?, nil):

                            outNotifier.notify(item: .eFailure(testName, "Expected \"\(itemStoreError)\" error on completion, got no error"), vTime: vTime)

                        case (let itemStoreError?, let selfError?):

                            if !itemStoreError.isEqual(other: selfError)
                            {
                                outNotifier.notify(item: .eFailure(testName, "Expected \"\(itemStoreError)\" error on completion, got \"\(selfError)\" error"), vTime: vTime)
                            }
                            else if remainingItemCount > 0
                            {
                                let missingItems = itemStore.items[storeIndex..<itemStore.itemCount]

                                // Extra match items not matched.
                                outNotifier.notify(item: .eFailure(testName, "Did not get all expected items, missing \(remainingItemCount) \(remainingItemCount == 1 ? "item" : "items"): \(missingItems)"), vTime: vTime)
                            }
                            else
                            {
                                outNotifier.notify(item: .eSuccess(testName), vTime: vTime)
                            }
                    }

                    outNotifier.notify(control: control, vTime: vTime)

                case eFabricNotify.eInstallTool:

                    outNotifier.notify(control: control, vTime: vTime)

                default:

                    outNotifier.notify(control: control, vTime: vTime)
            }
        }

        return matchOperator
    }
}

extension Rf.AElement where OutItem == Rf.eNamedResult /// Rf.eNamedResult Unit testing operators
{
    @discardableResult public func onProgress(_ progressFunc : @escaping (String, Float, Int?) -> Void) -> Rf.AOperator<OutItem, OutItem>
    {
        let onProgressOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.onProgress(progressFunc)

        compose(onProgressOperator)

        return onProgressOperator
    }

    @discardableResult public func onFailure(_ failureFunc : @escaping (String, String) -> Void) -> Rf.AOperator<OutItem, Rf.eNamedResult>
    {
        let onFailureOperator: Rf.AOperator<OutItem, OutItem> = Rf.Operators.onFailure(failureFunc)

        compose(onFailureOperator)

        return onFailureOperator
    }
}

extension Rf.AElement where OutItem : Equatable  // Item matching Unit testing support operators
{
    @discardableResult public func match(_ testName: String, _ store: Rf.Notifications.Store<OutItem>) -> Rf.AOperator<OutItem, Rf.eNamedResult>
    {
        let matchOperator = Rf.Operators.match(testName, store)

        compose(matchOperator)

        return matchOperator
    }

    @discardableResult public func matchNotifications<Item : Equatable>(_ testName: String, _ store: Rf.Notifications.Store<Rf.eTimestampedNotification<Item>>) -> Rf.AOperator<Rf.eTimestampedNotification<Item>, Rf.eNamedResult> where OutItem == Rf.eTimestampedNotification<Item>
    {
        let matchOperator = Rf.Operators.matchNotifications(testName, store)

        compose(matchOperator)

        return matchOperator
    }

    @discardableResult public func match(_ testName: String, _ items: ContiguousArray<OutItem>, _ error : IRfError? = nil) -> Rf.AOperator<OutItem, Rf.eNamedResult>
    {
        let store = Rf.Notifications.Store<OutItem>(Rf.TraceID("matchstore"), items, termination: Rf.eTermination.eCompletion(error))
        let matchOperator = Rf.Operators.match(testName, store)

        compose(matchOperator)

        return matchOperator
    }
}
