//
// Reactive Fabric Sample Code (Reactive Fabric Operators)
//
//  Created by Terry Stillone on 12/4/19.
//  Copyright © 2019 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import XCTest

@testable import ReactiveFabric

class SampleAsyncTests: RfTestCase
{
    override class func setUp()
    {
        testOutput("\n>> Async Operator Tests ==================================\n")

        super.setUp()
    }

    override func setUp()
    {
        super.setUp()
    }

    override func tearDown()
    {
        super.tearDown()
    }

    func testThrottle()
    {
        typealias TestSet = Rf.Test.TestSet
        typealias ItemType = Rf.eTimestampedNotification<Int>
        typealias StoreType = Rf.Notifications.Store<ItemType>

        let testRig  = createTestRig("testRig")
        let error    = Rf.eError("Unit Test Error")
        let runCount = RfSDK.Constant.AsyncUnitTestRunCount
        let traceId  = Rf.TraceID("testThrottle")

        // Run Tests with a delay cycle of 10msec
        let t        = TimeInterval(RfSDK.Constant.StandardTimeTick)

        // Tests with normal completion.
        let playStore_5Int           = StoreType(traceId.appending("store5"), [N(1, t), N(2, t * 2), N(3, t * 3), N(4, t * 4), N(5, t * 5), N(requestEndEval: nil, t * 6)], termination: .eNone)
        let playStore_1Int           = StoreType(traceId.appending("store1"), [N(1, t), N(requestEndEval: nil, t * 3)], termination: .eNone)
        let playStore_0Int           = StoreType(traceId.appending("store0"), [N(requestEndEval: nil)], termination: .eNone)
        // Tests with error completion.
        let playStore_5IntWithError  = StoreType(traceId.appending("store5"), [N(1, t), N(2, t * 2), N(3, t * 3), N(4, t * 4), N(5, t * 5), N(requestEndEval: error, t * 6)], termination: .eNone)
        let playStore_1IntWithError  = StoreType(traceId.appending("store1"), [N(1, t), N(requestEndEval: error, t * 2)], termination: .eNone)
        let playStore_0IntWithError  = StoreType(traceId.appending("store0"), [N(requestEndEval: error, t)], termination: .eNone)

        // Match with normal completion.
        let matchStore_5Int          = StoreType(traceId.appending("store5"), [N(1, t), N(2, t * 2), N(3, t * 3), N(4, t * 4), N(5, t * 5), N(notifyEvalEnd: nil, t * 6)], termination: .eNone)
        let matchStore_2Int          = StoreType(traceId.appending("store5"), [N(1, t), N(4, t * 4), N(notifyEvalEnd: nil, t * 6)], termination: .eNone)
        let matchStore_1Int          = StoreType(traceId.appending("store1"), [N(1, t), N(notifyEvalEnd: nil, t * 3)], termination: .eNone)
        let matchStore_0Int          = StoreType(traceId.appending("store0"), [N(notifyEvalEnd: nil)], termination: .eNone)
        // Match with error completion.
        let matchStore_1IntWithError = StoreType(traceId.appending("store1"), [N(1, t), N(notifyEvalEnd: error, t * 2)], termination: .eNone)
        let matchStore_3IntWithError = StoreType(traceId.appending("store5"), [N(1, t), N(3, t * 3), N(5, t * 5), N(notifyEvalEnd: error, t * 6)], termination: .eNone)
        let matchStore_0IntWithError = StoreType(traceId.appending("store0"), [N(notifyEvalEnd: error, t)], termination: .eNone)

        let tickSource = Rf.Sockets.TickSource(traceId.appending("tickSource"), vTimeEpoch: Date())

        let test5Items = TestSet("Throttle 5 Int", runCount: runCount, .eTestGroup([

           .eTestFabric("Throttle 5 Int For 3", .eSync(nil, WaitTimeout), { (testName, fabric) in

               fabric.emit(.eStore(playStore_5Int)).playNotifications().throttle(t * 2.9).matchNotifications(testName, matchStore_2Int).monitor(testName)
               fabric.install(tool: .eScheduler(["throttle", "playNotifications"], "scheduler", tickSource))
           }),

           .eTestFabric("Throttle 5 Int For 1", .eSync(nil, WaitTimeout), { (testName, fabric) in

               fabric.emit(.eStore(playStore_5Int)).playNotifications().throttle(t * 0.9).matchNotifications(testName, matchStore_5Int).monitor(testName)
               fabric.install(tool: .eScheduler(["throttle", "playNotifications"], "scheduler", tickSource))
           }),

           .eTestFabric("Throttle 5 Int With Error", .eSync(nil, WaitTimeout), { (testName, fabric) in

               fabric.emit(.eStore(playStore_5IntWithError)).playNotifications().throttle(t * 1.9).matchNotifications(testName, matchStore_3IntWithError).monitor(testName)
               fabric.install(tool: .eScheduler(["throttle", "playNotifications"], "scheduler", tickSource))
           })
        ]))

        let test1Items = TestSet("Throttle 1 Int", runCount: runCount, .eTestGroup([

           .eTestFabric("Throttle 1 Int", .eSync(nil, WaitTimeout), { (testName, fabric) in

               fabric.emit(.eStore(playStore_1Int)).playNotifications().throttle(t * 0.9).matchNotifications(testName, matchStore_1Int).monitor(testName)
               fabric.install(tool: .eScheduler(["throttle", "playNotifications"], "scheduler", tickSource))
           }),

           .eTestFabric("Throttle 1 Int With Error", .eSync(nil, WaitTimeout), { (testName, fabric) in

               fabric.emit(.eStore(playStore_1IntWithError)).playNotifications().throttle(t).matchNotifications(testName, matchStore_1IntWithError).monitor(testName)
               fabric.install(tool: .eScheduler(["throttle", "playNotifications"], "scheduler", tickSource))
           })
        ]))

        let test0Items = TestSet("Throttle 0 Int", runCount: runCount, .eTestGroup([

           .eTestFabric("Throttle 0 Int", .eSync(nil, WaitTimeout), { (testName, fabric) in

               fabric.emit(.eStore(playStore_0Int)).playNotifications().throttle(t).matchNotifications(testName, matchStore_0Int).monitor(testName)
               fabric.install(tool: .eScheduler(["throttle", "playNotifications"], "scheduler", tickSource))
           }),

           .eTestFabric("Throttle 0 Int With Error", .eSync(nil, WaitTimeout), { (testName, fabric) in

               fabric.emit(.eStore(playStore_0IntWithError)).playNotifications().throttle(t * 4).matchNotifications(testName, matchStore_0IntWithError).monitor(testName)
               fabric.install(tool: .eScheduler(["throttle", "playNotifications"], "scheduler", tickSource))
           })
        ]))

        testRig.syncNotify(item: .eRunTestSet(test5Items))
        testRig.syncNotify(item: .eRunTestSet(test1Items))
        testRig.syncNotify(item: .eRunTestSet(test0Items))
    }
}
