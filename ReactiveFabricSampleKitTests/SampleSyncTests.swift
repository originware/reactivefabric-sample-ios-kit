
//
// Reactive Fabric Sample Code (Reactive Fabric Operators)
//
//  Created by Terry Stillone on 12/4/19.
//  Copyright © 2019 Originware. All rights reserved.
//
// Licensed under Apache License v2.0
// See the accompanying License.txt file in the packaging of this file.
//

import XCTest
import ReactiveFabric

@testable import SampleiOSApp

class SampleSyncTests: RfTestCase {

    override class func setUp()
    {
        testOutput("\n>> Sync Operator Tests ==================================\n")
        
        super.setUp()
    }
    
    override func setUp()
    {
        super.setUp()
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
    
    func testTake()
    {
        let testRig         = createTestRig("testRig")
        let error           = Rf.eError("Unit Test Error")
        let runCount        = RfSDK.Constant.SyncUnitTestRunCount

        let test5Items = TestSet("Take long", runCount: runCount, .eTestGroup([
            
            .eTestFabric("Take long over reach", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArray([1, 2, 3, 4, 5])).take(7).match(testName, [1, 2, 3, 4, 5]).monitor(testName)
            }),
            
            .eTestFabric("Take long exact", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArray([1, 2, 3, 4, 5])).take(5).match(testName, [1, 2, 3, 4, 5]).monitor(testName)
            }),
            
            .eTestFabric("Take long under reach", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArray([1, 2, 3, 4, 5])).take(2).match(testName, [1, 2]).monitor(testName)
            }),
            
            .eTestFabric("Take long of 1", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArray([1, 2, 3, 4, 5])).take(1).match(testName, [1]).monitor(testName)
            }),
            
            .eTestFabric("Take long of 0", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArray([1, 2, 3, 4, 5])).take(0).match(testName, []).monitor(testName)
            }),
            
            .eTestFabric("Take long 5 with error", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArrayWithError([1, 2, 3, 4, 5], error)).take(5).match(testName, [1, 2, 3, 4, 5]).monitor(testName)
            }),
            
            .eTestFabric("Take long 1 with error", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArrayWithError([1, 2, 3, 4, 5], error)).take(2).match(testName, [1, 2]).monitor(testName)
            }),
            
            .eTestFabric("Take long 0 with error", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArrayWithError([1, 2, 3, 4, 5], error)).take(0).match(testName, []).monitor(testName)
            })
            ]))
        
        let test1Items = TestSet("Take short", runCount: runCount, .eTestGroup([
            
            .eTestFabric("Take short exact", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArray([1])).take(1).match(testName, [1]).monitor(testName)
            }),
            
            .eTestFabric("Take short 0", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArray([1])).take(0).match(testName, []).monitor(testName)
            }),
            
            .eTestFabric("Take short 2 with error", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(.eItemArrayWithError([1], error)).take(2).match(testName, [1], error).monitor(testName)
            })
            ]))
        
        let test0Items = TestSet("Take empty", runCount: runCount, .eTestGroup([
            
            .eTestFabric("Take empty over reach", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(Rf.eEmit<Int>.eEmpty).take(1).match(testName, []).monitor(testName)
            }),
            
            .eTestFabric("Take empty exact", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(Rf.eEmit<Int>.eEmpty).take(0).match(testName, []).monitor(testName)
            }),
            
            .eTestFabric("Take empty 1 with error", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(Rf.eEmit<Int>.eError(error)).take(1).match(testName, [], error).monitor(testName)
            }),
            
            .eTestFabric("Take empty 0 with error", .eSync(nil, WaitTimeout), { (testName, fabric) in
                fabric.emit(Rf.eEmit<Int>.eError(error)).take(0).match(testName, []).monitor(testName)
            }),
            ]))
        
        testRig.syncNotify(item: .eRunTestSet(test5Items))
        testRig.syncNotify(item: .eRunTestSet(test1Items))
        testRig.syncNotify(item: .eRunTestSet(test0Items))
    }
}
