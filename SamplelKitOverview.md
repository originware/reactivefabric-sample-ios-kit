 ---
# The [Originware](http://www.originware.com) Reactive Fabric SDK Overview Of The Evaluation Kit.

## 1. Intent of The Guide.

This is a guide is a brief to the key features of the **Reactive Fabric SDK** as demonstrated in this Evaluation Kit. It is advisable to read the [Reactive Fabric White Paper](Reactive Fabric/Doc/Reactive Fabric Whitepaper.pdf) in conjunction with this document.

Also see the included: [Reactive Fabric For Desktop and Mobile Product Application](Reactive Fabric/Doc/Reactive Fabric For Desktop and Mobile Product Application.pdf)

Please note this Source Code Project does not include the Reactive Fabric SDK framework binary and as such it will not compile. There is a video of the App in operation: [POI App Screencast](https://www.originware.com/video/RfSampleAppScreencast.mp4).
The SDK framework is available on request.


## 2. Reactive Fabric Core Concepts.

**Reactive Fabric** is a software technology that covers the scope of:
* The Software Design Process: A Collaborative and Iterative Software Design Process.
* The Visualisation and Documentation of Software Design: Designs are Visual. 
* Design and Implementation are coupled: Implementation and Design mirror each other. The Design elements are mapped to SDK classes.
* Includes an SDK which performs evaluation and supports tracing/instrumenting and debugging.  
* An asynchronous processing model that employs dispatch queues rather than direct thread manipulation.

**Reactive Fabric** formalises a design into a set of processing elements with a topology given by defined notification pathways between processing elements.

**Reactive Fabric** uses a pattern approach to operationally classify **Elements**. These patterns include:

* The **Fabric** pattern, manages the processing mesh.
* The **Source** pattern, emits item Notifications and composes with operators or collectors.
* The **Operator** pattern, operates on received Notifications by emitted it's own Notifications. It composes with operators or collectors.
* The **Collector** pattern, consumes received Notifications.

There are additional patterns for handling complex request-reply Notification interactions. A request-reply Notification is a scenario where a forward request (to a target Element) encapsulates a reply-back closure so that the target element can
reply back to the requester Element.

* The **Service** pattern: services request Notification by replying with reply Notifications. Client elements compose with it.
* The **Socket** pattern: pipelines complex request-reply processing using composition with other sockets or services.
* The **Subsystem** pattern: defines element hierarchy by delineating between macro Elements (the Subsystems) and sub-elements (within the subsystem). On the macro level the subsystem
can operate as either a service or socket.


## 3. Reactive Fabric Core Classes.

**Element Patterns** are mapped to corresponding abstract classes:

* **Rf.AFabric**:  The Fabric class that emits control Notifications.
* **Rf.ASource\<OutItem>**:  A Source class which emits item Notifications of type **OutItem** (and control Notifications).
* **Rf.AOperator\<InItem, OutItem>**: An Operator class which receives **InItem** item Notifications and emits **OutItem** item Notifications (as well as control Notifications).
* **Rf.ACollector\<InItem>**:  A collector class which consumes item Notifications of type **InItem** (and control Notifications).
* **Rf.Service\<Request>**: A Service class which services requests of type **Request**.
* **Rf.Socket\<InRequest, OutRequest>**: A Socket class which pipelines incoming **InRequest** and emitting **OutRequest**.
* **Rf.Subsystem\<InRequest, OutRequest>**: A Subsystem class which receives **InRequest** and emits **OutRequest**.

All these classes inherit from the base abstract **Element** class:

* **Rf.AElement\<InItem, OutItem>** 

Other support classes include:

* **Rf.ANotifier\<Item>**: A light weight notifier of item Notifications of type **Item**.
* **Rf.Eval.Queue**: A Dispatch Queue that provides processing, threading and synchronisation. 

## 3. A Sample Operator Element:


```
	
extension Rf.Operators
{
	public static func take<Item>(while predicate: @escaping (Int, Item, Rf.VTime?) -> Bool) -> Rf.AOperator<Item, Item>
	{
		typealias eEvalNotify = Rf.EvalScope.eNotify

		let traceID = Rf.TraceID("takeWhile")
		let takeWhileOperator: Rf.AOperator<Item, Item> = Rf.Operators.onNotify(traceID, { (inputNotification, outputNotifier, requestNotifier) in

			switch inputNotification
			{
				case .eItem(let (index, item, vTime)):             // On an Item notification.

					if predicate(index, item, vTime)
					{
						// Propagate the item notification.
						outputNotifier.notify(item: item, vTime: vTime)
					}
					else
					{
						// Request an end of evaluation.
						requestNotifier.raiseEndEval(nil, vTime: vTime)
					}

				case .eEvalControl(let (control, vTime)):       // On any control notification.

					// Propagate the event.
					outputNotifier.notify(control: control, vTime: vTime)

				case .eFabricTool:                              // On a tool availability event.

					// No tools required.
					break
			}
		})

		return takeWhileOperator
	}
 }   
```


## 4. The Reactive Fabric Design and Development Process.

The **Reactive Fabric Design Methodology** is a structured approach, an initial bare **concept** visual design is first authored. Those designs, once accepted are
 matured (over time) with more detail to the milestone of the **first iteration design**. The **first iteration implementation** is constructed from that design. More iterative cycles are applied to evolve the design, incorporate more functionality, form solutions to problems and simplify complexities. 
These cycles feedback on the design itself, as insights are followed and as possibilities surface from the big picture that emerges from the visual design and implementation experience. 

A key principle of the methodology is that *"design mirrors implementation and implementation mirrors design"*. So implementation and design are kept in sync throughout the development evolutionary lifecycle. 

## 5. Reactive Fabric Implementation-Development Benefits.

* The scope to visualise in the design space, brings in aspects of group and extra-group collaboration. It empowers individuals to comprehend and explore possibilities. 
* The Design process promotes simpler, more elegant design and associated code with greater code-consistency. Code becomes more definitional (i.e the what is done) as opposed to the how it is performed.   
* The unified asynchronous processing model underlying **Reactive Fabric** dismisses much of the brain sheer involved in co-threading complex interactions.
* Having an implementation backed by a design model brings consistency and transparency to development process. 
* By its inherent nature, **Reactive Fabric** lends itself naturally to monitoring, testing and simulation, through the use of fabric Notification injection and extraction. 
Whether unit testing individual design sections or realtime active system testing, the fabric is a very convenient testing medium. The **SDK** additional supports **Notification** tracing, instrumenting of **Elements** and **Fabric** consistency checking.

## 6. Contact Details:

Please direct questions, comments and feedback to [Terry Stillone](mailto:terry@originware.com) at Originware.com
