
// IRf.swift
// Reactive Fabric SDK
//
// Created by Terry Stillone on 24/4/18.
// Copyright (c) 2018 Originware. All rights reserved.
//

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The Reactive Fabric Interfaces (Protocols).
//


/// Rf Fabric and Evaluation Control Notification Type
public protocol IRfControl : CustomStringConvertible, CustomDebugStringConvertible
{
}

/// Rf Fabric and Evaluation Control Request Type
public protocol IRfRequest : IRfControl
{
}

/// Identifiable entities, for monitoring and tracing purposes.
public protocol IRfIdentifiable: CustomStringConvertible, CustomDebugStringConvertible
{
    /// The tracing ID of the identity
    var traceID : Rf.TraceID            { get }

    /// The element type of the identity.
    var elementType : Rf.eElementType   { get }
}

extension IRfIdentifiable
{
    /// CustomStringConvertible conformance.
    public var description : String         { return "\(traceID)(\(elementType))" }

    /// CustomStringConvertible conformance.
    public var debugDescription : String    { return "\(traceID)(\(elementType))" }
}

/// The Notification notifier capability. Notifies item and control notifications.
public protocol IRfNotifier: IRfIdentifiable
{
    /// The item notification data type.
    associatedtype InItem

    /// Issue an item notification to the notifier.
    /// - Parameter item: The item to be issued.
    /// - Parameter vTime: The virtual time.
    func notify(item: InItem, vTime: Rf.VTime?)

    /// Issue a control notification to the notifier.
    /// - Parameter control: The control to be issued.
    /// - Parameter vTime: The virtual time.
    func notify(control: IRfControl, vTime: Rf.VTime?)
}

/// A (public) Wrapper class or struct that acts as a container for an RF.Alement.
public protocol IRfPublicElementWrapper
{
    associatedtype InItem
    associatedtype OutItem

    /// The element hosted by the wrapper.
    var element : Rf.AElement<InItem, OutItem>      { get }

    /// The input consumer for the element.
    var consumer : Rf.AConsumer<InItem>             { get }

    /// The output notifier of the element.
    var producer : Rf.ANotifier<OutItem>            { get }
}

extension IRfPublicElementWrapper
{
    /// The input consumer for the element.
    public var consumer : Rf.AConsumer<InItem>     { return element.consumer }

    /// The output notifier of the element.
    public var producer : Rf.ANotifier<OutItem>    { return element.producer }

    /// Compose the self consumer with a given consumer.
    /// - Parameter withWrappedElement: The wrapper element to compose with.
    /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
    @discardableResult public func compose<WrappedElement>(_ withWrappedElement: WrappedElement, isBuffered: Bool = false) -> WrappedElement where WrappedElement : IRfPublicElementWrapper, WrappedElement.InItem == OutItem
    {
        element.compose(withWrappedElement)

        return withWrappedElement
    }
    
    /// Compose the self consumer with a given consumer.
    /// - Parameter withElement: The element to compose with.
    /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
    @discardableResult public func compose<TargetItem>(_ withElement: Rf.AElement<OutItem, TargetItem>, isBuffered: Bool = false) -> Rf.AElement<OutItem, TargetItem>
    {
        element.compose(withElement)

        return withElement
    }

    /// Compose the self consumer with a given consumer.
    /// - Parameter withConsumer: The consumer to compose with.
    /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
    public func compose(withConsumer: Rf.AConsumer<OutItem>, isBuffered: Bool = false)
    {
        element.compose(withConsumer: withConsumer, isBuffered: isBuffered)
    }
    
    /// Compose the )self element with a given notifier.
    /// - Parameter withNotifier: The notifier to compose with.
    /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
    public func compose(withNotifier: Rf.ANotifier<OutItem>, isBuffered: Bool = false)
    {
        element.compose(withNotifier: withNotifier, isBuffered: isBuffered)
    }

    /// Decompose from Consumer.
    /// - Parameter fromConsumer: The consumer to decompose from.
    public func decompose(fromConsumer: Rf.AConsumer<OutItem>)
    {
        element.decompose(fromConsumer: fromConsumer)
    }

    /// Decompose from Notifier.
    /// - Parameter fromNotifier: The notifier to decompose from.
    public func decompose(fromNotifier: Rf.ANotifier<OutItem>)
    {
        element.decompose(fromNotifier: fromNotifier)
    }

    /// Decompose from Notifier.
    /// - Parameter fromElementUri: The uri of the notifier to decompose from.
    public func decompose(fromElementUri: String)
    {
        element.decompose(fromElementUri: fromElementUri)
    }

    /// Decompose from all.
    public func decomposeAll()
    {
        element.decomposeAll()
        consumer.decomposeAll()
        producer.decomposeAll()
    }

    /// Called to begin evaluation. Notifies the upstream fabric supervisor to begin evaluation.
    /// - Parameter evalType: The evaluation queue type.
    /// - Parameter vTime: The virtual time.
    public func beginEval(_ evalType: Rf.EvalScope.eEvalType = .eAsync(nil), vTime: Rf.VTime? = nil)
    {
        element.beginEval(evalType, vTime: vTime)
    }

    /// Called to end evaluation. Notifies the upstream fabric supervisor to end evaluation.
    /// - Parameter error: The optional error associated with evaluation completion.
    /// - Parameter vTime: The virtual time.
    public func endEval(_ error : IRfError? = nil, vTime: Rf.VTime?)
    {
        element.endEval(error, vTime: vTime)
    }

    /// Called to restart evaluation. Notifies the upstream fabric supervisor to restart evaluation.
    /// - Parameter evalType: The evaluation queue type to be used when restarting.
    /// - Parameter vTime: The virtual time.
    public func restartEval(_ evalType: Rf.EvalScope.eEvalType, vTime: Rf.VTime?)
    {
        element.restartEval(evalType, vTime: vTime)
    }

    /// Trace the fabric.
    /// - Parameter scope: The scope of the instrumenting for the trace.
    @discardableResult public func trace(scope: Rf.eInstrumentScope = .eAll(nil), tracer: IRfTracer = RfSDK.Tracers.stdout) -> Self
    {
        element.trace(scope: scope, tracer: tracer)

        return self
    }

    /// Instrument the fabric.
    /// - Parameter scope: The scope of the instrumenting for the trace.
    @discardableResult public func instrument(scope: Rf.eInstrumentScope) -> Self
    {
        element.instrument(scope: scope)

        return self
    }

    /// Set the tracer for the fabric.
    /// - Parameter tracer: The tracer to be set.
    @discardableResult public func set(tracer: Rf.eTracer) -> Self
    {
        element.set(tracer: tracer)

        return self
    }

    /// Install a tool
    /// - Parameter tool: The tool to install.
    @discardableResult public func install(tool: Rf.FabricScope.eTool) -> Self
    {
        element.install(tool: tool)

        return self
    }

    /// Set an instrument.
    /// - Parameter instrument: The instrument to install.
    @discardableResult public func install(instrument: Rf.FabricScope.eInstrument)  -> Bool
    {
        return element.install(instrument: instrument)
    }

    /// Wait for an event and timeout if a timeout value is given.
    /// - Parameter for: The event type to wait for.
    /// - Parameter waitTimeout: The timeout for the wait as a TimeInterval in seconds, if null do not timeout.
    /// - Returns: An indicator of whether the method timed out.
    @discardableResult public func wait(for event: Rf.eWaitEvent, waitTimeout : TimeInterval? = nil) -> DispatchTimeoutResult
    {
        return element.wait(for: event, waitTimeout: waitTimeout)
    }
}

/// Rf SDK Error Type
public protocol IRfEquatable
{
    func isEqual(other : IRfEquatable) -> Bool
}

/// Rf SDK Error Type
public protocol IRfError: Error, IRfEquatable, CustomStringConvertible
{
}

/// The Composition capability of the IRfElement.
public protocol IRfComposable
{
    associatedtype OutItem

    /// Compose the self element with a given notifier.
    /// - Parameter withNotifier: The notifier to compose with.
    /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
    func compose(withConsumer:  Rf.AConsumer<OutItem>, isBuffered: Bool)

    /// Compose the self element with a given notifier.
    /// - Parameter withNotifier: The notifier to compose with.
    /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
    func compose(withNotifier: Rf.ANotifier<OutItem>, isBuffered: Bool)

    /// Decompose from notifier.
    /// - Parameter fromNotifier: The notifier to decompose from.
    func decompose(fromNotifier: Rf.ANotifier<OutItem>)

    /// Decompose from element.
    /// - Parameter fromElementUri: The element uri to decompose from.
    func decompose(fromElementUri: String)

    /// Decompose from all.
    func decomposeAll()
}

/// Element instrumentation capability.
public protocol IRfInstrumentable
{
    /// Set fabric consistency control request checking.
    @discardableResult func consistencyCheck(_ traceID : Rf.TraceID?) -> Self

    /// Trace the fabric.
    /// - Parameter scope: The scope of the instrumenting for the trace.
    @discardableResult func trace(scope: Rf.eInstrumentScope, tracer: IRfTracer) -> Self

    /// Instrument the fabric.
    /// - Parameter scope: The scope of the instrumenting for the trace.
    @discardableResult func instrument(scope: Rf.eInstrumentScope) -> Self

    /// Set the tracer for the fabric.
    /// - Parameter tracer: The tracer to be set.
    @discardableResult func set(tracer: Rf.eTracer) -> Self
}

/// Element tooling capability.
public protocol IRfToolable: class, IRfIdentifiable
{
    var tools : [String : Any]? { get set }
}

extension IRfToolable
{
    // Get the scheduler from the tools.
    public var scheduler : Rf.Sockets.AScheduler? {

        guard let tools = tools else { return nil }

        return tools["scheduler"] as? Rf.Sockets.AScheduler
    }

    /// Create the scheduler tool from the scheduler in the tools.
    /// - Parameter traceID: The trace ID of the tool.
    /// - Parameter checkIsValid: check whether the scheduler is available.
    public func getScheduleTool(_ traceID : Rf.TraceID, checkIsValid : Bool = true) -> Rf.Tools.Schedule?
    {
        guard let scheduler = scheduler else
        {
            if checkIsValid { RfSDK.assertionFailure("\(description): Scheduler not available ") }
            
            return nil
        }

        return Rf.Tools.Schedule(traceID, scheduler: scheduler)
    }
    
    @discardableResult public func set(tool: Rf.FabricScope.eTool) -> Bool
    {
        func setTool(name: String, tool: Any)
        {
            if tools == nil
            {
                tools = [String : Any]()
            }

            tools?[name] = tool
        }

        switch tool
        {
            case .eScheduler(let (traceIdURIs, name, scheduler)):

                if traceIdURIs == nil
                {
                    setTool(name: name, tool: scheduler)
                }
                else if let traceIdNames = traceIdURIs, traceIdNames.contains(where: { traceID.uri.hasSuffix($0) })
                {
                    setTool(name: name, tool: scheduler)
                }
                else
                {
                    return false
                }

            case .eCustom(let (traceIdURIs, name, customTool)):

                if traceIdURIs == nil
                {
                    setTool(name: name, tool: customTool)
                }
                else if let traceIdNames = traceIdURIs, traceIdNames.contains(where: { traceID.uri.hasSuffix($0) })
                {
                    setTool(name: name, tool: customTool)
                }
                else
                {
                    return false
                }
        }

        return true
    }

    public func removeAllTools()
    {
        tools = nil
    }
}

/// Element tooling capability.
public protocol IRfScheduling : class
{
    var scheduleTool : Rf.Tools.Schedule? { get set }
}

/// A facility for tracing operation through textual messaging.
public protocol IRfTracer
{
    /// Tracing enabler.
    var enabled : Bool { get set }
    
    /// Output a message
    /// - Parameter message: The trace message.
    /// - Parameter formatter: The content formatter of the message.
    func output(_ message : String)
}

/// A facility supporting tracing, logging and instrumenting.
public protocol IRfTracing : class, IRfIdentifiable
{
    typealias FormatterFunc = (IRfTracer, IRfIdentifiable, Int, String, Rf.VTime?) -> String

    var logger :      IRfTracer?                 { get set }
    var tracer:       IRfTracer?                 { get set }
    var instrumenter: IRfTracer?                 { get set }

    var loggerFormatter : FormatterFunc?         { get set }
    var tracerFormatter : FormatterFunc?         { get set }
    var instrumenterFormatter : FormatterFunc?   { get set }

    var identity : IRfIdentifiable               { get set }
    var indent : Int                             { get set }
}

extension IRfTracing
{
    /// Convenience Output a trace message.
    /// - Parameter tracer: The tracer to send the message to.
    /// - Parameter message: The trace message.
    /// - Parameter vTime: The Virtual Timestamp of the message.
    public func output(toTracer tracer: IRfTracer, message: String, vTime: Rf.VTime?)
    {
        let vTimeTimestamp: String = vTime != nil ? String(format: "[%.3f] ", vTime!): "[ --- ] "
        let padding                = String(repeating: " ", count : indent)

        tracer.output("\(vTimeTimestamp)\(padding)\(identity.description):".padding(toLength: RfSDK.Constant.TracerIndent + 4, withPad: " ", startingAt: 0) + padding + message)
    }

    /// Convenience Output a log message.
    /// - Parameter message: The log message.
    /// - Parameter vTime: The Virtual Timestamp of the message.
    public func outputLog(message: String, vTime: Rf.VTime?)
    {
        guard let logger = logger else { return }

        if let formatter = loggerFormatter
        {
            logger.output(formatter(logger, identity, indent, message, vTime))
        }
        else
        {
            output(toTracer: logger, message: message, vTime: vTime)
        }
    }

    /// Convenience Output a trace message.
    /// - Parameter message: The trace message.
    /// - Parameter vTime: The Virtual Timestamp of the message.
    public func outputTrace(message: String, vTime: Rf.VTime?)
    {
        guard let tracer = tracer else { return }

        if let formatter = tracerFormatter
        {
            tracer.output(formatter(tracer, identity, indent, message, vTime))
        }
        else
        {
            output(toTracer: tracer, message: message, vTime: vTime)
        }
    }

    /// Convenience Output an instrument message.
    /// - Parameter message: The instrument message.
    /// - Parameter vTime: The Virtual Timestamp of the message.
    public func outputInstrument(message: String, vTime: Rf.VTime?)
    {
        guard let instrumenter = instrumenter else { return }

        if let formatter = instrumenterFormatter
        {
            instrumenter.output(formatter(instrumenter, identity, indent, message, vTime))
        }
        else
        {
            output(toTracer: instrumenter, message: message, vTime: vTime)
        }
    }
}


/// Class Instance tracking capability managed through the IRfFactory.
public protocol IRfInstanceTracker : class
{
    var currentInstances: Set<Rf.TraceID> { get set }

    var enabled: Bool { get set }
}

/// Instance Tracking capability managed through the IRfFactory.
internal protocol IRfInstanceTrackable
{
    /// Track a traceID.
    func track(_ traceID: @autoclosure () -> Rf.TraceID)

    /// Untrack a trace ID.
    func untrack(_ traceID: @autoclosure () -> Rf.TraceID)
}

extension IRfInstanceTrackable where Self: IRfIdentifiable
{
    func track(_ traceID: @autoclosure () -> Rf.TraceID)
    {
        guard let instanceTracker = traceID().instanceTracker, instanceTracker.enabled else { return }

        instanceTracker.currentInstances.insert(traceID())
    }

    /// Untrack a trace ID.
    func untrack(_ traceID: @autoclosure () -> Rf.TraceID)
    {
        guard let instanceTracker = traceID().instanceTracker, instanceTracker.enabled else { return }

        instanceTracker.currentInstances.remove(traceID())
    }
}

/// The Reactive Fabric Factory to create Elements.
/// Note: The RfSDK static factory property stores the actual Factory instance.
public protocol IRfFactory
{
    var tracker : IRfInstanceTracker { get }

    func Fabric(_ traceID : Rf.TraceID) -> Rf.AFabric
    func SubSystem<InItem, OutItem>(_ traceID : Rf.TraceID) -> Rf.ASubSystem<InItem, OutItem>
    func Service<Request>(_ traceID : Rf.TraceID) -> Rf.AService<Request>
    func Socket<InItem, OutItem>(_ traceID : Rf.TraceID) -> Rf.ASocket<InItem, OutItem>
    func Operator<InItem, OutItem>(_ traceID : Rf.TraceID) -> Rf.AOperator<InItem, OutItem>
    func Source<OutItem>(_ traceID : Rf.TraceID) -> Rf.ASource<OutItem>
    func Collector<OutItem>(_ traceID : Rf.TraceID) -> Rf.ACollector<OutItem>
}

// Rf Schedulable Item conformance.
public protocol IRfScheduledItem
{
    var schedulerStartTime: Date?         { get }
    var scheduledOffset :   TimeInterval  { get }
    var triggerTime:        Date?         { get }

    mutating func setSchedulerStartTime(_ time: Date)
    mutating func setTriggerTime(_ time: Date)
}

extension IRfScheduledItem
{
    /// Set the scheduler start time
    public mutating func schedulerStart(time: Date = Date())
    {
        setSchedulerStartTime(time)
    }

    /// Set the trigger time.
    public mutating func trigger(time: Date = Date())
    {
        setTriggerTime(time)
    }

    /// Set the scheduler start and trigger time.
    public mutating func set(schedulerStartTime: Date, triggerTime: Date)
    {
        setSchedulerStartTime(schedulerStartTime)
        setTriggerTime(triggerTime)
    }
}
