
// RfSDK.swift
// Reactive Fabric SDK
//
// Created by Terry Stillone on 27/2/18.
// Copyright (c) 2018 Originware. All rights reserved.
//

import Foundation


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// The RfSDK.swift source file defines the RfSDK class which manages the Reactive Fabric SDK.
//

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The RfSDK Namespace/Class configures the Reactive Fabric SDK.
///
/// - The class manages tracing, logging and instrumentation handling.
/// - Includes the Reactive Fabric factory.

public class RfSDK
{
    /// The Reactive Factory Element factory for the SDK.
    ///
    /// Note: Elements can be created in a number of ways:
    /// - They can be subclassed from their abstract element base classes.
    /// - They can be created by a factory (if desired).
    /// - They can be wrapped by a container struct or class.
    ///
    public static var factory: IRfFactory = { return createFactory() }()

    /// SDK Constants.
    public struct Constant
    {
        /// The base of all DispatchQueue labels.
        public static let DispatchQueueStem = StaticString(stringLiteral: "com.originware")

        /// The standard scheduling tick time (used by Unit Tests).
        public static let StandardTimeTick = TimeInterval(0.01)

        /// The standard scheduling tick time leeway error threshold (used by Unit Tests).
        public static let TimerLeewayThreshold = TimeInterval(0.005)

        /// The standard scheduling tick time leeway error threshold (used by Unit Tests).
        public static let TimeDifferenceThreshold = TimeInterval(0.001)

        /// The repeat run count for Sync Unit Tests.
        public static let SyncUnitTestRunCount = 10

        /// The repeat run count for Async Unit Tests.
        public static let AsyncUnitTestRunCount = 1

        /// The padding level for tracers.
        public static let TracerIndent = 120

        /// The default sync begin eval wait timeout in seconds.
        public static let DefaultWaitTimeout = TimeInterval(5)

        /// The SDK not implemented message.
        public static let NotImplemented = StaticString(stringLiteral: "Not implemented")
    }

    /// The standard SDK Tracers.
    public struct Tracers
    {
        /// The standard output tracer
        public static let stdout = StandardOutputTracer()
        public static let stderr = StandardErrorTracer()
    }

    /// The standard SDK Evaluation Queues.
    public struct EvalQueues
    {
        public static let Timer = Rf.Eval.Queue(Rf.TraceID("Timer"), queueType: .eSerial)
        public static let TimerWorker = Rf.Eval.Queue(Rf.TraceID("TimerWorker"), queueType: .eConcurrent)
        public static let UI = Rf.Eval.Queue(Rf.TraceID("UI"), queueType: .eUIThread)
    }

}

extension RfSDK  // Standard Tracers.
{
    /// The tracer that outputs to standard output (console).
    open class StandardOutputTracer : IRfTracer
    {
        private var m_loggerDispatchQueue = DispatchQueue(label: RfSDK.Constant.DispatchQueueStem.description + ".standardOutput")

        /// Enabler.
        public var enabled = true

        // Indent level
        public var indent = 4

        public func output(_ message: String)
        {
            m_loggerDispatchQueue.async(execute: { [weak self] in

                if let strongSelf = self, strongSelf.enabled, let data = (message + "\n").data(using: String.Encoding.utf8)
                {
                    FileHandle.standardOutput.write(data)
                }
            })
        }
    }

    /// The tracer that outputs to standard error (console).
    open class StandardErrorTracer : IRfTracer
    {
        private var m_loggerDispatchQueue = DispatchQueue(label: RfSDK.Constant.DispatchQueueStem.description + ".standardError")

        /// Enabler.
        public var  enabled = true

        // Indent level
        public var  indent  = 4

        public func output(_ message: String)
        {
            m_loggerDispatchQueue.async(execute: { [weak self] in
                
                if let strongSelf = self, strongSelf.enabled, let data = (message + "\n").data(using: String.Encoding.utf8)
                {
                    FileHandle.standardError.write(data)
                }
            })
        }
    }
}

extension RfSDK  // Assertion handling.
{
    /// The SDK RfSDK.assertionFailure method, called on minor processing inconsistencies.
    public static func assertionFailure(_ message: @autoclosure () -> String = "", file: StaticString = #file, line: UInt = #line)
    {
#if !NDEBUG
        let text = message()

        print(text)
        assertionFailureAction(text, file, line)
#endif
    }
    
    /// The SDK RfSDK.systemFailure method, called on major processing errors.

    public static func systemFailure(_ message: @autoclosure () -> String = "", file: StaticString = #file, line: UInt = #line) -> Never
    {
#if !NDEBUG
        let text = message()

        print(text)
        assertionSystemAction(text, file, line)
#endif
    }
    
    /// The SDK customisable RfSDK.assertionFailure action. Can be customised for unit testing.
    public static var defaultAssertionFailureAction : (String, StaticString, UInt) -> Void = { (message: String, file: StaticString, line: UInt) in

        assert(false, message, file: file, line: line)
    }

    public static var assertionFailureAction : (String, StaticString, UInt) -> Void = defaultAssertionFailureAction

    /// The SDK customisable RfSDK.systemFailure action. Can be customised for unit testing.
    public static var defaultSystemFailureAction : (String, StaticString, UInt) -> Never = { (message: String, file: StaticString, line: UInt) in

        fatalError(message, file: file, line: line)
    }

    public static var assertionSystemAction : (String, StaticString, UInt) -> Never = defaultSystemFailureAction
}


