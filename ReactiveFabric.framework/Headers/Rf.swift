
// Rf.swift
// Reactive Fabric SDK
//
// Created by Terry Stillone on 16/2/18.
// Copyright (c) 2018 Originware. All rights reserved
//

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Rf.swift source file contains the basic types used by the Reactive Fabric SDK.
///

import Foundation

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Declarative Reactive Fabric Namespace.
///
/// - All Reactive Fabric definitions reside in this namespace.
/// - Also see the RfSDK Class/Namespace for SDK management.
///

public struct Rf
{
    /// Virtual Time is an offset from a defined epoch.
    public typealias VTime = TimeInterval

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  Rf.TraceID identifies Reactive Fabric instances. This includes Elements, Notifiers and EvalQueues.
    ///
    ///    - Identification stamping includes a (String) URI and an unique (Int) ID.
    ///    - Employed in element identification during evaluation, operational tracing and evaluation.
    ///

    public struct TraceID : CustomStringConvertible, CustomDebugStringConvertible, Hashable
    {
        public static var IndentUnit   = 4

        /// The URL Separator.
        public static let URISeparator = "/"

        /// The generator of TraceIDs.
        public struct TraceIDGenerator
        {
            /// The next (unallocated) Trace ID.
            private static var m_nextID = 0

            fileprivate static var nextID : Int
            {
                let currentId = m_nextID

                m_nextID += 1;

                return currentId
            }
        }

        /// The assigned unique (Int) trace Id.
        public let traceIntID: Int

        /// The Trace (String) URI.
        public let uri: String

        /// The computed trace name (the last component of the URI)
        public var name: String
        {
            if let lastComponent = uri.components(separatedBy: Rf.TraceID.URISeparator).last
            {
                return lastComponent
            }
            else
            {
                return uri
            }
        }

        /// The Rf class instance tracker.
        public private(set) weak var instanceTracker : IRfInstanceTracker? = nil

        /// Indicator is whether instance tracking is enabled.
        public var isTracking : Bool            { return instanceTracker != nil }

        /// Hashable conformance.
        public var hashValue : Int              { return traceIntID }

        /// CustomStringConvertible conformance.
        public var description : String         { return uri }

        /// CustomDebugStringConvertible conformance.
        public var debugDescription : String    { return uri }

        /// Initialise with URI.
        /// - Parameter uri: The TraceID URI.
        public init(_ uri: String)
        {
            self.uri = uri
            self.traceIntID = TraceIDGenerator.nextID
        }

        /// Initialise with URI.
        /// - Parameter uri: The TraceID URI.
        private init(_ uri: String, traceIntID: Int, tracking: IRfInstanceTracker)
        {
            self.uri = uri
            self.traceIntID = traceIntID
            self.instanceTracker = tracking
        }

        /// Return a new sub URI by appending the subPart to the existing URI.
        /// - Parameter subPart: The sub-component of the new URI.
        /// - Returns: The new URI with appended sub-component.
        public func appending(_ subComponent: String) -> Rf.TraceID
        {
            return Rf.TraceID(uri + Rf.TraceID.URISeparator + subComponent)
        }

        public func set(tracker: IRfInstanceTracker?) -> Rf.TraceID
        {
            guard let tracker = tracker else { return self }

            return Rf.TraceID(uri, traceIntID: traceIntID, tracking: tracker)
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Rf Element identification, includes the Rf.TraceID and Rf.eElementType of the element.
    ///
    /// - Employed for identification in tracing, logging and instrumenting.
    ///

    open class Identity : IRfIdentifiable
    {
        /// The trace ID of the instance.
        public let traceID : Rf.TraceID

        /// The element type.
        public internal(set) var elementType : Rf.eElementType

        /// Initialise with traceID and element type.
        /// - Parameter traceID: The traceID for the object.
        /// - Parameter elementType: The element type of the object.
        public init(_ traceID : Rf.TraceID, elementType: Rf.eElementType)
        {
            self.traceID = traceID
            self.elementType = elementType
        }

        /// Initialise from an identity.
        /// - Parameter identity: The identity for the object.
        public init(_ identity: IRfIdentifiable)
        {
            self.traceID = identity.traceID
            self.elementType = identity.elementType
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// Rf.eError, the error type.
    ///

    public enum eError : IRfError, Equatable
    {
        case eGeneral(String)
        case eEvalEndRequest(IRfError?)
        case eException(Error)
        case eCustom(Any)

        public init(_ description: String)
        {
            self = .eGeneral(description)
        }

        public var description : String
        {
            switch self
            {
                case .eGeneral(let description):            return description
                case .eEvalEndRequest(let error?):          return "eEvalEnd(\(error))"
                case .eEvalEndRequest(nil):                 return "eEvalEnd"
                case .eException(let error):                return "eException(\(String(describing: error)))"
                case .eCustom(let any):                     return "eCustom(\(String(describing: any)))"
            }
        }

        public func isEqual(other : IRfEquatable) -> Bool
        {
            guard let error = other as? eError else { return false }

            switch (self, error)
            {
                case (.eGeneral(let selfDescription), .eGeneral(let otherDescription)):

                    return selfDescription == otherDescription

                case (.eEvalEndRequest(let selfError), .eEvalEndRequest(let otherError)):

                    switch (selfError, otherError)
                    {
                        case (nil, nil):                            return true
                        case (let selfValue?, let otherValue?):     return selfValue.isEqual(other: otherValue)
                        default:                                    return false
                    }

                case (.eException(let selfException as IRfEquatable), .eException(let otherException as IRfEquatable)):

                    return selfException.isEqual(other: otherException)

                case (.eCustom(let selfCustom as IRfEquatable), .eCustom(let customOther as IRfEquatable)):

                    return selfCustom.isEqual(other: customOther)

                default:

                    return false
            }
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    //
    // Rf Notification definitions.
    //

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The non-timestamped Notification type, includes Item and Control notification.
    ///
    /// - Employed for synchronous notifications that are not time based.
    ///
    
    public enum eNotification<Item> : CustomStringConvertible
    {
        /// Item notification event.
        case eItem(Item)

        /// Fabric control notification event.
        case eControl(IRfControl)

        /// Indicate if this notification is an item notification.
        func isItem() -> Bool
        {
            if case .eItem = self { return true }

            return false
        }

        public var description : String {

            switch self
            {
                case .eItem(let item):              return String(describing: item)
                case .eControl(let control):        return String(describing: control)
            }
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The timestamped Notification type, includes Item and Control notification.
    ///
    public enum eTimestampedNotification<Item> : CustomStringConvertible
    {
        /// Item notification event with virtual time.
        case eItem(Item, Rf.VTime?)

        /// Fabric control notification event with virtual time.
        case eControl(IRfControl, Rf.VTime?)

        /// Indicate if this notification is an item notification.
        func isItem() -> Bool
        {
            if case .eItem = self { return true }

            return false
        }

        /// The item value of the notification.
        public var item : Item?
        {
            switch self
            {
                case .eItem(let (value, _)):    return value
                case .eControl:                 return nil
            }
        }

        /// The time offset of the notification.
        public var timeOffset : Rf.VTime?
        {
            switch self
            {
                case .eItem(let (_, vTime)):      return vTime
                case .eControl(let (_, vTime)):   return vTime
            }
        }

        /// CustomStringConvertible conformance.
        public var description : String {

            switch self
            {
                case .eItem(let (item, vTime)):         return "(\(item), \(String(describing: vTime)))"
                case .eControl(let (control, vTime)):   return "(\(control), \(String(describing: vTime))"
            }
        }

        /// Get the same eTimestampedNotification with a given timestamp.
        public func withTimeOffset(_ timeOffset : Rf.VTime?) -> eTimestampedNotification<Item>
        {
            switch self
            {
                case .eItem(let (item, _)):         return .eItem(item, timeOffset)
                case .eControl(let (control, _)):   return .eControl(control, timeOffset)
            }
        }
    }
   
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Rf Pattern Namespaces
    ///

    /// The namespace for Pattern implementations.
    public struct Patterns {}

    /// The namespace for Fabric implementations.
    public struct Fabrics {}

    /// The namespace for SubSystem implementations.
    public struct SubSystems {}

    /// The namespace for Service implementations.
    public struct Services {}

    /// The namespace for Source implementations.
    public struct Sources {}

    // The namespace for Collector implementations.
    public struct Collectors {}

    /// The namespace for Socket implementations.
    public struct Sockets {}

    /// The namespace for Operator implementations.
    public struct Operators {}

    /// The namespace for Notification implementations.
    public struct Notifications {}

    /// The namespace for Enumerator implementations.
    public struct Enumerators {}

    /// The namespace for Notifier implementations.
    public struct Notifiers {}

    /// The Eval Queue implementation.
    public struct Eval {}
    
    /// The support tools.
    public struct Tools {}

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// AConsumer<InItem>: the element abstract class that consumes item and control notifications.
    ///
    /// - InItem denotes the data type of the input item notification.
    ///

    open class AConsumer<InItem> : Rf.Notifiers.DelegateTarget<InItem>
    {
        public typealias eEvalRequestReply = Rf.EvalScope.eRequestResult
        
        /// The delegate for handling upstream requests.
        public var onRequest :         (IRfRequest, Rf.VTime?) -> eEvalRequestReply

        /// The count of inputs being received by the consumer.
        public var inputCount = Int(0)

        /// Lazy requestor for the consumer.
        public var requestor :         Rf.Notifiers.Requestor
        {
            guard let requestorVar = m_requestor else
            {
                let replacementRequest = Rf.Notifiers.Requestor(onRequestWithReply: onRequest)

                m_requestor = replacementRequest

                return replacementRequest
            }

            return requestorVar
        }

        /// Lazy requestor for the consumer.
        public var requestorWithReply: Rf.Notifiers.RequestorWithReply
        {
            guard let requestorVar = m_requestorWithReply else
            {
                let replacementRequest = Rf.Notifiers.RequestorWithReply(onRequest: onRequest)

                m_requestorWithReply = replacementRequest

                return replacementRequest
            }

            return requestorVar
        }

        /// The backing var for the requestor property.
        private var m_requestor: Rf.Notifiers.Requestor? = nil

        /// The backing var for the requestorWithReply property.
        private var m_requestorWithReply: Rf.Notifiers.RequestorWithReply? = nil

        /// Initialise with trace ID and element type.
        /// - Parameter traceID: The trace ID of the notifier.
        /// - Parameter elementType: The element type.
        public override init(_ traceID: Rf.TraceID, elementType: Rf.eElementType)
        {
            self.onRequest = { (_, _) in return Rf.EvalScope.eRequestResult.eNone }
            
            super.init(traceID, elementType: elementType)
        }

        /// Initialise with trace ID and element type.
        /// - Parameter traceID: The trace ID of the notifier.
        /// - Parameter onItem: The onItem delegate.
        /// - Parameter onControl: The onControl delegate.
        /// - Parameter onRequest: The onRequest delegate.
        public init(_ traceID: Rf.TraceID,
                                onItem: @escaping (InItem, Rf.VTime?) -> Void,
                                onControl : @escaping (IRfControl, Rf.VTime?) -> Void,
                                onRequest: @escaping  (IRfRequest, Rf.VTime?) -> Rf.EvalScope.eRequestResult)
        {
            self.onRequest = onRequest

            super.init(traceID, onItem: onItem, onControl: onControl)
        }

        @discardableResult public func notify(request: IRfRequest, vTime: Rf.VTime?) -> eEvalRequestReply
        {
            return onRequest(request, vTime)
        }

        /// Request the end of evaluation.
        /// - Parameter error: Indicator of whether the termination condition has an error.
        /// - Parameter tag: The tag of the element that is requesting the end of eval.
        /// - Parameter vTime: The virtual time.
        public func requestEvalEnd(_ error: IRfError? = nil, tag: String? = nil, vTime: Rf.VTime?)
        {
            let _ = onRequest(Rf.EvalScope.eRequest.eRequestEvalEnd(error, tag), vTime)
        }

        /// Add a parent requestor to this consumer.
        /// - Parameter requestNotifier: The requestor to add.
        open func addParentRequester(requestNotifier: Rf.Notifiers.ConsumerRequestorWithReply<InItem>)
        {
            inputCount += 1
        }

        /// Remove a parent requestor to this consumer.
        /// - Parameter requestNotifier: The requestor to remove.
        open func removeParentRequester(requestNotifier: Rf.Notifiers.ConsumerRequestorWithReply<InItem>)
        {
            inputCount -= 1
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// AElement<InItem, OutItem>: the abstract element class and the Reactive Fabric core processing element.
    ///
    /// The Element receives, processes and then emits item and control notifications.
    /// - InItem denotes the data type of the input item notification.
    /// - OutItem denotes the data type of the output item notification.
    ///
    /// Note: Elements can be created in a number of ways:
    /// - They can be subclassed from their abstract element base classes.
    /// - They can be created by a factory (if desired).
    /// - They can be wrapped by a container struct or class.
    ///

    open class AElement<InItem, OutItem> : Rf.AConsumer<InItem>, IRfToolable
    {
        /// The element consumer.
        public var consumer : Rf.AConsumer<InItem>                       { fatalError("\(self): consumer is abstract") }

        /// The element producer.
        public var producer : Rf.ANotifier<OutItem>                      { fatalError("\(self): producer is abstract") }

        /// Get the element producer in the form of a Source.
        public var producerAsSource: Rf.ASource<OutItem>                 { fatalError("\(self): producerAsSource is abstract") }

        /// The evaluation queue used in the evaluation. Set by the fabric beginEval control notification.
        public var evalQueue : Rf.Eval.Queue?                            { fatalError("\(self): evalQueue is abstract") }

        /// Indicator of whether the element can run an evaluation.
        public var canEvaluate: Bool                                     { fatalError("\(self): canEvaluate is abstract") }

        /// The state of the element evaluation expression.
        public var state :           Rf.eState                           { fatalError("\(self): state is abstract") }

        /// The tracing (logging) accessor.
        public var tracing:          IRfTracing                          { fatalError("\(self): tracer is abstract") }

        public var requestNotifier:  Rf.Notifiers.ConsumerRequestorWithReply<OutItem>   { fatalError("\(self): requestNotifier2 is abstract") }

        /// The composition mode of the expression.
        public var compositionMode : Rf.eCompositionMode
        {
            get         { fatalError("\(self): compositionMode is abstract") }
            set(value)  { fatalError("\(self): compositionMode is abstract") }
        }

        /// The termination mode of the element.
        public var terminationMode:  Rf.eTerminationMode
        {
            get         { fatalError("\(self): terminationMode is abstract") }
            set(value)  { fatalError("\(self): terminationMode is abstract") }
        }

        /// IRfToolable conformance.
        public var tools : [String : Any]?
        {
            get         { fatalError("\(self): tools is abstract") }
            set(value)  { fatalError("\(self): tools is abstract") }
        }

        public override init(_ traceID : Rf.TraceID, elementType: Rf.eElementType)
        {
            super.init(traceID, elementType: elementType)
        }

        /// Compose the self consumer with a given consumer.
        /// - Parameter withElement: The element to compose with.
        /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
        @discardableResult public func compose<TargetItem>(_ withElement: Rf.AElement<OutItem, TargetItem>, isBuffered: Bool = false) -> Rf.AElement<OutItem, TargetItem>
        {
            fatalError("\(self): compose() is abstract")
        }

        /// Compose the self consumer with a given consumer.
        /// - Parameter withWrappedElement: The element to compose with.
        /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
        @discardableResult public func compose<WrappedElement>(_ withWrappedElement: WrappedElement, isBuffered: Bool = false) -> WrappedElement where WrappedElement : IRfPublicElementWrapper, WrappedElement.InItem == OutItem
        {
            fatalError("\(self): compose() is abstract")
        }

        /// Compose the self consumer with a given consumer.
        /// - Parameter withConsumer: The consumer to compose with.
        /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
        public func compose(withConsumer: Rf.AConsumer<OutItem>, isBuffered: Bool = false)
        {
            fatalError("\(self): compose() is abstract")
        }

        /// Compose the self element with a given notifier.
        /// - Parameter withNotifier: The notifier to compose with.
        /// - Parameter isBuffered: Indicator whether the composition should be buffered for item notifications.
        public func compose(withNotifier: Rf.ANotifier<OutItem>, isBuffered: Bool = false)
        {
            fatalError("\(self): compose() is abstract")
        }

        /// Decompose from Consumer.
        /// - Parameter fromConsumer: The consumer to decompose from.
        public func decompose(fromConsumer: Rf.AConsumer<OutItem>)
        {
            fatalError("\(self): decompose() is abstract")
        }

        /// Decompose from Notifier.
        /// - Parameter fromNotifier: The notifier to decompose from.
        public func decompose(fromNotifier: Rf.ANotifier<OutItem>)
        {
            fatalError("\(self): decompose() is abstract")
        }

        /// Decompose from Notifier.
        /// - Parameter fromElementUri: The uri of the notifier to decompose from.
        public func decompose(fromElementUri: String)
        {
            fatalError("\(self): decompose() is abstract")
        }

        /// Decompose from all.
        public override func decomposeAll()
        {
            fatalError("\(self): decomposeAll() is abstract")
        }

        /// Called to begin evaluation. Notifies the upstream fabric supervisor to begin evaluation.
        /// - Parameter evalType: The evaluation queue type.
        /// - Parameter vTime: The virtual time.
        open func beginEval(_ evalType: Rf.EvalScope.eEvalType = .eAsync(nil), vTime: Rf.VTime? = nil)
        {
            fatalError("\(self): beginEval() is abstract")
        }

        /// Called to end evaluation. Notifies the upstream fabric supervisor to end evaluation.
        /// - Parameter error: The optional error associated with evaluation completion.
        /// - Parameter tag: The identifying tag for the request.
        /// - Parameter vTime: The virtual time.
        open func endEval(_ error : IRfError? = nil, tag: String? = nil, vTime: Rf.VTime?)
        {
            fatalError("\(self): endEval() is abstract")
        }

        /// Called to restart evaluation. Notifies the upstream fabric supervisor to restart evaluation.
        /// - Parameter evalType: The evaluation queue type to be used when restarting.
        /// - Parameter vTime: The virtual time.
        open func restartEval(_ evalType: Rf.EvalScope.eEvalType, vTime: Rf.VTime?)
        {
            fatalError("\(self): restartEval() is abstract")
        }

        /// Trace the fabric.
        /// - Parameter scope: The scope of the instrumenting for the trace.
        @discardableResult open func trace(scope: Rf.eInstrumentScope = .eAll(nil), tracer: IRfTracer = RfSDK.Tracers.stdout) -> Self
        {
            fatalError("\(self): trace() is abstract")
        }

        /// Instrument the fabric.
        /// - Parameter scope: The scope of the instrumenting for the trace.
        @discardableResult open func instrument(scope: Rf.eInstrumentScope) -> Self
        {
            fatalError("\(self): instrument() is abstract")
        }

        /// Set the tracer for the fabric.
        /// - Parameter tracer: The tracer to be set.
        @discardableResult open func set(tracer: Rf.eTracer) -> Self
        {
            fatalError("\(self): set() is abstract")
        }

        /// Install a tool
        /// - Parameter tool: The tool to install.
        @discardableResult open func install(tool: Rf.FabricScope.eTool) -> Self
        {
            fatalError("\(self): install() is abstract")
        }

        /// Set an instrument.
        /// - Parameter instrument: The instrument to install.
        @discardableResult open func install(instrument: Rf.FabricScope.eInstrument)  -> Bool
        {
            fatalError("\(self): install() is abstract")
        }

        /// Wait for an event and timeout if a timeout value is given.
        /// - Parameter for: The event type to wait for.
        /// - Parameter waitTimeout: The timeout for the wait as a TimeInterval in seconds, if null do not timeout.
        /// - Returns: An indicator of whether the method timed out.
        @discardableResult open func wait(for event: Rf.eWaitEvent, waitTimeout : TimeInterval? = nil) -> DispatchTimeoutResult
        {
            fatalError("\(self): wait() is abstract")
        }

        /// Set fabric consistency control request checking.
        @discardableResult open func consistencyCheck(_ traceID : Rf.TraceID? = nil) -> Self
        {
            fatalError("\(self): consistencyCheck() is abstract")
        }
    }
}


extension Rf
{

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Element Factory is either Native or Emulated. The Native factory is for full performance production target,
    ///     whereas the Emulated factory is an obfuscated, demonstration, emulating non-production target. The Emulation version
    ///     may be given out for evaluation purposes.
    ///

#if NativeFactory

    public typealias FactoryElement<InItem, OutItem> = Rf.NativeElement<InItem, OutItem>

#elseif EmulatedFactory

    public typealias FactoryElement<InItem, OutItem> = Rf.EmulatedElement<InItem, OutItem>

#endif
    
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The abstract classes for Element patterns.
    ///

    /// The abstract Fabric Element base class.
    open class AFabric: FactoryElement<Void, Void>
    {
        public init(_ traceID : Rf.TraceID)
        {
            super.init(traceID, elementType: .eFabric("Fabric"))
        }

        // Fabric version of observe(_:control). This is so sources can be composed with this observe.
        // - Parameter traceID: The traceID of the operator.
        // - Parameter onControlFunc: The call back to send control notifications to.
        @discardableResult public func observe(_ traceID: Rf.TraceID = Rf.TraceID("observeControl"), control onControlFunc: @escaping (IRfControl, Rf.VTime?) -> Void) -> Self
        {
            consumer.onControl = { [weak self] (control, vTime) in

                guard let strongSelf = self else { return }

                strongSelf.producer.notify(control: control, vTime: vTime)

                onControlFunc(control, vTime)
            }

            return self
        }

        // Fabric evaluation completion observer.
        // - Parameter onCompleted: The completion function to run at expression eval end.
        @discardableResult public func onEvalEnd(_ onCompleted: @escaping () -> Void) -> Self
        {
            consumer.onControl = { [weak self] (control, vTime) in

                guard let strongSelf = self else { return }

                switch control
                {
                    case eEvalNotify.eEvalEnd:

                        strongSelf.producer.notify(control: control, vTime: vTime)

                        onCompleted()

                    default:

                        strongSelf.producer.notify(control: control, vTime: vTime)
                }
            }

            return self
        }
    }
    
    /// The Abstract SubSystem Element base class.
    open class ASubSystem<InItem, OutItem>: FactoryElement<InItem, OutItem>
    {
        /// The socket connector that connects input consumers to the subsystem.
        open var inputConnector:  Rf.ASocket<InItem, InItem>      { fatalError("\(self): inputConnector() is abstract") }

        /// The socket connector that connects output producers to the subsystem.
        open var outputConnector: Rf.ASocket<OutItem, OutItem>    { fatalError("\(self): outputConnector() is abstract") }

        public init(_ traceID : Rf.TraceID)
        {
            super.init(traceID, elementType: .eService("SubSystem<\(String(reflecting: InItem.self)), \(String(reflecting: OutItem.self))>"))
        }

        /// Add a consumer/producer pair to the subsystem processing chain.
        /// - Parameter consumer: The consumer of the processing chain.
        /// - Parameter producer: The producer of the processing chain.
        open func interconnect<OtherItem>(consumer: Rf.AElement<InItem, OtherItem>, producer: Rf.ANotifier<OutItem>)
        {
            fatalError("\(self): interconnect() is abstract")     
        }

        /// Add an element to the subsystem processing chain.
        /// - Parameter element: The element to add to the processing chain.
        open func interconnect(element: Rf.AElement<InItem, OutItem>)
        {
            fatalError("\(self): interconnect() is abstract")
        }
    }

    /// The Abstract Source Element base class.
    open class ASource<Item>: FactoryElement<Void, Item>
    {
        public init(_ traceID : Rf.TraceID)
        {
            super.init(traceID, elementType: .eSource("Source<\(String(reflecting: Item.self))>"))
        }
    }

    /// The Abstract Service Element base class.
    open class AService<Request>: FactoryElement<Request, Void>
    {
        public init(_ traceID : Rf.TraceID)
        {
            super.init(traceID, elementType: .eService("Service<\(String(reflecting: Request.self))>"))
        }
    }

    /// The Abstract Socket Element base class.
    open class ASocket<InItem, OutItem>: FactoryElement<InItem, OutItem>
    {
        public init(_ traceID : Rf.TraceID)
        {
            super.init(traceID, elementType: .eSocket("Socket<\(String(reflecting: InItem.self)), \(String(reflecting: OutItem.self))>"))
        }
    }

    /// The Abstract Operator Element base class.
    open class AOperator<InItem, OutItem>: FactoryElement<InItem, OutItem>
    {
        public init(_ traceID : Rf.TraceID)
        {
            super.init(traceID, elementType: .eOperator("Operator<\(String(reflecting: InItem.self)), \(String(reflecting: OutItem.self))>"))
        }
    }

    /// The Abstract Collector Element base class.
    open class ACollector<Item>: FactoryElement<Item, Void>
    {
        public init(_ traceID : Rf.TraceID)
        {
            super.init(traceID, elementType: .eCollector("Collector<\(String(reflecting: Item.self))>"))
        }
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// The Reactive Fabric operational (evaluation) definitions.
///
extension Rf
{
    /// The Reactive Fabric operational scopes.
    public enum eFabricScopes : Int
    {
        case eFabricScope = 0
        case eEvalScope   = 1
    }

    /// The Fabric state.
    public enum eState
    {
        case eFabricConstructed
        case eFabricReady
        case eEvalBegun
        case eEvaluating
        case eEvalEnded
        case eFabricEnded
        case eFabricDeconstructed

        /// Indicator that the evaluation has begun.
        public var hasBegunEval: Bool
        {
            switch self
            {
                case .eFabricConstructed, .eFabricReady, .eEvalBegun:   return false
                default:                                                return true
            }
        }

        /// Indicator that the evaluation is currently running.
        public var isEvaluating : Bool
        {
            switch self
            {
                case .eFabricConstructed, .eFabricReady, .eFabricEnded, .eEvalEnded:    return false
                default:                                                                return true
            }
        }

        /// Indicator that the evaluation has ended.
        public var hasEndedEval: Bool
        {
            switch self
            {
                case .eFabricEnded, .eFabricDeconstructed:  return true
                default:                                    return false
            }
        }
    }

    /// The Fabric operational scope.
    public struct FabricScope
    {
        /// The Fabric scope notifications.
        public enum eNotify: IRfControl, Equatable, CustomStringConvertible, CustomDebugStringConvertible
        {
            case eFabricBegin
            case eInstallTool(Rf.FabricScope.eTool)
            case eInstrument(Rf.FabricScope.eInstrument)
            case eFabricEnd(IRfError?, String?)

            public var scope : Int { return eFabricScopes.eFabricScope.rawValue }

            public var description : String
            {
                switch self
                {
                    case .eFabricBegin:                 return "eFabricBegin"
                    case .eInstallTool(let tool):       return "eInstallTool(\(tool))"
                    case .eInstrument(let instrument):  return "eInstrument(\(instrument))"
                    case .eFabricEnd:                   return "eFabricEnd"
                }
            }

            public var debugDescription : String
            {
                return description
            }
        }

        /// Tools that can be installed into Element.
        public enum eTool: Equatable, CustomStringConvertible, CustomDebugStringConvertible
        {
            case eScheduler(ContiguousArray<String>?, String, Rf.Sockets.AScheduler)
            case eCustom(ContiguousArray<String>?, String, Any)

            public var description : String
            {
                switch self
                {
                    case .eScheduler(let (matchElements, name, scheduler)):

                        if let matchElements = matchElements
                        {
                            return "eScheduler(\(matchElements), \"\(name)\", \(scheduler))"
                        }
                        else
                        {
                            return "eScheduler(All Elements, \"\(name)\", \(scheduler))"
                        }

                    case .eCustom(let (matchElements, name, custom)):

                        if let matchElements = matchElements
                        {
                            return "eCustom(\(matchElements), \"\(name)\", \(custom))"
                        }
                        else
                        {
                            return "eCustom(All Elements, \"\(name)\", \(custom))"
                        }
                }
            }

            public var debugDescription : String
            {
                return description
            }
        }

        /// Instrumenting to be installed into Element.
        public enum eInstrument: Equatable
        {
            case eTrace(Rf.eInstrumentScope)
            case eMonitor(Rf.eInstrumentScope)
            case eCheckConsistency(Rf.TraceID?)
            case eSetTracer(Rf.eTracer)

            public var description : String
            {
                switch self
                {
                    case .eTrace(let instrument):           return "eTrace(\(instrument))"
                    case .eMonitor(let instrument):         return "eMonitor(\(instrument))"
                    case .eCheckConsistency(let traceID):   return "eCheckConsistency(\(String(describing: traceID))"
                    case .eSetTracer(let traceID):          return "eSetTracer(\(traceID))"
                }
            }
        }
    }

    /// The Evaluation operational scope.
    public struct EvalScope
    {
        /// The Evaluation scope notifications.
        public enum eNotify: IRfControl, Equatable, CustomStringConvertible, CustomDebugStringConvertible
        {
            case eEvalBegin(eEvalType)
            case eEvalRestart(eEvalType)
            case ePause
            case eResume
            case eEvalEnd(IRfError?)

            public var scope : Int { return eFabricScopes.eEvalScope.rawValue }

            public var description : String
            {
                switch self
                {
                    case .eEvalBegin(let evalType):     return "eEvalBegin(\(evalType))"
                    case .eEvalRestart(let evalType):   return "eEvalRestart(\(evalType))"
                    case .ePause:                       return "ePause"
                    case .eResume:                      return "eResume"
                    case .eEvalEnd(let error):          return error != nil ? "eEvalEnd(\(error!)))" : "eEvalEnd"
                }
            }

            public var debugDescription : String
            {
                return description
            }
        }

        /// An evaluation management request.
        public enum eRequest: IRfRequest, Equatable, CustomStringConvertible
        {
            case eRequestEvalBegin(eEvalType)
            case eRequestEvalRestart(eEvalType)
            case eRequestEvalEnd(IRfError?, String?)
            case eRequestFabricEnd(IRfError?, String?)
            case eRequestInstrument(Rf.FabricScope.eInstrument)
            case eRequestInstallTool(Rf.FabricScope.eTool)
            case eRequestRecomposeWithProducer
            case eRequestWait(Rf.eWaitEvent, TimeInterval?)

            public var description : String
            {
                func describe(_ name: String, _ error: IRfError?, _ tag: String?) -> String
                {
                    switch (error, tag)
                    {
                        case (nil, nil):                        return name
                        case (nil, let (tagValue?)):            return "\(name): \(String(describing: tagValue))"
                        case (let (errorValue?), nil):          return "\(name)(\(String(describing: errorValue)))"
                        case (let (errorValue?, tagValue?)):    return "\(name)(\(String(describing: errorValue))): \(tagValue)"
                    }
                }

                switch self
                {
                    case .eRequestEvalBegin(let evalType):      return "eRequestEvalBegin(\(evalType))"
                    case .eRequestEvalRestart(let evalType):    return "eRequestEvalRestart(\(evalType))"
                    case .eRequestEvalEnd(let (error, tag)):    return describe("eRequestEvalEnd", error, tag)
                    case .eRequestFabricEnd(let (error, tag)):  return describe("eRequestFabricEnd", error, tag)
                    case .eRequestInstrument(let instrument):   return "eRequestInstrument(\(instrument))"
                    case .eRequestInstallTool(let tool):        return "eRequestInstallTool(\(tool))"
                    case .eRequestRecomposeWithProducer:        return "eRequestRecomposeWithProducer"
                    case .eRequestWait(let (event, timeout)):   return "eRequestWait(\(event), \(String(describing: timeout))"
                }
            }

            public var debugDescription : String
            {
                return description
            }
        }

        /// The result of an evaluation management request.
        public enum eRequestResult : Equatable, CustomStringConvertible
        {
            case eNone
            case eWait(DispatchTimeoutResult)

            public var description : String
            {
                switch self
                {
                    case .eNone:                        return "eNone"
                    case .eWait(let timeoutResult):     return "eWait(\(timeoutResult))"
                }
            }
        }

        /// The Evaluation scope state change events.
        public enum eInstrumentationEvent<Item> : CustomStringConvertible
        {
            case eEvalBegin(eEvalType)
            case eItem(Item)
            case eEvalEnd(IRfError?)

            public var description : String
            {
                switch self
                {
                    case .eEvalBegin(let evalType):     return "eEvalBegin(\(evalType))"
                    case .eItem(let item):              return "eItem(\(item))"
                    case .eEvalEnd(let error):          return error != nil ? "eEvalEnd(\(error!))" : "eEvalEnd"
                }
            }
        }

        /// The Evaluation mechanism types.
        public enum eEvalType : CustomStringConvertible, Equatable
        {
            /// The asynchronous evaluation with optional evaluation queue.
            case eAsync(Rf.Eval.Queue?)

            /// The synchronous evaluation with optional evaluation queue.
            case eSync(Rf.Eval.Queue?, TimeInterval?)

            /// The inline thread evaluation.
            case eInline(Rf.Eval.Queue?)

            public func asEvalExecType(_ traceId: Rf.TraceID) -> eEvalType
            {
                switch self
                {
                    case .eAsync(let evalQueue):

                        return .eAsync(evalQueue)

                    case .eSync(let (evalQueue, waitTimeout)):

                        return .eSync(evalQueue, waitTimeout)

                    case .eInline:

                        return .eInline(nil)
                }
            }

            /// The eval queue for the eEvalType.
            public var evalQueue : Rf.Eval.Queue?
            {
                switch self
                {
                    case .eAsync(let evalQueue):        return evalQueue
                    case .eSync(let (evalQueue, _)):    return evalQueue
                    case .eInline(let evalQueue):       return evalQueue
                }
            }

            // CustomStringConvertible conformance.
            public var description : String
            {
                switch self
                {
                    case .eAsync:    return "eAsync"
                    case .eSync:     return "eSync"
                    case .eInline:   return "eInline"
                }
            }
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Tracing and Instrumentation definitions.
///
extension Rf
{
    /// The tracer to emit tracing or instrumenting information.
    public enum eTracerType: String
    {
        case eLog        = "Log"
        case eTrace      = "Trace"
        case eInstrument = "Instrument"
    }

    /// The tracer role.
    public enum eTracer: Equatable
    {
        case eLog(IRfTracer?, Int)
        case eTrace(IRfTracer?, Int)
        case eInstrument(IRfTracer?, Int)

        public mutating func set(indent : Int)
        {
            switch self
            {
                case .eLog(let (tracer, _)):            self = .eLog(tracer, indent)
                case .eTrace(let (tracer, _)):          self = .eTrace(tracer, indent)
                case .eInstrument(let (tracer, _)):     self = .eInstrument(tracer, indent)
            }
        }
    }

    /// The instrumentation scope.
    public enum eInstrumentScope: Equatable
    {
        case eAll(Rf.TraceID?)
        case eLifeCycle(Rf.TraceID?)
        case eNotifyItem(Rf.TraceID?)
        case eNotifyControl(Rf.TraceID?)
    }

    /// The instrumentation scope options.
    struct oInstrumentOptions: OptionSet
    {
        let rawValue: Int

        static let oLifeCycle     = oInstrumentOptions(rawValue: 1 << 0)
        static let oNotifyItem    = oInstrumentOptions(rawValue: 1 << 1)
        static let oNotifyControl = oInstrumentOptions(rawValue: 1 << 2)

        static let all: oInstrumentOptions = [.oLifeCycle, .oNotifyItem, .oNotifyControl]
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Rf Element Pattern extensions.
///

extension Rf // Pattern types
{
    /// The Reactive Fabric SDK element types.
    public enum eElementType : CustomStringConvertible
    {
        /// The fabric element type manages elements associated with the fabric and the evaluation process.
        case eFabric(String)

        /// The System element manages a collection of fabrics. Useful for modularisation of a large application.
        case eSubSystem(String)

        /// The Service element acts a provider of some service.
        case eService(String)

        /// The Socket element is a hybrid source/operator element, acting as a source but also capable of receiving.
        case eSocket(String)

        /// The Source element participates in the evaluation by emitting notifications.
        case eSource(String)

        /// The Collector element participates in the evaluation by consuming notifications.
        case eCollector(String)

        /// The Operator element participates in the evaluation by receiving and emitting notifications.
        case eOperator(String)

        /// The Control Notifier notifies notifiable targets.
        case eControlNotifier(String)

        /// The State Supervisor manages evaluation state.
        case eStateSupervisor(String)

        /// The Notifier notifies notifiable targets.
        case eNotifier(String)

        /// The Evalqueue executes operations.
        case eEvalQueue(String)

        /// The Scheduler Tool schedules operations.
        case eScheduleTool(String)

        /// The Enumerator enumerates over notifications.
        case eEnumeration(String)

        /// CustomStringConvertible conformance.
        public var description : String
        {
            switch self
            {
                case .eSubSystem(let name):         return name
                case .eFabric(let name):            return name
                case .eService(let name):           return name
                case .eSocket(let name):            return name
                case .eSource(let name):            return name
                case .eCollector(let name):         return name
                case .eOperator(let name):          return name
                case .eControlNotifier(let name):   return name
                case .eStateSupervisor(let name):   return name
                case .eNotifier(let name):          return name
                case .eEvalQueue(let name):         return name
                case .eScheduleTool(let name):      return name
                case .eEnumeration(let name):       return name
            }
        }

        /// The element indent level for tracing and logging.
        public var signatureIndent : Int
        {
            switch self
            {
                case .eFabric:                      return 0
                case .eSubSystem:                   return 5
                case .eService:                     return 3
                case .eSocket:                      return 5
                case .eSource:                      return 3
                case .eCollector:                   return 7
                case .eOperator:                    return 5
                case .eControlNotifier:             return 1
                case .eStateSupervisor:             return 1
                case .eNotifier:                    return 6
                case .eEvalQueue:                   return 1
                case .eScheduleTool:                return 1
                case .eEnumeration:                 return 6
            }
        }
    }

    /// The modes for evaluation termination.
    public enum eTerminationMode
    {
        /// Keep the Source or fabric running.
        case eKeepRunning

        /// Terminate evaluation on any sub-element evaluation end.
        case eTerminateOnAnyChildTerminations

        /// Terminate evaluation on all sub-element evaluation end.
        case eTerminateWhenAllChildrenTerminated
    }

    /// The fabric modes.
    public enum eCompositionMode
    {
        /// Composition can be performed while the fabric is begin evaluated.
        ///  - This mode has more overhead than eStatic.
        case eDynamic

        /// Composition can only be performed before fabric evaluation begins.
        ///  - This mode is more performant.
        case eStatic
    }

    /// Composition operations.
    public enum eCompose<OutItem>
    {
        case eComposeConsumer(Rf.AConsumer<OutItem>)
        case eComposeNotifier(Rf.ANotifier<OutItem>)
        case eDecomposeElementUri(String)
        case eDecomposeAll
    }

    /// Wait for event types.
    public enum eWaitEvent
    {
        case eForEvalBegin
        case eForEvalEnd
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Rf Notifier definitions.
//

extension Rf
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The abstract Notifier base class.
    ///

    open class ANotifier<InItem> : Rf.Identity, IRfNotifier
    {
        /// Indicator that the notifier is enabled and notifying item notifications.
        var isActive : Bool = true

        /// The count of target outputs of the notifier.
        open var targetCount: Int
        {
            return 0
        }

        /// The count of active output targets of the notifier.
        open var isActiveCount: Int
        {
            return 0
        }

        /// Initialise with trace TD.
        /// - Parameter traceID: The trace ID of the notifier.
        /// - Parameter elementType: The element type of the notifier.
        public override init(_ traceID : Rf.TraceID, elementType: Rf.eElementType = .eNotifier("Notifier<\(InItem.self)>"))
        {
            super.init(traceID, elementType: elementType)
        }

        /// Initialise with identity.
        /// - Parameter identity: The identity of the notifier.
        public init(_ identity : Rf.Identity)
        {
            super.init(identity.traceID, elementType: identity.elementType)
        }

        /// Notify an item notification.
        /// - Parameter item: The item notification.
        /// - Parameter vTime: The virtual time.
        public func notify(item: InItem, vTime: Rf.VTime?)
        {
            // Do nothing by default.
        }

        /// Notify a control notification.
        /// - Parameter control: The control notification.
        /// - Parameter vTime: The virtual time.
        public func notify(control: IRfControl, vTime: Rf.VTime?)
        {
            // Do nothing by default.
        }

        /// Remove All targets
        public func decomposeAll()
        {
            // To be overridden
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Rf Notifier and Requestor implementations.
//

extension Rf.Notifiers
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Delegate target Notifier.
    ///

    open class DelegateTarget<InItem> : Rf.ANotifier<InItem>
    {
        /// Indicator that the onItem delegate can be overwritten.
        public private(set) var onItemWasSet    = false

        /// Indicator that the onControl delegate can be overwritten.
        public private(set) var onControlWasSet = false

        /// Indicator that the onTargetReadyForNotify delegate can be overwritten.
        public private(set) var onTargetReadyForNotifyWasSet = false

        /// Indicator that the onItem and onControl delegates can be overwritten.
        public var delegatesAreOverwiteable: Bool { return onItemWasSet || onControlWasSet }

        /// The count of output targets.
        open override var targetCount: Int
        {
            return (onItemWasSet || onControlWasSet) ? 1 : 0
        }

        /// The count of active output targets.
        open override var isActiveCount: Int
        {
            return (onItemWasSet || onControlWasSet) && isActive ? 1 : 0
        }

        /// The target item notification delegate.
        public var onItem : (InItem, Rf.VTime?) -> Void            = { (_, _) in }
        {
            didSet { onItemWasSet = true }
        }

        /// The target control notification delegate.
        public var onControl: (IRfControl, Rf.VTime?) -> Void      = { (_, _) in }
        {
            didSet { onControlWasSet = true }
        }

        /// The target ready for notifications delegate.
        public var onTargetReadyForNotify: (Rf.VTime?) -> Void     = { (_) in }
        {
            didSet { onTargetReadyForNotifyWasSet = true }
        }

        /// Initialise with trace ID and element type.
        /// - Parameter traceID: The trace ID of the notifier.
        /// - Parameter elementType: The object element type.
        public override init(_ traceID: Rf.TraceID, elementType: Rf.eElementType = .eNotifier("DelegateTarget<\(InItem.self)>"))
        {
            super.init(traceID, elementType: elementType)
        }

        /// Initialise with trace ID and delegates.
        public init(_ traceID: Rf.TraceID,
                    onItem: @escaping (InItem, Rf.VTime?) -> Void,
                    onControl: @escaping (IRfControl, Rf.VTime?) -> Void,
                    elementType: Rf.eElementType = .eNotifier("DelegateTarget<\(InItem.self)>"))
        {
            self.onItem = onItem
            self.onControl = onControl

            super.init(traceID, elementType: elementType)
        }

        /// Notify receipt of an item notification.
        /// - Parameter item: The item notification received.
        /// - Parameter vTime: The virtual time.
        open override func notify(item: InItem, vTime: Rf.VTime?)
        {
            onItem(item, vTime)
        }

        /// Notify receipt of a control notification.
        /// - Parameter control: The control notification received.
        /// - Parameter vTime: The virtual time.
        open override func notify(control: IRfControl, vTime: Rf.VTime?)
        {
            onControl(control, vTime)
        }

        /// Remove all delegates.
        public override func decomposeAll()
        {
            onItem = { (_, _) in }
            onControl = { (_, _) in }

            onItemWasSet = false
            onControlWasSet = false
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Control Request notifier.
    ///

    open class Requestor
    {
        public typealias eEvalRequestReply = Rf.EvalScope.eRequestResult

        /// The onRequest delegate.
        public var onRequest : (IRfRequest, Rf.VTime?) -> Void

        /// Initialise with no onRequest delegate.
        public init()
        {
            self.onRequest = { (_, _) in }
        }

        /// Initialise with onRequest delegate.
        /// - Parameter: onRequest: the onRequest delegate to use in the requestor.
        public init(onRequest : @escaping (IRfRequest, Rf.VTime?) -> Void)
        {
            self.onRequest = onRequest
        }

        /// Initialise with onRequestWithReply delegate.
        /// - Parameter: onRequestWithReply: the onRequest delegate to use in the requestor.
        public init(onRequestWithReply : @escaping (IRfRequest, Rf.VTime?) -> eEvalRequestReply)
        {
            self.onRequest = { (request, vTime) in

                let _ = onRequestWithReply(request, vTime)
            }
        }

        /// The request notify method.
        public func notify(request: IRfRequest, vTime: Rf.VTime?)
        {
            onRequest(request, vTime)
        }

        /// Request the end of evaluation.
        /// - Parameter error: Indicator of whether the termination condition has an error.
        /// - Parameter tag: The tag of the element that is requesting the end of eval.
        /// - Parameter vTime: The virtual time.
        public func requestEvalEnd(_ error: IRfError? = nil, tag: String? = nil, vTime: Rf.VTime?)
        {
            onRequest(Rf.EvalScope.eRequest.eRequestEvalEnd(error, tag), vTime)
        }

        /// Remove all delegates.
        public func decomposeAll()
        {
            onRequest = { (_, _) in }
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Control Request notifier with request reply.
    ///

    open class RequestorWithReply
    {
        public typealias eEvalRequestReply = Rf.EvalScope.eRequestResult

        /// The onRequest delegate.
        public var onRequest : (IRfRequest, Rf.VTime?) -> eEvalRequestReply

        /// Initialise with no onRequest delegate.
        public init()
        {
            self.onRequest = { (_, _) in return .eNone }
        }

        /// Initialise with onRequest delegate.
        /// - Parameter: onRequest: the onRequest delegate to use in the requestor.
        public init(onRequest : @escaping (IRfRequest, Rf.VTime?) -> eEvalRequestReply)
        {
            self.onRequest = onRequest
        }

        @discardableResult public func notify(request: IRfRequest, vTime: Rf.VTime?) -> eEvalRequestReply
        {
            return onRequest(request, vTime)
        }

        /// Request the end of evaluation.
        /// - Parameter error: Indicator of whether the termination condition has an error.
        /// - Parameter tag: The tag of the element that is requesting the end of eval.
        /// - Parameter vTime: The virtual time.
        public func requestEvalEnd(_ error: IRfError? = nil, tag: String? = nil, vTime: Rf.VTime?)
        {
            let _ = onRequest(Rf.EvalScope.eRequest.eRequestEvalEnd(error, tag), vTime)
        }

        /// Remove all delegates.
        public func decomposeAll()
        {
            onRequest = { (_, _) in return .eNone }
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Consumer requestor delegate notifier.

    public class ConsumerRequestorWithReply<OutItem> : Rf.Identity, Equatable
    {
        public typealias eRequest = Rf.EvalScope.eRequest
        public typealias eEvalRequestReply = Rf.EvalScope.eRequestResult

        public var onRequest : (IRfRequest, Rf.AConsumer<OutItem>?, Rf.VTime?) -> eEvalRequestReply = { (_, _, _) in return .eNone }

        /// Initialise general supervisor notifier with trace ID and invoking element identity.
        public init(_ traceID: Rf.TraceID)
        {
            super.init(traceID, elementType: .eControlNotifier("Supervisor Notifier(\(traceID))"))
        }

        /// The request notify method.
        @discardableResult public func notify(request: IRfRequest, fromConsumer: Rf.AConsumer<OutItem>?, vTime: Rf.VTime?) -> eEvalRequestReply
        {
            return onRequest(request, fromConsumer, vTime)
        }

        /// Remove all targets.
        public func decomposeAll()
        {
            onRequest = { (_, _, _) in return .eNone }
        }

        public static func==<OutItem>(lhs: Rf.Notifiers.ConsumerRequestorWithReply<OutItem>, rhs: Rf.Notifiers.ConsumerRequestorWithReply<OutItem>) -> Bool
        {
            return lhs.traceID.traceIntID == rhs.traceID.traceIntID
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Rf Tracer definitions.
//

extension IRfTracer
{
    /// Format a trace message, with a title and indent.
    /// - Parameter title: The title placed at the beginning of the trace line.
    /// - Parameter message: The message
    /// - Parameter indent: The indent of the message.
    public func format(_ title: String, _ message : String, indent : Int, marker : String? = nil)
    {
        guard enabled else { return }

        let padding = String(repeating: " ", count : indent * Rf.TraceID.IndentUnit)
        let effectiveName = ">> " + title

        output("\(padding)\(effectiveName) \(message)")
    }

    /// Format a line separator followed by a trace lines, with a title and message indented.
    // - Parameter title: The title placed at the beginning of the trace line.
    /// - Parameter message: The message
    /// - Parameter indent: The indent of the message.
    public func formatWithSeparator(_ title: String, _ message : String, indent : Int, marker : String? = nil)
    {
        guard enabled else { return }

        let padding = String(repeating: " ", count : indent * Rf.TraceID.IndentUnit)
        let effectiveName = ">> " + title

        output("\n\(padding)----------------------------------------\n\(padding)\(effectiveName) \(message)")
    }

    /// Format an error message, with a title and indent.
    // - Parameter title: The title placed at the beginning of the trace line.
    /// - Parameter message: The message
    /// - Parameter indent: The indent of the message.
    public func formatError(_ title: String, _ message : String, indent : Int, force : Bool = false)
    {
        guard enabled else { return }

        let padding = String(repeating: " ", count : indent * Rf.TraceID.IndentUnit)

        output("\(padding)>> Error >>>>>>> \(title) \(message)")
    }
}


//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Rf Enumeration definitions.
///

extension Rf
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The Non-timestamped Enumerable base class implemented as a collector.
    ///

    public class Enumerable<Item> : Rf.ACollector<Item>, Sequence
    {
        public typealias Element = Item

        public var store : Rf.Notifications.Store<Item>

        public var generator : AnyIterator<Item>? = nil
        
        public override init(_ traceID: Rf.TraceID)
        {
            self.store =  Rf.Notifications.Store<Item>(traceID.appending("store"), termination: .eNone)

            super.init(traceID)
        }

        /// The Sequence iterator generator.
        public func makeIterator() -> AnyIterator<Item>
        {
            return generator!
        }
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// The timestamped Enumerable base class implemented as a collector.
    ///

    public class EnumerableWithVTime<Item> : Rf.ACollector<Item>, Sequence
    {
        public typealias ItemVTime = (Item, Rf.VTime?)

        public var store : Rf.Notifications.Store<ItemVTime>

        public var generator : AnyIterator<ItemVTime>? = nil

        public override init(_ traceID: Rf.TraceID)
        {
            self.store = Rf.Notifications.Store<ItemVTime>(traceID.appending("store"), termination: .eNone)

            super.init(traceID)
        }

        /// The Sequence iterator generator.
        public func makeIterator() -> AnyIterator<ItemVTime>
        {
            return generator!
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Rf Support extensions.
//

extension Rf
{
    /// The instance ID type for TraceIDs.
    public typealias InstanceID = Int64

    /// The instance counter:  A sequential cardinal number generator for instances.
    public struct Instance
    {
        /// The current instance counter value.
        private static var counter : InstanceID = 1

        /// Get the next Instance ID
        public static var nextInstanceID: InstanceID
        {
            let currentCount = counter

            counter += 1

            return currentCount
        }
    }

    /// Success/Failure result type with a tag name.
    public enum eNamedResult : Equatable
    {
        case eSuccess(String)
        case eFailure(String, String)
        case eProgress(String, Float, Int?)
    }

    /// The item expression result type.
    public enum eItemResult<Item>
    {
        case eSuccess
        case eResult(Item)
        case eError(IRfError)
    }

    /// Notification Store termination condition.
    public enum eTermination : Equatable
    {
        /// No termination specified.
        case eNone

        /// Completion termination with optional error.
        case eCompletion(IRfError?)

        public var error :IRfError?
        {
            switch self
            {
                case .eNone:                    return nil
                case .eCompletion(let error):   return error
            }
        }

        public var count : Int
        {
            switch self
            {
                case .eNone:                    return 0
                case .eCompletion:              return 1
            }
        }
    }

    /// Operator failure handling options.
    public enum eFailureOptions<Item>
    {
        case eNothing
        case eEmit(Item)
        case eEmitError(IRfError)
    }

    /// Operator error handling options.
    public enum eOnErrorOptions<Item>
    {
        case eRetry(UInt?, Rf.EvalScope.eEvalType)
        case eEmitItem(Item)
        case eEmitError(IRfError)
        case eEmitStream(Rf.ASource<Item>, Rf.EvalScope.eEvalType)
    }

    /// The synchronous Rf.ASource emit types.
    public enum eEmit<Item>
    {
        case eEmpty
        case eEmptyWithNoComplete
        case eItem(Item)
        case eError(IRfError)
        case eItemWithError(Item, IRfError)
        case eItemArray(ContiguousArray<Item>)
        case eItemArrayWithError(ContiguousArray<Item>, IRfError)
        case eStore(Rf.Notifications.Store<Item>)

        public var generator : (Int, Rf.VTime?, Rf.AElement<Void, Item>) -> Bool
        {
            switch self
            {
                case .eEmpty:

                    return { (index, vTime, element) in

                        if index == 0
                        {
                            element.endEval(nil, vTime: vTime)
                        }

                        return false
                    }

                case .eEmptyWithNoComplete:

                    return { (_, _, _) in return false }

                case .eItem(let item):

                    return { (index, vTime, element) in

                        if index == 0
                        {
                            element.producer.notify(item: item, vTime: vTime)
                            element.endEval(nil, vTime: vTime)
                        }

                        return false
                    }

                case .eError(let error):

                    return { (index, vTime, element) in

                        if index == 0
                        {
                            element.endEval(error, vTime: vTime)
                        }

                        return false
                    }

                case .eItemWithError(let (item, error)):

                    return { (index, vTime, element) in

                        if index == 0
                        {
                            element.producer.notify(item: item, vTime: vTime)
                            element.endEval(error, vTime: vTime)
                        }

                        return false
                    }

                case .eItemArray(let items):

                    return { (index, vTime, element) in

                        switch index
                        {
                            case 0..<items.count:
                                element.producer.notify(item: items[index], vTime: vTime)
                                return true

                            case items.count:
                                element.endEval(nil, vTime: vTime)

                            default:
                                break
                        }

                        return false
                    }

                case .eItemArrayWithError(let (items, error)):

                    return { (index, vTime, element) in

                        switch index
                        {
                            case 0..<items.count:
                                element.producer.notify(item: items[index], vTime: vTime)
                                return true

                            case items.count:
                                element.endEval(error, vTime: vTime)

                            default:
                                break
                        }

                        return false
                    }

                case .eStore(let itemStore):

                    return { (index, vTime, element) in

                        switch index
                        {
                            case 0..<itemStore.itemCount:

                                if let item = itemStore[index]
                                {
                                    element.producer.notify(item: item, vTime: vTime)
                                }

                                return true

                            case itemStore.itemCount:

                                switch itemStore.termination
                                {
                                    case .eNone:                        break
                                    case .eCompletion(let error):       element.endEval(error, vTime: vTime)
                                }

                            default:

                                break
                        }

                        return false
                    }
            }
        }
    }

    /// The asynchronous Rf.ASource emit types.
    public enum eEmitAsync<Item>
    {
        case eStore(Rf.Notifications.Store<Rf.eTimestampedNotification<Item>>)
        case eFromNotifier(Rf.Notifiers.DelegateTarget<Item>)
    }

    /// Source Notification Enumeration including control and item notifications.
    public enum eSourceNotification<Item>
    {
        case eFabricTool(String, IRfToolable)
        case eEvalControl(Rf.EvalScope.eNotify, Rf.VTime?)
    }

    /// Operator Notification Enumeration including control and item notifications.
    public enum eOperatorNotification<Item>
    {
        case eFabricTool(String, IRfToolable)
        case eItem(Int, Item, Rf.VTime?)
        case eEvalControl(Rf.EvalScope.eNotify, Rf.VTime?)
    }

    /// Source generation types.
    public enum eGenerate<Item>
    {
        case eSync((Int) -> Item?)
        case eASync((Int) -> (Item, Rf.VTime)?)
        case eASyncByDate((Int) -> (Item, Date)?)
    }

    /// Combine source types.
    public enum eCombineStrategy
    {
        case eLatest
        case eZip
    }

    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    /// eTimerLeeway: Timer leeway handling types.
    ///
    /// - eLeewayMin: Use the minimum leeway 10msec.
    /// - eLeewayAsTime: Define the leeway as a fixed time, in sec (as TimeInterval).
    /// - eLeewayAsFactor: Define the leeway as a factor of the timer time offset (as Double).

    public enum eTimerLeeway
    {
        /// Use the minimum leeway 10msec.
        case eLeewayMin

        /// Define the leeway as a fixed time, in sec (as TimeInterval).
        case eLeewayAsTime(leewayTime: TimeInterval)

        /// Define the leeway as a factor of the timer time offset (as Double).
        case eLeewayAsFactor(leewayFactor: Double)
    }

    /// The settings related to the Scheduler.
    public struct SchedulerSettings
    {
        public static let SourceDef   = SchedulerSettings()
        public static let OperatorDef = SchedulerSettings(priority: 1)

        public let priority : Int
        public let leeway : eTimerLeeway

        public init(priority : Int = 0, leeway: eTimerLeeway = .eLeewayMin)
        {
            self.priority = 0
            self.leeway = .eLeewayMin
        }

        public func setting(priority: Int) -> SchedulerSettings
        {
            return SchedulerSettings(priority: priority, leeway: self.leeway)
        }
    }

    /// Client Id management and Id generation.
    public struct SessionId
    {
        var  m_currentClientId = 0

        public mutating func nextId() -> Int
        {
            m_currentClientId += 1;

            return m_currentClientId
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Rf Scoping definitions.
//

extension Rf // Scoping
{
    /// The Scope stack support for scoped elements.
    open class ScopeStack<Element>
    {
        /// The scope stack.
        public var stack = ContiguousArray<Element>()

        /// The top of the stack.
        public var top : Element { return stack.last! }

        /// The current scope element.
        public private(set) var current : Element? = nil

        public init()
        {
        }

        /// Push a scoped element onto the scope stack.
        public func push(_ element: Element)
        {
            stack.append(element)

            current = element
        }

        /// Pop a scoped element from the scope stack.
        @discardableResult public func pop() -> Element?
        {
            let element = stack.removeLast()

            current = stack.first

            return element
        }
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
//
// Rf Support definitions.
//

extension Rf
{
    /// Weak reference support.
    public class WeakRef<T> where T: AnyObject
    {
        private(set) weak var value: T?

        public init(value: T?)
        {
            self.value = value
        }
    }
}