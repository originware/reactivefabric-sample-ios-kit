
// UnitTestSupport.swift
// Reactive Fabric SDK
//
// Created by Terry Stillone on 26/3/18.
// Copyright (c) 2018 Originware. All rights reserved.
//
// This file, all proprietary knowledge and algorithms it details are the sole property of
// Originware unless otherwise specified. The software, this file
// belong with is the confidential and proprietary information of Originware.
// ("Confidential Information"). You shall not disclose such
// Confidential Information and shall use it only in accordance with the terms
// of the license agreement you entered into with Originware.
//

import XCTest

import ReactiveFabric

@testable import SampleiOSApp

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// RfTestCase: The Reactive Fabric Unit Test Case.
///
/// - Creates test rigs.
/// - Defines Unit Test constants.
///

class RfTestCase: XCTestCase
{
    typealias TestSet = Rf.Test.TestSet
    typealias eEvalNotify = Rf.EvalScope.eNotify
    typealias eEvalRequest = Rf.EvalScope.eRequest
    typealias RfTime = TimeInterval

    /// The default UnitTest timeout.
    let WaitTimeout = RfSDK.Constant.DefaultWaitTimeout

    /// Create an Element Test Rig.
    func createTestRig(_ name : String) -> Rf.Sockets.TestRig
    {
        typealias TestRig = Rf.Sockets.TestRig
        typealias TestSet = Rf.Test.TestSet
        typealias eTestItem = Rf.Test.eTestNode
        
        enum eMessageChange
        {
            case eTestDidFinish
            case eTestMessageTimedout
        }
        
        let traceId = Rf.TraceID(name)
        let testRig = TestRig(traceId.appending("testRig"))
        var lastMessageTime : Date? = nil
 
        let _ = testRig.onProgress({ (testName, fraction, totalCount) in

            func classifyMessageChange() -> eMessageChange?
            {
                if fraction > (Float(totalCount!) - 1) / Float(totalCount!)
                {
                    return .eTestDidFinish
                }
                
                if let lastMessageTime = lastMessageTime, -lastMessageTime.timeIntervalSinceNow > 2
                {
                    return .eTestMessageTimedout
                }
        
                return nil
            }
            
            func formatMessage(forChange change: eMessageChange) -> String
            {
                switch change
                {
                    case .eTestDidFinish:
                    
                        return "    >> \(testName) ".padding(toLength: 50, withPad: " ", startingAt: 0) + " 100%"
                    
                    case .eTestMessageTimedout:
                    
                        return "    >>  ".padding(toLength: 50, withPad: " ", startingAt: 0) + String(format: " %.0f%%", fraction * 100)
                }
            }

            guard let change = classifyMessageChange() else { return }
            
            let message = formatMessage(forChange: change)
        
            lastMessageTime = Date()
            
            testOutput(message)
        })
        
        return testRig
    }
    
    func N<Item>(_ item: Item, _ vTime : Rf.VTime) -> Rf.eTimestampedNotification<Item>
    {
        return Rf.eTimestampedNotification.eItem(item, vTime)
    }
    
    func N<Item>(notifyEvalEnd error: IRfError?, _ vTime : Rf.VTime = 0) -> Rf.eTimestampedNotification<Item>
    {
        return Rf.eTimestampedNotification.eControl(eEvalNotify.eEvalEnd(error), vTime)
    }
    
    func N<Item>(requestEndEval error: IRfError?, _ vTime : Rf.VTime = 0) -> Rf.eTimestampedNotification<Item>
    {
        return Rf.eTimestampedNotification.eControl(eEvalRequest.eRequestEvalEnd(error, nil), vTime)
    }
    
    func NA<Item>(_ itemTuples: ContiguousArray<(Item, Rf.VTime)>, _ scheduledOffset : TimeInterval) -> Rf.eTimestampedNotification<ContiguousArray<Rf.eTimestampedNotification<Item>>>
    {
        var scheduledItems = ContiguousArray<Rf.eTimestampedNotification<Item>>()
        
        for itemTuple in itemTuples
        {
            scheduledItems.append(Rf.eTimestampedNotification.eItem(itemTuple.0, itemTuple.1))
        }
        
        return Rf.eTimestampedNotification.eItem(scheduledItems, scheduledOffset)
    }
    
    func NA<Item>(terminateEval error: IRfError?, _ vTime : Rf.VTime = 0) -> Rf.eTimestampedNotification<ContiguousArray<Rf.eTimestampedNotification<Item>>>
    {
        return Rf.eTimestampedNotification.eControl(eEvalRequest.eRequestEvalEnd(error, nil), vTime)
    }
    
    func NA<Item>(endEval error: IRfError?, _ vTime : Rf.VTime = 0) -> Rf.eTimestampedNotification<ContiguousArray<Rf.eTimestampedNotification<Item>>>
    {
        return Rf.eTimestampedNotification.eControl(eEvalNotify.eEvalEnd(error), vTime)
    }
}

//<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
///
/// Rf Scheduling
///
extension Rf
{
    /// Use for tracking scheduled items.
    public class ScheduledItem<Item> : IRfScheduledItem, CustomStringConvertible
    {
        /// The item that was scheduled.
        public let item :              Item
        
        /// The start time of the scheduler.
        public var schedulerStartTime: Date? = nil
        
        /// The time offset from sheduler start that the item was scheduled for.
        public let scheduledOffset :   TimeInterval
        
        /// The item trigger time by the scheduler.
        public var triggerTime:        Date? = nil
        
        /// CustomStringConvertible conformance.
        public var description: String
        {
            return "item: \(item), scheduledOffset: \(scheduledOffset)"
        }
        
        /// Initialise with item and scheduled time offset for schedulder start.
        public init(item: Item, scheduledOffset: TimeInterval)
        {
            self.item = item
            self.scheduledOffset = scheduledOffset
        }
        
        /// Set the scheduler start time.
        /// -
        public func setSchedulerStartTime(_ time: Date)
        {
            self.schedulerStartTime = time
        }
        
        public func setTriggerTime(_ time: Date)
        {
            self.triggerTime = time
        }
    }
}

extension Rf.ScheduledItem: Equatable where Item : Equatable
{
    public static func==<Item : Equatable>(lhs: Rf.ScheduledItem<Item>, rhs: Rf.ScheduledItem<Item>) -> Bool
    {
        return (lhs.item == rhs.item) && abs(lhs.scheduledOffset - rhs.scheduledOffset) < 0.000001
    }
}

extension Rf.AElement where OutItem == Rf.eNamedResult
{
    /// Monitor the Unit Test target fabric for failures.
    @discardableResult func monitor(_ name : String? = nil) -> Rf.AOperator<Rf.eNamedResult, Rf.eNamedResult>
    {
        let op : Rf.AOperator<Rf.eNamedResult, Rf.eNamedResult> = Rf.Operators.onFailure({
            
            let tag = name == nil ? "" : "test(\(name!)): "
            
            XCTFail("\(tag)\($0): \($1)")
        })
        
        self.compose(op)
        
        return op
    }
}

extension FileHandle : TextOutputStream
{
    public func write(_ string: String)
    {
        guard let data = string.data(using: .utf8) else { return }
        
        self.write(data)
    }
}

private var standardError = FileHandle.standardError
private let TestOutputQueue = Rf.Eval.Queue(Rf.TraceID("TestOutputQueue"), queueType: .eSerial)

func testOutput(_ message : String)
{
    print(message, terminator: "\n", to: &standardError)
}
