
import Foundation

// TestRig.swift
// Reactive Fabric SDK
//
// Created by Terry Stillone on 15/6/18.
// Copyright (c) 2018 Originware. All rights reserved.
//
// This file, all proprietary knowledge and algorithms it details are the sole property of
// Originware unless otherwise specified. The software, this file
// belong with is the confidential and proprietary information of Originware.
// ("Confidential Information"). You shall not disclose such
// Confidential Information and shall use it only in accordance with the terms
// of the license agreement you entered into with Originware.
//

import ReactiveFabric

public extension Rf
{
    public struct Test
    {
        /// Test items in the Test Set.
        public indirect enum eTestNode
        {
            public typealias OnCompletionFunc = () -> Void

            /// Evaluate a Group of Test Runs.
            case eTestGroup([eTestNode])

            /// Test run with no managed fabric.
            case eTestRun(String, (String) -> Void)

            /// Test run with a managed fabric.
            case eTestFabric(String, Rf.EvalScope.eEvalType, (String, Rf.AFabric) -> Void)

            /// Test run with a managed fabric and a completion action.
            case eTestFabricWithOnCompletion(String, Rf.EvalScope.eEvalType, (String, Rf.AFabric) -> Void, () -> Void)

            /// Get the number of test items in a test set.
            public var count : Int
            {
                var result = 0

                switch self
                {
                    case .eTestGroup(let testItems):

                        for testItem in testItems
                        {
                            result += testItem.count
                        }

                    case .eTestRun, .eTestFabric, .eTestFabricWithOnCompletion:

                        result += 1
                }

                return result
            }
        }

        /// The Test set to be evaluated by the TestRig.
        public class TestSet
        {
            /// The name of the Test Set.
            public let name :        String

            /// The number of Test Set runs
            public let runCount:     Int

            /// The tree of test nodes for the Test Set.
            public let testNodeTree: eTestNode

            /// Initialise with Test Set name and Test node tree.
            /// - Parameter name: The name of the Test Set.
            /// - Parameter runCount: The number of Test Set runs.
            /// - Parameter testNodeTree: The test node tree.
            public init(_ name: String, runCount: Int = 1, _ testNodeTree: eTestNode)
            {
                self.name = name
                self.runCount = runCount
                self.testNodeTree = testNodeTree
            }
        }

        public struct TestSetQueue
        {
            /// The set of queued test sets to run.
            private var m_queue = ContiguousArray<TestSet>()

            /// The count of test sets queued.
            public var count : Int { return m_queue.count }

            /// Queue a test set.
            public mutating func queue(_ testSet: TestSet)
            {
                m_queue.append(testSet)
            }

            /// Get the next test set.
            public mutating func getNext() -> TestSet?
            {
                guard m_queue.count > 0 else { return nil }

                m_queue.removeFirst()

                if let nextTestSet = m_queue.first
                {
                    return nextTestSet
                }
                else
                {
                    return nil
                }
            }
        }
    }
}

extension Rf.Sockets
{
    //<<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>><<>>
    ///
    ///  TestRig is employed in the Unit Testing of the SDK.
    ///

    public class TestRig: Rf.Patterns.Socket<TestRig.eRequest, Rf.eNamedResult>
    {
        /// The request enum of the Test Rig.
        public enum eRequest
        {
            case eRunTestSet(Rf.Test.TestSet)
        }

        private var m_currentItemCount = 0

        /// Initialise the Test Rig with the traceID
        /// - Parameter traceID: The traceID of the TestRig.
        /// - Parameter doFabricConsistencyCheck: Indicator that fabric notification integrity should be checked.
        public init(_ traceID: Rf.TraceID, doFabricConsistencyCheck: Bool = true)
        {
            super.init(traceID)

            consumer.onItem = { [weak self] (item, vTime) in

                guard let strongSelf = self else { return }

                switch item
                {
                    case .eRunTestSet(let testSet):

                        // Process the Test Set synchronously.
                        strongSelf.run(testSet: testSet, testItem: testSet.testNodeTree, doFabricCheck: doFabricConsistencyCheck)
                }
            }
        }

        /// Notify a request to the Test Rig.
        /// - Parameter: item: The request for the Test Rig.
        /// - Parameter vTime: The virtual time.
        public final func syncNotify(item: eRequest, vTime: Rf.VTime? = nil)
        {
            // Process the item synchronously.
            consumer.onItem(item, vTime)
        }

        /// Run the given test item in the given test set.
        /// - Parameter testSet: The Test Set that includes the Test Item to process.
        /// - Parameter testItem: The Test Item to process
        /// - Parameter doFabricCheck: Indicator of whether to check the consistency of the fabric.
        private func run(testSet: Rf.Test.TestSet, testItem: Rf.Test.eTestNode, doFabricCheck: Bool)
        {
            // Used to calculate the test set progress.
            let totalTestItemCount         = testSet.testNodeTree.count
            let totalRunTestItemCount      = testSet.runCount * totalTestItemCount
            let totalRunTestItemCountFloat = Float(totalRunTestItemCount)

            func notifyProgress()
            {
                let progressRatio = Float(m_currentItemCount) / Float(totalRunTestItemCountFloat)

                producer.notify(item: Rf.eNamedResult.eProgress(testSet.name, progressRatio, totalRunTestItemCount), vTime: nil)
            }

            func checkFabricConsistency(_ fabric : Rf.AFabric)
            {
                guard doFabricCheck else { return }

                fabric.consistencyCheck(nil)
            }

            switch testItem
            {
                    // Evaluate a Group of Test Runs.
                case .eTestGroup(let testItems):

                    m_currentItemCount = 0
                    
                    for testItem in testItems
                    {
                        run(testSet: testSet, testItem: testItem, doFabricCheck: doFabricCheck)
                    }

                    // Test run with no managed fabric.
                case .eTestRun(let (testName, fabricNotifyFunc)):

                    for _ in 0..<testSet.runCount
                    {
                        notifyProgress()

                        fabricNotifyFunc(testName)

                        m_currentItemCount += 1

                        notifyProgress()
                    }

                    // Test run with a managed fabric.
                case .eTestFabric(let (testName, evalType, fabricNotifyFunc)):

                    for _ in 0..<testSet.runCount
                    {
                        let fabric = RfSDK.factory.Fabric(traceID.appending(testSet.name + "/" + testName))

                        notifyProgress()

                        fabricNotifyFunc(testName, fabric)
                        
                        m_currentItemCount += 1

                        checkFabricConsistency(fabric)

                        fabric.beginEval(evalType, vTime: fabric.scheduler?.currentVTime)
                    }

                    notifyProgress()

                    /// Test run with a managed fabric and a post completion action.
                case .eTestFabricWithOnCompletion(let (testName, evalType, fabricNotifyFunc, onCompletion)):

                    for _ in 0..<testSet.runCount
                    {
                        notifyProgress()

                        do
                        {
                            let fabric = RfSDK.factory.Fabric(traceID.appending(testSet.name + "/" + testName))

                            fabricNotifyFunc(testName, fabric)
                            
                            m_currentItemCount += 1

                            checkFabricConsistency(fabric)

                            fabric.beginEval(evalType, vTime: fabric.scheduler?.currentVTime)
                        }

                        onCompletion()
                    }

                    notifyProgress()
            }
        }
    }
}
