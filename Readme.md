---
# README:    The Reactive Fabric Technology Sample Source Kit v1.0
Sample Kit Platforms | iOS
-------------------| :----
SDK Platforms      | iOS, OSX (and Linux coming)
Sample Kit Platforms | iOS Simulator only
Language           | Swift 5 & 4.2 compatible
Kit Requirements:  | Xcode 10+
Origination        | [Originware.com](http://www.originware.com)


This sample kit provides an **iOS** App for the purpose of evaluating the **Reactive Fabric Technology** and associated **SDK**.

The supplied App (source) demonstrates the application and benefits of the technology on software architecture and design.
This source version does not include the **Reactive Fabric SDK** framework binary for security reasons (and this project as such will fail compilation due this omission).
The SDK can be supplied, on request but will probably require signing an NDA.

### What is **Reactive Fabric Technology** ?

Reactive Fabric technology covers a number of areas in software design and development:
                
* **Reactive Fabric** is software design methodology. It supports a collaborative and iterative design process.
* **Reactive Fabric** is a design visualisation technology. Designs are visual and in turn supports design sharing, documentation and comprehension.
* **Reactive Fabric** elements provide an asynchronous processing architectural model. **Elements** can operate synchronously or asynchronously.
* **Reactive Fabric** includes a framework **SDK** which maps design-elements to Swift element classes and performs **Fabric** evaluation.

For more information, please see the Reactive Fabric White Paper on the [Originware documentation page](https://www.originware.com/doc.html).

### This Sample Kit Package

This package contains:

* A Swift Xcode Project which includes: 	
	* An iPad **Reactive Fabric** based **Point of Interest** sample app.
	* A sample source library of **Reactive Fabric** **Sources** and **Operators**.
	* A sample source of a **Reactive Fabric** element Test Rig and a small collection of sample synchronous and asynchronous Unit Tests.
	* White papers on the **Reactive Fabric Technology**.

* Notes:
	* [The Sample Kit Release Notes](ReleaseNotes.md).
	* [The Overview of the Sample Kit](SamplelKitOverview.md) guide.


### Licensing

The source in this package is provided for under the Apache Version 2.0 license, see the [License.txt](License.txt) file.

### For More Information

See the video of this Apps operation: [POI App Screencast](https://www.originware.com/video/RfSampleAppScreencast.mp4).

See the included documentation: [Reactive Fabric White Paper](Reactive Fabric/Doc/Reactive Fabric Whitepaper.pdf) and [Reactive Fabric For Desktop and Mobile Product Application](Reactive Fabric/Doc/Reactive Fabric For Desktop and Mobile Product Application.pdf)

See the Originware [Reactive Fabric Technology](https://www.originware.com/reactivefabric.html) and [Sample Kit](https://www.originware.com/rfsamplekit.html) webpages.

Please direct requests, comments and feedback to [Terry Stillone](mailto:terry@originware.com) at Originware.com